
# -*- python -*-
#
#  This file is part of BioManager 
#
##############################################################################

from .bioSparqlService import Orphadata,MeshEndpoint,OntobeeEndpoint,EbiEndpoint,UniprotEndpoint,OmimEndpoint,LinkedLifeDataEndpoint,DisgenetEndpoint,PdbEndpoint


__all__ = ["BioEntity","Orphanet","Mesh","Go","Reactome","Uniprot","Omim","UMLS","Nextprot","Mondo","PDB","Disgenet","ALBase"]



class BioEntity(object) :
    name = ""
    baseUri = ""
    idSeparator = "/"

    def __init__(self,name,baseUri,idSeparator):
        self._name = name
        self._baseUri = baseUri
        self._idSeparator = idSeparator

    def get_name(self):
        return self._name

    def set_name(self,value):
        self._name = value
    name = property(get_name,set_name, doc="Name of this entity")

    def get_baseUri(self):
        return self._baseUri

    def set_baseUri(self,value):
        self._baseUri = value
    baseUri = property(get_baseUri, set_baseUri, doc="BaseUri of this entity")


    def set_idSeparator(self,value):
        self._idSeparator = value

    def get_idSeparator(self):
        return self._idSeparator

    idSeparator = property(get_idSeparator,set_idSeparator, doc="Separator befor the id")


class Orphanet(BioEntity) :
    """ Info about Orphanet
    """
    #name="orphanet"
    #baseUri="http://www.orpha.net/ORDO/"
    #idSeparator="/"

    def __init__(self) :
        self.name="Orphanet"
        self.baseUri="http://www.orpha.net/ORDO/"
        self.idSeparator="/"
        super().__init__(self.name,self.baseUri,self.idSeparator)

    def getBioSparqlService(self) :
        return Orphadata()


class Mesh(BioEntity) :
    """ Info about Mesh  
    """

    def __init__(self) :
        self.name="MeSH"
        self.baseUri="http://id.nlm.nih.gov/mesh/"
        self.idSeparator="/"
        super().__init__(self.name,self.baseUri,self.idSeparator)

    def getBioSparqlService(self) :
        return MeshEndpoint()

class Omim(BioEntity) : 
    """ Info about OMIM
    """

    def __init__(self) : 
        self.name = "OMIM"
        self.baseUri="http://omim.org/entry/"
        self.idSeparator="/"
        super().__init__(self.name,self.baseUri,self.idSeparator)

    def getBioSparqlService(self) :
        return OmimEndpoint()


class Go(BioEntity) :
    """ Info about Gene Ontology  
    """
    def __init__(self) :
        self.name="GO"
        self.baseUri="http://purl.obolibrary.org/obo/"
        self.idSeparator="/"
        super().__init__(self.name,self.baseUri,self.idSeparator)

    def getBioSparqlService(self) :
        return OntobeeEndpoint()


class Mondo(BioEntity) :
    """ Info about Gene Ontology  
    """
    def __init__(self) :
        self.name="MONDO"
        self.baseUri="http://purl.obolibrary.org/obo/"
        self.idSeparator="/"
        super().__init__(self.name,self.baseUri,self.idSeparator)

    def getBioSparqlService(self) :
        return OntobeeEndpoint()

class Disgenet(BioEntity) :
    """ Info about Gene Ontology  
    """

    def __init__(self) :
        self.name="Disgenet"
        self.baseUri="http://rdf.disgenet.org/resource/"
        self.idSeparator="/"
        super().__init__(self.name,self.baseUri,self.idSeparator)

    def getBioSparqlService(self) :
        return DisgenetEndpoint()


class Reactome(BioEntity) :
    """ Info about Gene Ontology  
    """

    def __init__(self) :
        self.name="Reactome"
        self.baseUri="http://www.reactome.org/biopax/61/"
        self.idSeparator="/"
        super().__init__(self.name,self.baseUri,self.idSeparator)

    def getBioSparqlService(self) :
        return EbiEndpoint()

class Uniprot(BioEntity) :
    """ Info about Uniprot
    """

    def __init__(self) :
        self.name="Uniprot"
        self.baseUri="http://purl.uniprot.org/uniprot/"
        self.idSeparator="/"
        super().__init__(self.name,self.baseUri,self.idSeparator)


    def getBioSparqlService(self):
        return UniprotEndpoint()

class UMLS(BioEntity) :
    """ Info about UMLS
    """
    def __init__(self) : 
        self.name="UMLS"
        self.baseUri="http://linkedlifedata.com/resource/umls/id/"
        self.idSeparator="/"
        super().__init__(self.name,self.baseUri,self.idSeparator)

    def getBioSparqlService(self) :
        return LinkedLifeDataEndpoint()

class Nextprot(BioEntity) :
    """ Info about Uniprot
    """
    def __init__(self) :
        self.name="Nextprot"
        self.baseUri="http://nextprot.org/rdf/entry/"
        self.idSeparator="/"
        super().__init__(self.name,self.baseUri,self.idSeparator)


    def getBioSparqlService(self):
        return NextprotEndpoint()


class PDB(BioEntity) :
    """ Info about Uniprot
    """
    def __init__(self) :
        self.name="PDB"
        self.baseUri="https://rdf.wwpdb.org/pdb/"
        self.idSeparator="/"
        super().__init__(self.name,self.baseUri,self.idSeparator)


    def getBioSparqlService(self):
        return PdbEndpoint()

class ALBase(BioEntity) :
    """ Info about Uniprot
    """
    def __init__(self) :
        self.name="ALBase"
        self.baseUri="http://albase.bumc.bu.edu/aldb"
        self.idSeparator=None
        super().__init__(self.name,self.baseUri,self.idSeparator)


    def getBioSparqlService(self):
        return None