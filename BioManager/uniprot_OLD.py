# -*- python -*-
#
#  This file is part of bioSparqlServices 
#
##############################################################################

from SPARQLWrapper import SPARQLWrapper, JSON


__all__ = ["UniProt"]


class UniProt(object):
    """Interface to the `UniProt <http://www.uniprot.org>`_ service
	"""

	def __init__(self):

	    self._endpoint = "https://sparql.uniprot.org/"
	    self._prefixes = """
	    PREFIX up:<http://purl.uniprot.org/core/> 
		PREFIX keywords:<http://purl.uniprot.org/keywords/> 
		PREFIX uniprotkb:<http://purl.uniprot.org/uniprot/> 
		PREFIX taxon:<http://purl.uniprot.org/taxonomy/> 
		PREFIX ec:<http://purl.uniprot.org/enzyme/> 
		PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
		PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> 
		PREFIX skos:<http://www.w3.org/2004/02/skos/core#> 
		PREFIX owl:<http://www.w3.org/2002/07/owl#> 
		PREFIX bibo:<http://purl.org/ontology/bibo/> 
		PREFIX dc:<http://purl.org/dc/terms/> 
		PREFIX xsd:<http://www.w3.org/2001/XMLSchema#> 
		PREFIX faldo:<http://biohackathon.org/resource/faldo#> 
	    """
	    
	def _get_prefixes(self):
        return self._prefixes
    prefixes = property(_get_prefixes)

	def execQuery(self,query,returnFormat=JSON):
		sparql = SPARQLWrapper(self._endpoint)
		sparql.setQuery(query)
		sparql.setReturnFormat(returnFormat)
		return sparql.query().convert()


