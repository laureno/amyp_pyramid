# AMYPdb

AMYPdb is a pyramid application which gathers information about genes, proteins and diseases related to amyloid processes.

## Prerequisite

The data used in this application is stored in a RDF triplestore.
The current version works with [RDF4J](https://rdf4j.eclipse.org/download/).

There are two options : either set up the triple store from scratch or use a snapshot with a pre-installed triple-store

### Setting up the triple store from scratch 

RDF4J server and workbench can be installed via the following docker image : [yyz1989/rdf4j](https://hub.docker.com/r/yyz1989/rdf4j/)
To run the docker image : 

```
docker run --name rdf4j -p 80:8080 -d yyz1989/rdf4j 
```

The workbench console can then be accessed at [http://localhost:8080/rdf4j-workbench/](http://localhost:8080/rdf4j-workbench/)  

The following repositories need to be created :
- mondo : title/id : mondo, type : In memory store, data to import : [mondo.owl](http://www.obofoundry.org/ontology/mondo.html)
- omim : title/id : omim, type : In Memory Store, data to import : [omim.ttl](https://bioportal.bioontology.org/ontologies/OMIM)
- go : title/id : go, type : In memory store, data to import : [go.owl](http://geneontology.org/docs/download-ontology/)
- uniprot : title/id : uniprot, type : in memory store, data to import :  [uniprot keywords](ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/rdf/) , uniprot diseases, and optionally : 
  - either leave it as it is : the remaining data will be fetched on the fly when generating RDF data for amyp (NB : very slow process)
  - or use the script load_Uni2.py located in amypdb/libAmyp/init_scripts to generate a complete file containing the RDF data corresponding to a given list of Uniprot Entries. This file can then be imported via the RDF4J workbench interface.
- amyp : title/id : amyp, type : native store **AND Lucene**. To generate the data, see the section dedicated to this repository below

These repositories are listed in development.ini and production.ini : if you wish to change the title/id for the different repositories, the following statements should be updated : 

```
repo_uniprot_query =  http://localhost:8080/rdf4j-server/repositories/uniprot
repo_uniprot_update = http://localhost:8080/rdf4j-server/repositories/uniprot/statements
repo_amyp_query = http://localhost:8080/rdf4j-server/repositories/amyp
repo_amyp_update = http://localhost:8080/rdf4j-server/repositories/amyp/statements
repo_mondo_query = http://localhost:8080/rdf4j-server/repositories/mondo
repo_go_query = http://localhost:8080/rdf4j-server/repositories/go
repo_omim_query = http://localhost:8080/rdf4j-server/repositories/omim
```

### Using a snapshot

For genouest users, an image containing the triple store already configured is available (image : RDF4J + Debian9 + Docker)



## Running the application

AMYPdb is an application based on [Pyramid](https://trypyramid.com/), a python web framework. 

Information about the general structure / features / commands can be found [here](https://docs.pylonsproject.org/projects/pyramid/en/1.10-branch/)

Basic command to run the application in development mode :  

```
pserve development.ini --reload
```

NB : The CSS used for the project makes use of the [bulma framework](https://bulma.io/). The source code is in the folder bulma_css. 
- to change the styles, edit src/mystyles.scss
- to specify the CSS output file, edit src/index.js
- to build the CSS file from the SCSS file, run the following command line :

```
npm run build
```

The generated CSS file can be found in the dist/css folder.
The CSS files are then to be put in the static folder of the Amypdb pyramid application



## Generating new data and annotations for AMYPdb

To generate new data / annotations for AMYPdb : 

- Clear the content of the amyp repository (This can be done via the interface of the RDF4J workbench. TO DO : write a script to do it programmatically)
- Optional : clear the content of the uniprot repository and fetch updated data by using libAmyp/init_scripts/load_Uni2.py
- Optional : in the production.ini file, change the status maintenance = False to maintenance = True  to show the page "under maintenance" to end-users
- Run the script **libAmyp/Pipeline.py**. What this script does : 
  - step 1 : creation of the thesaurus about amyloid descriptors from different sources (Mondo, Go, Reactome, Interprot ...). Those data are stored in a specific context graph in the amyp repository (<http://purl.amypdb.org/thesaurus/>)
  - step2 : creation of the entries of the knowledge base (proteins, genes, transcripts, isoforms, diseases...). Those data are stored in a specific contexte graph in the amyp repository (<http://purl.amypdb.org/kb/>)
  - step 3 : annotation of the entries of the knowledge base (amyloid, amyloid-like, amyloidosis, pathogenic...) 
  - step 4 : creation of tags for proteins and diseases from the different annotations found
  - step 5 : creation of a json file to store quantitative data (Not in use for the moment. To be used for the browse functionality on the website)
- Optional : in the production.ini file, change the status maintenance = True to maintenance = False


