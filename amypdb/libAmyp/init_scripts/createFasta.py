import requests, sys

with open("isolistseq2.csv","r") as f : 
    lines = f.readlines()
    cp=0
    for line in lines : 
        cp +=1
        print(cp)
        line = line.strip()
        info = line.split(";")
        isoId = info[0]
        seq = info[1]
        fastaF="../data/fasta2/"+isoId+".fasta"
        with open(fastaF, "w") as f: 
            f.write(">"+isoId+"\n")
            f.write(seq)


