import requests
import re
#from amypdb.libAmyp.AmyConf import *
endpoint = "http://localhost:80/rdf4j-server/repositories/uniprot"
#nextprot = "http://nextprot.org/rdf/entry/"
prot_file = "../data/init_uniprot_id.txt"

prefixes = """
@base <http://purl.uniprot.org/uniprot/> .
@prefix annotation: <http://purl.uniprot.org/annotation/> .
@prefix bibo: <http://purl.org/ontology/bibo/> .
@prefix citation: <http://purl.uniprot.org/citations/> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix disease: <http://purl.uniprot.org/diseases/> .
@prefix ECO: <http://purl.obolibrary.org/obo/ECO_> .
@prefix enzyme: <http://purl.uniprot.org/enzyme/> .
@prefix faldo: <http://biohackathon.org/resource/faldo#> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix go: <http://purl.obolibrary.org/obo/GO_> .
@prefix isoform: <http://purl.uniprot.org/isoforms/> .
@prefix keyword: <http://purl.uniprot.org/keywords/> .
@prefix location: <http://purl.uniprot.org/locations/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix position: <http://purl.uniprot.org/position/> .
@prefix pubmed: <http://purl.uniprot.org/pubmed/> .
@prefix range: <http://purl.uniprot.org/range/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix taxon: <http://purl.uniprot.org/taxonomy/> .
@prefix tissue: <http://purl.uniprot.org/tissues/> .
@prefix up: <http://purl.uniprot.org/core/> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
"""


with open(prot_file) as f:
    id_list= f.read().splitlines()


#r1 = requests.get('https://api.nextprot.org/entry/NX_P10279.ttl')
#print(r1)
#print(r1.status_code)

#print(len(id_list))
#print(len(id_list)/10)
dict={}
"""
for i in range(0,9) :
    if i*392+392 > len(id_list) : 
        dict[i]=id_list[i*392:len(id_list)]
    else : 
        dict[i]=id_list[i*392:i*392+392]
"""
#dict[0]=id_list[3900:]
dict[0]=id_list[4625:]
#print(dict[2])

for k,v in dict.items() :
    content=prefixes
    for elt  in v  :
        query= " ASK {{ <{uri}> ?p ?o }} ".format(uri="http://purl.uniprot.org/uniprot/"+elt)     
        headers = {'content-type' : 'application/x-www-form-urlencoded'}
        myparam = { 'query': query }
        r=requests.get(endpoint,myparam,headers=headers)    
        exists = r.json()['boolean']
        if not exists : 
            r = requests.get('https://www.uniprot.org/uniprot/{idu}.ttl'.format(idu=elt))
            #print(r.text)
            print(r.status_code, " : " , elt)
            if (r.status_code != 404) :
                filecontent = r.content.decode()
                filecontent = re.sub(r'@base.*\..*\n','', filecontent)
                filecontent = re.sub(r'@prefix.*\..*\n','', filecontent)
                content = content + "\n" + filecontent
    name_file = "results/temporary_Uni"+str(k)+".ttl"
    print(name_file)
    with open(name_file,"w") as temp : 
        temp.write(content)
        #with open("temporary_NX.ttl","a") as temp :
            #temp.write(r.content.decode())


