import requests, sys

requestURL = "https://www.uniprot.org/uniprot/"
with open("isolist.txt","r") as f : 
    lines = f.readlines()
    for line in lines : 
        isoId = line.strip()
        query = requestURL + isoId+".fasta"
        r = requests.get(query)
        responseBody = r.text
        print(r.status_code)
        fastaF="../data/fasta/"+isoId+".fasta"
        with open(fastaF, "w") as f: 
            f.write(responseBody)


