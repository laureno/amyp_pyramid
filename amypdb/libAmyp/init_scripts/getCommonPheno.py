from SPARQLWrapper import SPARQLWrapper, JSON

sparql = SPARQLWrapper("http://localhost:8080/rdf4j-server/repositories/amyp")
sparql.setQuery("""
PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
                        PREFIX model: <http://purl.amypdb.org/model/>
						PREFIX thes: <http://purl.amypdb.org/thesaurus/>
                        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
                        SELECT ?phenoRef ?phenoLabel  (COUNT(?maladie) AS ?nb)
                        WHERE {
						{ ?maladie model:tag thes:Amyp_Amyloidosis .}
						UNION 
						{ ?maladie model:tag thes:Amyp_Amyloid_Related_Disease . } 
						?maladie model:phenoRef ?phenoRef .
						?phenoRef skos:prefLabel ?phenoLabel .						
                        } GROUP BY ?phenoRef ?phenoLabel ORDER BY ?nb 

        """)
sparql.setReturnFormat(JSON)
results = sparql.query().convert()

resultStr=""
for result in results["results"]["bindings"]:
        uri=result["phenoRef"]["value"]
        label=result["phenoLabel"]["value"]
        nb=result["nb"]["value"]
        resultStr+=uri+";"+";"+label+";"+nb+"\n"


with open("results/commonPheno.txt", "w") as f: 
        f.write(resultStr)


