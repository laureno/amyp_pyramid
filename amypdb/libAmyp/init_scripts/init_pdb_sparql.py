import requests, sys

requestURL = "https://integbio.jp/rdf/sparql"
query = """
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX PDBo: <https://rdf.wwpdb.org/schema/pdbx-v50.owl#>
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
SELECT ?protein
FROM <http://rdf.integbio.jp/dataset/pdbj>
WHERE { {
?pdb dc:title ?text .
} UNION {  
?struct_keywords_ref PDBo:struct_keywords.text ?text .
?struct_keywords_ref PDBo:of_datablock ?pdb . 
}
FILTER regex(?text, "amylo(id|gen|do|se)|fibril", "i")
?pdb PDBo:has_entityCategory ?entity_category .
?entity_category PDBo:has_entity ?entity .
?entity PDBo:referenced_by_struct_ref ?struct_ref .
?struct_ref PDBo:link_to_uniprot ?protein .
}
 """



myparam = { 'query': query}
headers = {'Accept' : 'application/sparql-results+json'}
r=requests.get(requestURL,params=myparam, headers=headers)
results=r.json()


resultStr=""

for result in results["results"]["bindings"]:
    uri=result["protein"]["value"]
    uri=uri.replace("http://purl.uniprot.org/uniprot/","")
    resultStr+=uri+"\n"

with open("results/pdb_idlist.txt", "w") as f:
    f.write(resultStr)
