
from SPARQLWrapper import SPARQLWrapper, JSON
#code à refactoriser....utilisation one-shot pour le moment


diseaseList1 = """
<http://linkedlifedata.com/resource/umls/id/C0268398>
<http://linkedlifedata.com/resource/umls/id/C0268389>
<http://linkedlifedata.com/resource/umls/id/C0206246>
<http://linkedlifedata.com/resource/umls/id/C0206245>
<http://linkedlifedata.com/resource/umls/id/C2751492>
<http://linkedlifedata.com/resource/umls/id/CN227096>
<http://linkedlifedata.com/resource/umls/id/CN204529>
<http://linkedlifedata.com/resource/umls/id/C0268397>
<http://linkedlifedata.com/resource/umls/id/C1861735>
<http://linkedlifedata.com/resource/umls/id/CN237622>
<http://linkedlifedata.com/resource/umls/id/C1867773>
<http://linkedlifedata.com/resource/umls/id/C2751494>
<http://linkedlifedata.com/resource/umls/id/C2751536>
<http://linkedlifedata.com/resource/umls/id/C3888307>
<http://linkedlifedata.com/resource/umls/id/C3888308>
<http://linkedlifedata.com/resource/umls/id/C3888309>
<http://linkedlifedata.com/resource/umls/id/C3151404>
<http://linkedlifedata.com/resource/umls/id/C0268394>
<http://linkedlifedata.com/resource/umls/id/C2931672>
<http://linkedlifedata.com/resource/umls/id/C0002726>
<http://linkedlifedata.com/resource/umls/id/C0546394>
<http://linkedlifedata.com/resource/umls/id/C4274331>
<http://linkedlifedata.com/resource/umls/id/C0544839>
<http://linkedlifedata.com/resource/umls/id/CN201610>
<http://linkedlifedata.com/resource/umls/id/C4302669>
<http://linkedlifedata.com/resource/umls/id/CN203779>
<http://linkedlifedata.com/resource/umls/id/C0268380>
<http://linkedlifedata.com/resource/umls/id/C0281479>
<http://linkedlifedata.com/resource/umls/id/C0268381>
<http://linkedlifedata.com/resource/umls/id/C0342623>
<http://linkedlifedata.com/resource/umls/id/CN204235>
<http://linkedlifedata.com/resource/umls/id/C0221014>
<http://linkedlifedata.com/resource/umls/id/C3536715>
<http://linkedlifedata.com/resource/umls/id/CN206197>
<http://linkedlifedata.com/resource/umls/id/CN206638>
<http://linkedlifedata.com/resource/umls/id/CN206639>
<http://linkedlifedata.com/resource/umls/id/CN206640>
<http://linkedlifedata.com/resource/umls/id/C0333572>
<http://linkedlifedata.com/resource/umls/id/C1706802>
<http://linkedlifedata.com/resource/umls/id/C2751842>
<http://linkedlifedata.com/resource/umls/id/C1834207>
<http://linkedlifedata.com/resource/umls/id/C4284284>
<http://linkedlifedata.com/resource/umls/id/CN201303>
<http://linkedlifedata.com/resource/umls/id/C0268242>
<http://linkedlifedata.com/resource/umls/id/C0268243>
<http://linkedlifedata.com/resource/umls/id/C3151403>
<http://linkedlifedata.com/resource/umls/id/C0026764>
<http://linkedlifedata.com/resource/umls/id/C1833921>
<http://linkedlifedata.com/resource/umls/id/C3810100>
<http://linkedlifedata.com/resource/umls/id/C3160718>
<http://linkedlifedata.com/resource/umls/id/C1868675>
<http://linkedlifedata.com/resource/umls/id/C3150899>
<http://linkedlifedata.com/resource/umls/id/C0752120>
<http://linkedlifedata.com/resource/umls/id/C0030567>
<http://linkedlifedata.com/resource/umls/id/C0002736>
<http://linkedlifedata.com/resource/umls/id/C0020179>
<http://linkedlifedata.com/resource/umls/id/C2931257>
<http://linkedlifedata.com/resource/umls/id/C1846862>
<http://linkedlifedata.com/resource/umls/id/C1846707>
<http://linkedlifedata.com/resource/umls/id/C0034050>
<http://linkedlifedata.com/resource/umls/id/C1836694>
<http://linkedlifedata.com/resource/umls/id/C0270733>
<http://linkedlifedata.com/resource/umls/id/C1833662>
<http://linkedlifedata.com/resource/umls/id/C0338451>
<http://linkedlifedata.com/resource/umls/id/C0520716>
<http://linkedlifedata.com/resource/umls/id/C0030568>
<http://linkedlifedata.com/resource/umls/id/C0030569>
<http://linkedlifedata.com/resource/umls/id/CN203530>
<http://linkedlifedata.com/resource/umls/id/C0028064>
<http://linkedlifedata.com/resource/umls/id/C1135993>
<http://linkedlifedata.com/resource/umls/id/C0037019>
<http://linkedlifedata.com/resource/umls/id/C0393571>
<http://linkedlifedata.com/resource/umls/id/C0949664>
<http://linkedlifedata.com/resource/umls/id/C0022802>
"""
diseaseList2 = """
<http://linkedlifedata.com/resource/umls/id/C0036457>
<http://linkedlifedata.com/resource/umls/id/C0085209>
<http://linkedlifedata.com/resource/umls/id/CN043596>
<http://linkedlifedata.com/resource/umls/id/C1863051>
<http://linkedlifedata.com/resource/umls/id/C0024408>
<http://linkedlifedata.com/resource/umls/id/C3711380>
<http://linkedlifedata.com/resource/umls/id/C1861668>
<http://linkedlifedata.com/resource/umls/id/C0393584>
<http://linkedlifedata.com/resource/umls/id/CN202816>
<http://linkedlifedata.com/resource/umls/id/CN202725>
<http://linkedlifedata.com/resource/umls/id/C1852223>
<http://linkedlifedata.com/resource/umls/id/C0751781>
<http://linkedlifedata.com/resource/umls/id/C0752347>
<http://linkedlifedata.com/resource/umls/id/C1868681>
<http://linkedlifedata.com/resource/umls/id/C0017495>
<http://linkedlifedata.com/resource/umls/id/C0022797>
<http://linkedlifedata.com/resource/umls/id/C2931675>
<http://linkedlifedata.com/resource/umls/id/CN205864>
<http://linkedlifedata.com/resource/umls/id/C1850100>
<http://linkedlifedata.com/resource/umls/id/C1868594>
<http://linkedlifedata.com/resource/umls/id/CN203548>
<http://linkedlifedata.com/resource/umls/id/C0236642>
<http://linkedlifedata.com/resource/umls/id/C0752121>
<http://linkedlifedata.com/resource/umls/id/C1860872>
<http://linkedlifedata.com/resource/umls/id/C0041341>
<http://linkedlifedata.com/resource/umls/id/C0393576>
<http://linkedlifedata.com/resource/umls/id/C0022340>
<http://linkedlifedata.com/resource/umls/id/C0268281>
<http://linkedlifedata.com/resource/umls/id/C2931673>
<http://linkedlifedata.com/resource/umls/id/C1859098>
<http://linkedlifedata.com/resource/umls/id/C0018523>
<http://linkedlifedata.com/resource/umls/id/C1855588>
<http://linkedlifedata.com/resource/umls/id/C3179455>
<http://linkedlifedata.com/resource/umls/id/C0220756>
<http://linkedlifedata.com/resource/umls/id/C0038522>
<http://linkedlifedata.com/resource/umls/id/CN201680>
<http://linkedlifedata.com/resource/umls/id/C1850077>
<http://linkedlifedata.com/resource/umls/id/C1845165>
<http://linkedlifedata.com/resource/umls/id/C2677888>
<http://linkedlifedata.com/resource/umls/id/C3806722>
<http://linkedlifedata.com/resource/umls/id/C0796195>
<http://linkedlifedata.com/resource/umls/id/C1839130>
<http://linkedlifedata.com/resource/umls/id/C1839022>
<http://linkedlifedata.com/resource/umls/id/C1838867>
<http://linkedlifedata.com/resource/umls/id/C0206042>
<http://linkedlifedata.com/resource/umls/id/C1838327>
<http://linkedlifedata.com/resource/umls/id/C0338462>
<http://linkedlifedata.com/resource/umls/id/C0751706>
<http://linkedlifedata.com/resource/umls/id/C4011788>
<http://linkedlifedata.com/resource/umls/id/C1833296>
<http://linkedlifedata.com/resource/umls/id/CN201679>
<http://linkedlifedata.com/resource/umls/id/C0038868>
<http://linkedlifedata.com/resource/umls/id/CN205522>
<http://linkedlifedata.com/resource/umls/id/C1865868>
<http://linkedlifedata.com/resource/umls/id/C1865581>
<http://linkedlifedata.com/resource/umls/id/C1864112>
<http://linkedlifedata.com/resource/umls/id/C1858751>
<http://linkedlifedata.com/resource/umls/id/C1858114>
<http://linkedlifedata.com/resource/umls/id/C1857933>
<http://linkedlifedata.com/resource/umls/id/C1854182>
<http://linkedlifedata.com/resource/umls/id/C1853578>
<http://linkedlifedata.com/resource/umls/id/C1853555>
<http://linkedlifedata.com/resource/umls/id/C1853445>
<http://linkedlifedata.com/resource/umls/id/C1847987>
<http://linkedlifedata.com/resource/umls/id/C1847650>
<http://linkedlifedata.com/resource/umls/id/C1847640>
<http://linkedlifedata.com/resource/umls/id/C1847360>
<http://linkedlifedata.com/resource/umls/id/C1847200>
<http://linkedlifedata.com/resource/umls/id/C1846735>
<http://linkedlifedata.com/resource/umls/id/C1843366>
<http://linkedlifedata.com/resource/umls/id/C1843211>
<http://linkedlifedata.com/resource/umls/id/C1837149>
<http://linkedlifedata.com/resource/umls/id/C1836148>
<http://linkedlifedata.com/resource/umls/id/C1864828>
<http://linkedlifedata.com/resource/umls/id/C1853360>
<http://linkedlifedata.com/resource/umls/id/C1864923>
<http://linkedlifedata.com/resource/umls/id/C1970476>
<http://linkedlifedata.com/resource/umls/id/C1970209>
"""
diseaseList3 = """
<http://linkedlifedata.com/resource/umls/id/C1970147>
<http://linkedlifedata.com/resource/umls/id/C1970144>
<http://linkedlifedata.com/resource/umls/id/C1970143>
<http://linkedlifedata.com/resource/umls/id/C2673257>
<http://linkedlifedata.com/resource/umls/id/C0027877>
<http://linkedlifedata.com/resource/umls/id/C2677567>
<http://linkedlifedata.com/resource/umls/id/C3502417>
<http://linkedlifedata.com/resource/umls/id/C2751067>
<http://linkedlifedata.com/resource/umls/id/C2751012>
<http://linkedlifedata.com/resource/umls/id/C2750442>
<http://linkedlifedata.com/resource/umls/id/CN035550>
<http://linkedlifedata.com/resource/umls/id/C3280133>
<http://linkedlifedata.com/resource/umls/id/C3280271>
<http://linkedlifedata.com/resource/umls/id/C3539123>
<http://linkedlifedata.com/resource/umls/id/C3554452>
<http://linkedlifedata.com/resource/umls/id/C3715049>
<http://linkedlifedata.com/resource/umls/id/C3809468>
<http://linkedlifedata.com/resource/umls/id/C3809469>
<http://linkedlifedata.com/resource/umls/id/C3809811>
<http://linkedlifedata.com/resource/umls/id/C4510873>
<http://linkedlifedata.com/resource/umls/id/CN204972>
<http://linkedlifedata.com/resource/umls/id/C3809824>
<http://linkedlifedata.com/resource/umls/id/C3810041>
<http://linkedlifedata.com/resource/umls/id/C3810349>
<http://linkedlifedata.com/resource/umls/id/C4225353>
<http://linkedlifedata.com/resource/umls/id/C4225326>
<http://linkedlifedata.com/resource/umls/id/C4225238>
<http://linkedlifedata.com/resource/umls/id/C4225186>
<http://linkedlifedata.com/resource/umls/id/C4310791>
<http://linkedlifedata.com/resource/umls/id/C4310743>
<http://linkedlifedata.com/resource/umls/id/C0206716>
<http://linkedlifedata.com/resource/umls/id/C0022336>
<http://linkedlifedata.com/resource/umls/id/CN201109>
<http://linkedlifedata.com/resource/umls/id/CN201110>
<http://linkedlifedata.com/resource/umls/id/CN201112>
<http://linkedlifedata.com/resource/umls/id/CN201113>
<http://linkedlifedata.com/resource/umls/id/CN201114>
<http://linkedlifedata.com/resource/umls/id/CN201115>
<http://linkedlifedata.com/resource/umls/id/CN201116>
<http://linkedlifedata.com/resource/umls/id/CN201371>
<http://linkedlifedata.com/resource/umls/id/CN226961>
<http://linkedlifedata.com/resource/umls/id/CN201681>
<http://linkedlifedata.com/resource/umls/id/CN226962>
<http://linkedlifedata.com/resource/umls/id/C0751208>
<http://linkedlifedata.com/resource/umls/id/CN201979>
<http://linkedlifedata.com/resource/umls/id/C0431112>
<http://linkedlifedata.com/resource/umls/id/C0751668>
<http://linkedlifedata.com/resource/umls/id/C0751669>
<http://linkedlifedata.com/resource/umls/id/C0751670>
<http://linkedlifedata.com/resource/umls/id/C4303482>
<http://linkedlifedata.com/resource/umls/id/CN202723>
<http://linkedlifedata.com/resource/umls/id/CN203142>
<http://linkedlifedata.com/resource/umls/id/CN203531>
<http://linkedlifedata.com/resource/umls/id/CN203533>
<http://linkedlifedata.com/resource/umls/id/C0677050>
<http://linkedlifedata.com/resource/umls/id/C0393565>
<http://linkedlifedata.com/resource/umls/id/CN203536>
<http://linkedlifedata.com/resource/umls/id/CN227172>
<http://linkedlifedata.com/resource/umls/id/CN203776>
<http://linkedlifedata.com/resource/umls/id/C4518776>
<http://linkedlifedata.com/resource/umls/id/CN226138>
<http://linkedlifedata.com/resource/umls/id/C0393911>
<http://linkedlifedata.com/resource/umls/id/C2931939>
<http://linkedlifedata.com/resource/umls/id/CN205091>
<http://linkedlifedata.com/resource/umls/id/CN237752>
<http://linkedlifedata.com/resource/umls/id/C4303527>
<http://linkedlifedata.com/resource/umls/id/CN205270>
<http://linkedlifedata.com/resource/umls/id/C4275078>
<http://linkedlifedata.com/resource/umls/id/CN205712>
<http://linkedlifedata.com/resource/umls/id/C0014040>
<http://linkedlifedata.com/resource/umls/id/C1305924>
<http://linkedlifedata.com/resource/umls/id/CN206907>
<http://linkedlifedata.com/resource/umls/id/CN206908>
<http://linkedlifedata.com/resource/umls/id/CN207200>
<http://linkedlifedata.com/resource/umls/id/C0268248>
<http://linkedlifedata.com/resource/umls/id/C1321878>
<http://linkedlifedata.com/resource/umls/id/C0686352>
<http://linkedlifedata.com/resource/umls/id/CN248785>
"""
diseaseList4 = """
<http://linkedlifedata.com/resource/umls/id/CN239392>
<http://linkedlifedata.com/resource/umls/id/C3888102>
<http://linkedlifedata.com/resource/umls/id/CN239493>
<http://linkedlifedata.com/resource/umls/id/C0751587>
<http://linkedlifedata.com/resource/umls/id/C0015624>
<http://linkedlifedata.com/resource/umls/id/C0278619>
<http://linkedlifedata.com/resource/umls/id/C0032131>
<http://linkedlifedata.com/resource/umls/id/C0272256>
<http://linkedlifedata.com/resource/umls/id/C1332936>
<http://linkedlifedata.com/resource/umls/id/C0278620>
<http://linkedlifedata.com/resource/umls/id/C0456845>
<http://linkedlifedata.com/resource/umls/id/C3898125>
<http://linkedlifedata.com/resource/umls/id/C2049069>
<http://linkedlifedata.com/resource/umls/id/CN244395>
<http://linkedlifedata.com/resource/umls/id/C0017661>
<http://linkedlifedata.com/resource/umls/id/C0043195>
<http://linkedlifedata.com/resource/umls/id/C1334614>
<http://linkedlifedata.com/resource/umls/id/C1862937>
<http://linkedlifedata.com/resource/umls/id/C0431693>
<http://linkedlifedata.com/resource/umls/id/C2959918>
<http://linkedlifedata.com/resource/umls/id/CN206512>
<http://linkedlifedata.com/resource/umls/id/C0700299>
<http://linkedlifedata.com/resource/umls/id/C0206693>
<http://linkedlifedata.com/resource/umls/id/C0238462>
<http://linkedlifedata.com/resource/umls/id/CN239214>
<http://linkedlifedata.com/resource/umls/id/C3468114>
<http://linkedlifedata.com/resource/umls/id/CN239582>
<http://linkedlifedata.com/resource/umls/id/C1859806>
<http://linkedlifedata.com/resource/umls/id/C1859805>
<http://linkedlifedata.com/resource/umls/id/C0268164>
<http://linkedlifedata.com/resource/umls/id/C1848336>
<http://linkedlifedata.com/resource/umls/id/C4305530>
<http://linkedlifedata.com/resource/umls/id/CN206679>
<http://linkedlifedata.com/resource/umls/id/C0878681>
<http://linkedlifedata.com/resource/umls/id/C1839874>
<http://linkedlifedata.com/resource/umls/id/CN239269>
<http://linkedlifedata.com/resource/umls/id/C1845167>
<http://linkedlifedata.com/resource/umls/id/C4305529>
<http://linkedlifedata.com/resource/umls/id/C2677877>
<http://linkedlifedata.com/resource/umls/id/C2931035>
<http://linkedlifedata.com/resource/umls/id/C3275459>
<http://linkedlifedata.com/resource/umls/id/C0033375>
<http://linkedlifedata.com/resource/umls/id/C1832544>
<http://linkedlifedata.com/resource/umls/id/C1832387>
<http://linkedlifedata.com/resource/umls/id/C1865864>
<http://linkedlifedata.com/resource/umls/id/C1865409>
<http://linkedlifedata.com/resource/umls/id/C1863594>
<http://linkedlifedata.com/resource/umls/id/C4054550>
<http://linkedlifedata.com/resource/umls/id/C1858990>
<http://linkedlifedata.com/resource/umls/id/C4274391>
<http://linkedlifedata.com/resource/umls/id/C0002895>
<http://linkedlifedata.com/resource/umls/id/C1842674>
<http://linkedlifedata.com/resource/umls/id/C1842642>
<http://linkedlifedata.com/resource/umls/id/C1835934>
<http://linkedlifedata.com/resource/umls/id/C1970472>
<http://linkedlifedata.com/resource/umls/id/C1970456>
<http://linkedlifedata.com/resource/umls/id/C2678468>
<http://linkedlifedata.com/resource/umls/id/C0011880>
"""
diseaseList5 = """
<http://linkedlifedata.com/resource/umls/id/C2675491>
<http://linkedlifedata.com/resource/umls/id/C3150652>
<http://linkedlifedata.com/resource/umls/id/C3150692>
<http://linkedlifedata.com/resource/umls/id/C3150998>
<http://linkedlifedata.com/resource/umls/id/C3151432>
<http://linkedlifedata.com/resource/umls/id/C3280216>
<http://linkedlifedata.com/resource/umls/id/C3280226>
<http://linkedlifedata.com/resource/umls/id/C3280452>
<http://linkedlifedata.com/resource/umls/id/C3280574>
<http://linkedlifedata.com/resource/umls/id/C3280587>
<http://linkedlifedata.com/resource/umls/id/C1836076>
<http://linkedlifedata.com/resource/umls/id/C3553719>
<http://linkedlifedata.com/resource/umls/id/C3553816>
<http://linkedlifedata.com/resource/umls/id/C3809327>
<http://linkedlifedata.com/resource/umls/id/C3715156>
<http://linkedlifedata.com/resource/umls/id/C3809651>
<http://linkedlifedata.com/resource/umls/id/C3715155>
<http://linkedlifedata.com/resource/umls/id/C3810326>
<http://linkedlifedata.com/resource/umls/id/C4014261>
<http://linkedlifedata.com/resource/umls/id/C4014648>
<http://linkedlifedata.com/resource/umls/id/C4014962>
<http://linkedlifedata.com/resource/umls/id/C4015183>
<http://linkedlifedata.com/resource/umls/id/C4015301>
<http://linkedlifedata.com/resource/umls/id/C4015505>
<http://linkedlifedata.com/resource/umls/id/C4015512>
<http://linkedlifedata.com/resource/umls/id/C4225325>
<http://linkedlifedata.com/resource/umls/id/C4225211>
<http://linkedlifedata.com/resource/umls/id/C4310741>
<http://linkedlifedata.com/resource/umls/id/CN197327>
<http://linkedlifedata.com/resource/umls/id/CN197328>
<http://linkedlifedata.com/resource/umls/id/C4274082>
<http://linkedlifedata.com/resource/umls/id/C3873302>
<http://linkedlifedata.com/resource/umls/id/CN237633>
<http://linkedlifedata.com/resource/umls/id/C0917981>
<http://linkedlifedata.com/resource/umls/id/C0085253>
<http://linkedlifedata.com/resource/umls/id/CN206037>
<http://linkedlifedata.com/resource/umls/id/C4510369>
<http://linkedlifedata.com/resource/umls/id/CN206457>
<http://linkedlifedata.com/resource/umls/id/C1333947>
<http://linkedlifedata.com/resource/umls/id/CN206635>
<http://linkedlifedata.com/resource/umls/id/CN206636>
<http://linkedlifedata.com/resource/umls/id/C0238239>
<http://linkedlifedata.com/resource/umls/id/CN248786>
<http://linkedlifedata.com/resource/umls/id/C0334574>
<http://linkedlifedata.com/resource/umls/id/C2931773>
<http://linkedlifedata.com/resource/umls/id/C0403434>
<http://linkedlifedata.com/resource/umls/id/CN778765>
<http://linkedlifedata.com/resource/umls/id/CN842244>
<http://linkedlifedata.com/resource/umls/id/CN895594>
<http://linkedlifedata.com/resource/umls/id/C0341702>
<http://linkedlifedata.com/resource/umls/id/C2930859>
"""

diseaseList = (diseaseList1,diseaseList2,diseaseList3,diseaseList4,diseaseList5)
resultStr=""

for elt in diseaseList : 
    query = """
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
        PREFIX void: <http://rdfs.org/ns/void#>
        PREFIX sio: <http://semanticscience.org/resource/>
        PREFIX ncit: <http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl#>
        PREFIX up: <http://purl.uniprot.org/core/>
        PREFIX dcat: <http://www.w3.org/ns/dcat#>
        PREFIX dctypes: <http://purl.org/dc/dcmitype/>
        PREFIX wi: <http://http://purl.org/ontology/wi/core#>
        PREFIX eco: <http://http://purl.obolibrary.org/obo/eco.owl#>
        PREFIX prov: <http://http://http://www.w3.org/ns/prov#>
        PREFIX pav: <http://http://http://purl.org/pav/>
        PREFIX obo: <http://purl.obolibrary.org/obo/> 
        SELECT DISTINCT ?protein
        WHERE {
        VALUES ?sub {"""
    query+= elt
    query += """
    }
        ?sub skos:exactMatch ?diseaseMain .
        ?diseaseG rdfs:subClassOf* ?diseaseMain .  
        ?disease skos:exactMatch ?diseaseG .
        ?gda sio:SIO_000628 ?gene,?disease .
        ?gda sio:SIO_000216 ?valueR .  ?valueR sio:SIO_000300 ?gdaScore .  FILTER (?gdaScore >= 0.2) 
        ?gene rdf:type ncit:C16612 ;
        sio:SIO_010078 ?protein .
        ?protein skos:exactMatch ?uniprot .
        FILTER regex(?uniprot, "http://purl.uniprot.org/uniprot/")  
        }
        """


    sparql = SPARQLWrapper("http://rdf.disgenet.org/sparql/")
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    print("Executing Query")
    results = sparql.query().convert()

    for result in results["results"]["bindings"]:
        uri=result["protein"]["value"]
        uri=uri.replace("http://identifiers.org/uniprot/","")
        resultStr+=uri+"\n"

for elt in diseaseList : 
    print("Starting query for variants")
    query2 = """
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
        PREFIX void: <http://rdfs.org/ns/void#>
        PREFIX sio: <http://semanticscience.org/resource/>
        PREFIX ncit: <http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl#>
        PREFIX up: <http://purl.uniprot.org/core/>
        PREFIX dcat: <http://www.w3.org/ns/dcat#>
        PREFIX dctypes: <http://purl.org/dc/dcmitype/>
        PREFIX wi: <http://http://purl.org/ontology/wi/core#>
        PREFIX eco: <http://http://purl.obolibrary.org/obo/eco.owl#>
        PREFIX prov: <http://http://http://www.w3.org/ns/prov#>
        PREFIX pav: <http://http://http://purl.org/pav/>
        PREFIX obo: <http://purl.obolibrary.org/obo/> 
        SELECT DISTINCT ?protein
        WHERE {
        VALUES ?sub {
        """
    query2 += elt
    query2 += """
    }
        ?sub skos:exactMatch ?diseaseMain .
        ?diseaseG rdfs:subClassOf* ?diseaseMain .  
        ?disease skos:exactMatch ?diseaseG .
        ?vda sio:SIO_000628 ?variant,?disease .
        ?vda sio:SIO_000216 ?valueR .  ?valueR sio:SIO_000300 ?vdaScore .  FILTER (?vdaScore >= 0.2) 
        ?variant sio:SIO_000216 ?spe,?pleio .
        ?variant so:associated_with ?gene .
        ?gene rdf:type ncit:C16612 ;
        sio:SIO_010078 ?protein .
        ?protein skos:exactMatch ?uniprot .
        FILTER regex(?uniprot, "http://purl.uniprot.org/uniprot/")  
        }
        """

    sparql.setQuery(query2)
    sparql.setReturnFormat(JSON)
    results2 = sparql.query().convert()

    for result in results2["results"]["bindings"]:
        uri=result["protein"]["value"]
        uri=uri.replace("http://identifiers.org/uniprot/","")
        resultStr+=uri+"\n"

print("Writing results into file")

with open("results/disgenet_related_idlist.txt", "w") as f: 
        f.write(resultStr)