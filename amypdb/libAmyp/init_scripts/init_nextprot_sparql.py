
from SPARQLWrapper import SPARQLWrapper, JSON

sparql = SPARQLWrapper("https://sparql.nextprot.org")
sparql.setQuery("""
PREFIX : <http://nextprot.org/rdf#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
select distinct ?prot where {
        {?entry :isoform / :generalAnnotation / :term / rdfs:label ?label . } 
        UNION {?entry :isoform / :keyword / :term / rdfs:label ?label . }
        UNION {?entry :isoform / :disease / :term / rdfs:label ?label . } 
        UNION { ?entry :isoform / :generalAnnotation / rdfs:comment ?label }
        ?entry skos:exactMatch ?prot
        filter (regex(str(?prot), "uniprot","i")) 
        filter (regex(?label, "amylo(id|gen|do)"@en,"i"))}

        """)
sparql.setReturnFormat(JSON)
results = sparql.query().convert()

resultStr=""
for result in results["results"]["bindings"]:
        uri=result["prot"]["value"]
        uri=uri.replace("http://purl.uniprot.org/uniprot/","")
        resultStr+=uri+"\n"


with open("results/nextprot_idlist.txt", "w") as f: 
        f.write(resultStr)


