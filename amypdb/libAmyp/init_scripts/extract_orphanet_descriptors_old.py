from rdflib import Graph
from SPARQLWrapper import SPARQLWrapper,JSON,TURTLE,N3,POSTDIRECTLY

#TO DO : stocker variables à l'extérieur
default_graph = "http://www.orpha.net/ORDO/"
repository = "http://localhost:8080/rdf4j-server/repositories/external"
sparql = SPARQLWrapper(repository)

#RETRIEVING GROUP OF DISORDERS
cq = """\
PREFIX owl:<http://www.w3.org/2002/07/owl#>
PREFIX oboInOwl:<http://www.geneontology.org/formats/oboInOwl#>
PREFIX obo1:<http://purl.obolibrary.org/obo/>
PREFIX skos:<http://www.w3.org/2004/02/skos/core#>
PREFIX efo:<http://www.ebi.ac.uk/efo/>
PREFIX obo:<http://data.bioontology.org/metadata/obo/>
SELECT DISTINCT *
WHERE {
{ 
?orpha rdfs:label  ?label . 
} UNION {
?orpha skos:prefLabel ?label .
} UNION {
?orpha efo:definition | efo:alternative_term ?label .
}
OPTIONAL {?orpha2 rdfs:subClassof* ?orpha}
OPTIONAL {?orpha2 obo:part_of ?orpha}

FILTER (regex(?label, "amyloid|prion","i"))
FILTER NOT EXISTS {?orpha ?p ?label . FILTER (regex(?label,"non-amyloid","i")) }
FILTER NOT EXISTS {?orpha2 efo:definition | efo:alternative_term ?label2 . FILTER (regex(?label2, "non-amyloid","i"))}
}"""

print("querying candidate terms")

sparql.setRequestMethod(POSTDIRECTLY)
sparql.setReturnFormat(JSON)
sparql.setQuery(cq)

results = sparql.query().convert()


uriList=[]
for result in results["results"]["bindings"]:
    if result["orpha"]["value"] not in uriList :
        uriList.append(result["orpha"]["value"])
    if "orpha2" in result :
        if result["orpha2"]["value"] not in uriList :
            uriList.append(result["orpha2"]["value"]) 




queryInfo = """
PREFIX owl:<http://www.w3.org/2002/07/owl#>
PREFIX oboInOwl:<http://www.geneontology.org/formats/oboInOwl#>
PREFIX obo1:<http://purl.obolibrary.org/obo/>
PREFIX skos:<http://www.w3.org/2004/02/skos/core#>
PREFIX efo:<http://www.ebi.ac.uk/efo/>
PREFIX obo:<http://data.bioontology.org/metadata/obo/>
CONSTRUCT { ?s ?p ?o . ?s rdfs:type ?type . }
WHERE { VALUES ?s { """

for item in uriList :
    queryInfo += "<"+item+"> "

queryInfo += """ } 
?s ?p ?o .
?s rdfs:subClassOf* ?type .
VALUES ?type {<http://www.orpha.net/ORDO/Orphanet_377794> <http://www.orpha.net/ORDO/Orphanet_C010> <http://www.orpha.net/ORDO/Orphanet_377795> <http://www.orpha.net/ORDO/Orphanet_377788> <http://www.orpha.net/ORDO/Orphanet_377792> <http://www.orpha.net/ORDO/Orphanet_377796> }
}"""




print("executing second query")

sparql.setRequestMethod(POSTDIRECTLY)
sparql.setReturnFormat(N3) 
sparql.setQuery(queryInfo)
results=sparql.query().convert()

#g = Graph()
#g.parse(data=results, format="turtle")

print("writing results")

with open("results/orphanet.ttl","w") as file :
    file.write(results.decode("UTF-8")) 




queryInfo2 = """
PREFIX owl:<http://www.w3.org/2002/07/owl#>
PREFIX oboInOwl:<http://www.geneontology.org/formats/oboInOwl#>
PREFIX obo1:<http://purl.obolibrary.org/obo/>
PREFIX skos:<http://www.w3.org/2004/02/skos/core#>
PREFIX efo:<http://www.ebi.ac.uk/efo/>
PREFIX obo:<http://data.bioontology.org/metadata/obo/>
CONSTRUCT { ?s ?p ?o . ?s rdfs:type ?type . }
WHERE { ?s ?p ?o .
        ?s rdfs:subClassOf* ?type .      
{ 
?s rdfs:label  ?label . 
} UNION {
?s skos:prefLabel ?label .
} UNION {
?s efo:definition | efo:alternative_term ?label .
}
        FILTER (regex(?label, "Transthyretin|Beta(-| )*2(-| )*microglobulin|Apolipoprotein A(-| )*I|Apolipoprotein A(-| )*II|Apolipoprotein A(-| )*(IV|4)|Apolipoprotein C(-| )*(II|2)|Gelsolin|Lysozyme C|Leukocyte cell-derived chemotaxin-2|Fibrinogen alpha chain|Cystatin(-| )*C|Integral membrane protein 2B|Amyloid-beta precursor protein|Alpha-synuclein|Major prion protein|Islet amyloid polypeptide|Natriuretic peptides A|Prolactin|Insulin|Lactadherin|Transforming growth factor-beta-induced protein ig-h3|Lactotransferrin","i"))  
FILTER NOT EXISTS {?s ?y ?label . FILTER (regex(?label,"non-amyloid","i")) }
        VALUES ?type { <http://www.orpha.net/ORDO/Orphanet_C010>  }
        }"""


sparql.setRequestMethod(POSTDIRECTLY)
sparql.setReturnFormat(N3) 
sparql.setQuery(queryInfo2)
results=sparql.query().convert()

#g = Graph()
#g.parse(data=results, format="turtle")

print("writing results")

with open("results/orphanet.ttl","a") as file :
    file.write(results.decode("UTF-8")) 
   # 69 : http://www.orpha.net/ORDO/Orphanet_377794
#newg = Graph().parse(data=result_graph.serialize(format='nt'))
#http://id.nlm.nih.gov/mesh/sparql


#RETRIEVING PREDICATES
#query2 = """\
#
#?p a <http://www.w3.org/2002/07/owl/ObjectProperty> .
#?p rdfs:label ?label .
#?p efo:definition ?def .
#"""


