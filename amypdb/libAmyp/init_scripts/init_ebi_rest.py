import requests

#Pour consulter l'aide ebi pour ecrire la requete : https://www.ebi.ac.uk/ebisearch/swagger.ebi
requestURL = "http://www.ebi.ac.uk/ebisearch/ws/rest/uniprot"
query = "amyloidogenic OR amyloid OR amylogenic OR amylogenesis OR amyloidosis OR prion"
myparam = { 'query': query, 'format':'acclist', 'facets':'status:Reviewed','domain':'uniprot', 'size':'1000'}
headers = {'content-type':'text/plain;charset=UTF-8'}
r = requests.get(requestURL,params=myparam, headers=headers)
print(r)
responseBody = r.text
print(r.status_code)
with open("results/ebi_idlist.txt", "w") as f:
        f.write(responseBody)
