import requests

#endpoint = "http://localhost:80/rdf4j-server/repositories/nextprot/statements"
#nextprot = "http://nextprot.org/rdf/entry/"
prot_file = "results/unique_ids_ok.txt"

prefixes = """
@prefix : <http://nextprot.org/rdf#>
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix up: <http://purl.uniprot.org/core/> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix entry: <http://nextprot.org/rdf/entry/> .
@prefix isoform: <http://nextprot.org/rdf/isoform/> .
@prefix annotation: <http://nextprot.org/rdf/annotation/> .
@prefix evidence: <http://nextprot.org/rdf/evidence/> .
@prefix xref: <http://nextprot.org/rdf/xref/> .
@prefix publication: <http://nextprot.org/rdf/publication/> .
@prefix identifier: <http://nextprot.org/rdf/identifier/> .
@prefix cv: <http://nextprot.org/rdf/terminology/> .
@prefix gene: <http://nextprot.org/rdf/gene/> .
@prefix source: <http://nextprot.org/rdf/source/> .
@prefix db: <http://nextprot.org/rdf/db/> .
@prefix context: <http://nextprot.org/rdf/context/> .
@prefix interaction: <http://nextprot.org/rdf/interaction/> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix uniprot: <http://purl.uniprot.org/uniprot/> .
@prefix unipage: <http://www.uniprot.org/uniprot/> .
@prefix proteoform: <http://nextprot.org/rdf/proteoform/> .
@prefix chebi: <http://purl.obolibrary.org/obo/> .
@prefix drugbankdrugs: <http://wifo5-04.informatik.uni-mannheim.de/drugbank/resource/drugs/> .
"""


with open(prot_file) as f:
    id_list= f.read().splitlines()


#r1 = requests.get('https://api.nextprot.org/entry/NX_P10279.ttl')
#print(r1)
#print(r1.status_code)

#print(len(id_list))
#print(len(id_list)/10)
dict={}

for i in range(0,9) :
    if i*392+392 > len(id_list) : 
        dict[i]=id_list[i*392:len(id_list)]
    else : 
        dict[i]=id_list[i*392:i*392+392]


#print(dict[2])

for k,v in dict.items() :
    content=prefixes
    for elt  in v  :
        r = requests.get('https://api.nextprot.org/entry/NX_{idu}.ttl'.format(idu=elt))
        #print(r.text)
        print(r.status_code, " : " , elt)
        if (r.status_code != 404) :
            content = content + "\n" + r.content.decode()
    name_file = "results/temporary_NX"+str(k)+".ttl"
    print(name_file)
    with open(name_file,"w") as temp : 
        temp.write(content)
        #with open("temporary_NX.ttl","a") as temp :
            #temp.write(r.content.decode())


