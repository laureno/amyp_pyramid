from amypdb.libAmyp.AmyConf import *
from amypdb.libAmyp.Thesaurus import *
from amypdb.libAmyp.Entry import *
import xml.etree.ElementTree as ET
import json
import requests
import os
from rdflib import URIRef, Literal,Graph
from rdflib.namespace import SKOS,RDF,RDFS,OWL,DC,XSD
from SPARQLWrapper import SPARQLWrapper,N3,JSON
from BioManager.bioSparqlService import *
from BioManager.bioEntity import *
from amypdb.libAmyp.Utils import *

#MONDO : http://purl.amypdb.org/model/Amyloidosis http://purl.amypdb.org/model/Amyloid_Related_Disease
#REACTOME : Amyloid_Pathway Amyloid_Complex
#GO : Amyloid_Process Amyloid_Function Amyloid_Complex
#PDB : Amyloid_Fibril Amyloid_Structure Amyloid_Like_Structure
#AMYLOAD : Amyloidogenic_Fragment


    # ECO:0000000 evidence
    #     ECO:0000006 experimental evidence
    #     ECO:0000041 similarity evidence
    #     ECO:0000088 biological system reconstruction evidence
    #     ECO:0000177 genomic context evidence
    #     ECO:0000204 author statement
    #     ECO:0000212 combinatorial evidence
    #     ECO:0000311 imported information
    #     ECO:0000352 evidence used in manual assertion
    #     ECO:0000361 inferential evidence
    #     ECO:0000501 evidence used in automatic assertion
    #     ECO:0006055 high throughput evidence
    #     ECO:0007672 computational evidence
    # ECO_0000366 inference automatic annotation automatic assertion
    #ECO:0005561 pairwise sequence alignment evidence used in automatic assertion
    #ECO:0007815 structure determination evidence used in automatic assertion

prot_file="data/init_uniprot_id.txt"
prot_file2="data/new_uniprot_id.txt"
amyload_file = "data/Amyload/AmyLoad_all.xml"
ALBase_file = "data/ALBase/AL_sequences.csv"
isa_file_human = "data/ISA/ISA_human_vir.csv"
isa_file_animal = "data/ISA/ISA_animals_id.txt"
amypro_file = "data/AmyPro/amypro.json"
funct_amy_file = "data/Articles/Functional_prot_id.txt"
site_json_file = "../static/browse_count.json"
gene_trans_file = "data/Gene_transcripts.txt" #Gene stable ID,Transcript stable ID,Protein stable ID,Gene description,Gene name
gene_tr_prot_file = "data/Gene_Map_From_Uni_Retrieve.csv"  #geneId;protId;geneName;geneSyn;transInfo;taxonId;geneId
#rfAmyloid_file = "data/RFAmyloid/results.txt"
Waltz_file = "data/WaltzDb/waltzdb_export_2020_10_20.json"
blackList=["Q16143"]

def createEntriesFromUniprot():
    #prot_file = "init_scripts/results/unique_ids_ok.txt"
    with open(prot_file) as f:
        id_list= f.read().splitlines()
    #dict={}

    #for i in range(0,9) :
    #    if i*440+440 > len(id_list) : 
    #        dict[i]=id_list[i*440:len(id_list)]
    #    else :
    #        dict[i]=id_list[i*440:i*440+440]
    
    # for elt in id_list[0:250] :
    
    cp=0
    for elt in id_list[:100] :
        cp += 1
        print(cp)
        p=Protein(elt)
        p.create()

def createEntriesFromAmyload():
    tree = ET.parse(amyload_file)  
    root = tree.getroot()
    for fragment in root:
        sequence = fragment.find('sequence').text
        fragId = id_from_seq(sequence)
        #fragId = fragment.find('id').text
        frag = Fragment(fragId)
        if not frag.exists() :
            frag.create(fragId,sequence,"Amyload")

def createEntriesFromALBase():
    with open(ALBase_file) as f:
        for line in f : 
            info = line.split(";")
            alName = info[2].strip()
            #alId = alName.replace("*","_")
            #alId = alId.replace("-","_")
            #fragId = "ALBase_"+alId
            sequence = info[1].strip()
            fragId = id_from_seq(sequence)
            frag = Fragment(fragId)
            if not frag.exists() :
                frag.create(fragId,sequence,"ALBase")
            else : 
                frag.addSource("ALBase")
      
def createEntriesFromWaltz():
    with open(Waltz_file) as json_file:
        data = json.load(json_file)
        for elt in data :
            sequence = elt["Sequence"]
            fragId = id_from_seq(sequence)
            frag = Fragment(fragId)
            seeAlso = "http://waltzdb.switchlab.org/sequences/"+sequence
            if not frag.exists() :
                frag.create(fragId,sequence,"Waltz-DB",seeAlso)
            else : 
                frag.addSource("Waltz-DB")
                
                
def addAnnotForWaltz():
    with open(Waltz_file) as json_file:
        data = json.load(json_file)
        for elt in data :
            sequence = elt["Sequence"]
            fragId = id_from_seq(sequence)   
            frag = Fragment(fragId)
            reference = frag.uri
            source="Waltz-DB"
            evidence = "ECO_0000006"
            annotId = fragId+"_"+source+"_"+evidence
            annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloidogenic"))
            if not annot.exists() :
                description= "Fibrilation detected experimentally. Structural information for this sequence is available on Waltz-DB"
                annot.create(evidence,reference,source,description)

def addAnnotForAmyload():
    tree = ET.parse(amyload_file)
    root = tree.getroot()
    for fragment in root:
        sequence = fragment.find('sequence').text
        fragId = id_from_seq(sequence)   
        frag = Fragment(fragId)
        reference = frag.uri
        source="Amyload"
        evidence = "ECO_0000006"
        annotId = fragId+"_"+source+"_"+evidence
        annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloidogenic"))

        if not annot.exists() :
            description= "Fibrilation detected experimentally on a fragment"
            if fragment.find('solution').text != None : 
                description += " | Solution : " + fragment.find('solution').text
            description += " | Method : "
            for method in fragment.iter(tag='method') :
                description += method.find('name').text + ","
            description = description[:-1]
            if fragment.find('protein').find('name').text != None :
                description += " | Protein : " + fragment.find('protein').find('name').text
            cp=0
            for disease in fragment.iter(tag='disease') :
                if cp < 1 : 
                    description += " | Associated diseases : "
                cp += 1
                description += disease.find('name').text + ","
            description = description[:-1]
            annot.create(evidence,reference,source,description)

            pubList=[]
            for ref in fragment.iter(tag='reference') :
                pubList.append(ref.find('url').text) 
            annot.addSupportingEvi(source,"ECO_0000313",pubList)


def addAnnotForALBase():
    with open(ALBase_file) as f:
        for line in f : 
            info = line.split(";")
            alName = info[2].strip()
            sequence = info[1].strip()
            fragId = id_from_seq(sequence)   
            frag = Fragment(fragId)
            reference = frag.uri
            source="ALBase"
            evidence = "ECO_0000006"
            annotId = fragId+"_"+source+"_"+evidence
            annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloidogenic"))

            if not annot.exists() :
                description= "Also known as : " + alName + ". Fibrilation detected experimentally. Reported to form fibrillar deposits in AL patients."
                annot.create(evidence,reference,source,description)



def queryAndCreateAnnot(query,endpoint,annotType,evidence,description,source):
    sparql = SPARQLWrapper(endpoint)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results=sparql.query().convert()
    dict={}
    for row in results["results"]["bindings"]:            
        annotId  = row["entityId"]["value"]+"_"+row["indicatorId"]["value"] + "_" +source+"_"+evidence            
        annot = Annotation(annotId,annotType)
        if not annot.exists() :
            annot.create(evidence,URIRef(row["entity"]["value"]),source,description,URIRef(row["indicator"]["value"]))
            dict[annot] = row["indicator"]["value"]
    return dict
    
    
def addAnnotForMetamyl() : 
    sparql = SPARQLWrapper(AMQUERY)
    
    queryHotSpots = """
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    SELECT distinct ?hs ?hsId ?seq 
    WHERE { ?hs rdf:type model:HotSpot  .
    ?hs model:accession ?hsId .
    ?hs model:primStruct ?primStruct .
    ?primStruct model:sequence ?seq .
    ?hs model:hexaMaxScore ?score .
    FILTER (?score > 0.8)
    }"""
    description = "Presence of a hotspot with an hexapeptide max score > 0.8 "
    sparql.setQuery(queryHotSpots)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    entityList1 = []
    evidence = "ECO_0005561"
    source = "Metamyl"
    
    for row in results["results"]["bindings"]:
        dict = {}
        dict["entity"] = row["hs"]["value"]
        dict["entityId"] = row["hsId"]["value"]
        dict["seq"] = row["seq"]["value"]
        entityList1.append(dict)
    
    for entityDict in entityList1 : 
        seq = entityDict["seq"]
        seqId = id_from_seq(seq)
        annotId  = entityDict["entityId"]+"_"+ seqId +"_"+source+"_"+evidence          
        annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloidogenic"))
        if not annot.exists() :                   
            annot.create(evidence,URIRef(entityDict["entity"]),source,description,Literal(entityDict["seq"]))
  
  
    print("query hotspots and annotate proteins")    
    queryHotSpots2 = """
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    SELECT distinct ?entity ?entityId ?seq 
    WHERE { ?hs rdf:type model:HotSpot  .
    ?hs model:primStruct ?primStruct .
    ?primStruct model:sequence ?seq .
    ?hs model:hexaMaxScore ?score .
    FILTER (?score > 0.8)
    ?iso model:hotSpot ?hs .
    ?entity model:isoform ?iso .
    ?entity model:accession ?entityId .
    }"""
    description = "Presence of a hotspot with an hexapeptide max score > 0.8 in one of the isoforms of the protein "
    sparql.setQuery(queryHotSpots2)
    sparql.setReturnFormat(JSON)
    results2 = sparql.query().convert()
    entityList2 = []
    source = "Metamyl"
    evidence = "ECO_0005561"
    
    for row in results2["results"]["bindings"]:
        dict = {}
        dict["entity"] = row["entity"]["value"]
        dict["entityId"] = row["entityId"]["value"]
        dict["seq"] = row["seq"]["value"]
        entityList2.append(dict)
    
    for entityDict in entityList2 : 
        seq = entityDict["seq"]
        seqId = id_from_seq(seq)
        annotId  = entityDict["entityId"]+"_"+ seqId +"_"+source+"_"+evidence          
        annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid_Candidate"))
        if not annot.exists() :                   
            annot.create(evidence,URIRef(entityDict["entity"]),source,description,Literal(entityDict["seq"]))


def inferFromFragment():

    sparql = SPARQLWrapper(AMQUERY)


    #for each sequence from polypeptide, check if it contains an amyloidogenic fragment
    #if if does, add a amyloid annot source amypdb, reference inferred, description 

    #PREFIX search: <http://www.openrdf.org/contrib/lucenesail#>
    #queryFragSeq = """
        # PREFIX model: <http://purl.amypdb.org/model/> 
        # PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
        # SELECT ?indicatorId ?indicator ?amyseq 
        # WHERE { ?indicator rdf:type model:Fragment ; model:accession ?indicatorId ; model:primStruct ?primStructF . 
        # ?primStructF model:sequence ?amyseq .
        # }"""

    queryFrag = """
PREFIX search: <http://www.openrdf.org/contrib/lucenesail#> 
        PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
        SELECT ?entity ?entityId ?seq ?source
        WHERE { ?entity rdf:type model:Isoform ; model:accession ?entityId ; model:amyFragment ?frag . 
        ?frag model:primStruct ?primStruct . ?primStruct model:sequence ?seq . ?frag model:source ?source .
        } """

    sparql.setQuery(queryFrag)
    sparql.setReturnFormat(JSON)
    results3 = sparql.query().convert()

    fragList = []
    for row in results3["results"]["bindings"]:
        dict = {}
        dict["entity"] = row["entity"]["value"]
        dict["entityId"] = row["entityId"]["value"]
        dict["seq"] = row["seq"]["value"]
        dict["source"] = row["source"]["value"]
        fragList.append(dict)

    description = "Presence of a sequence reported to form fibrillar deposits"
    evidence = "ECO_0005561"
    


    for fragDict in fragList :         
        source =  STR_AMSOURCE + "/" + fragDict["source"]
        sourceURI = STR_AMSOURCE + "_" + fragDict["source"]
        annotId  = fragDict["entityId"]+"_"+ "_"+sourceURI+"_"+evidence          
        annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid_Candidate"))
        if not annot.exists() :                   
            annot.create(evidence,URIRef(fragDict["entity"]),source,description,Literal(fragDict["seq"]))
    

    

def inferFromPDBWithQualifier(descriptorClue,descriptorResult,description,source,complEvi=None) : 
    pdbScheme = STR_AMTHES+PDB().name+"/scheme"

    queryQualif = """
    PREFIX model: <http://purl.amypdb.org/model/>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    SELECT ?entity ?entityId ?indicator ?indicatorId
    WHERE {{
    ?pdbRef skos:broadMatch <{desc}> .
    ?pdbRef skos:inScheme <{scheme}> .
    ?pdbRef model:accession ?indicatorId . 
    ?pdbRef owl:sameAs ?indicator . 
    ?terStruct model:pdb ?indicator .
    ?entity model:terStruct ?terStruct.
    ?entity rdf:type <{uri}> .
    ?entity model:accession ?entityId .
    }}""".format(uri=str(AMMODEL.Isoform),scheme=pdbScheme,desc=descriptorClue)

    dict = queryAndCreateAnnot(queryQualif,AMQUERY,descriptorResult,"ECO_0007815",description,source)

    for annot,indic in dict.items() : 
        print("add pub for PDB")
        sparql = SPARQLWrapper(PdbEndpoint().get_endpoint())
        query = "PREFIX dcterms:<http://purl.org/dc/terms/> \
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
        SELECT ?pubmed \
        WHERE {{ <{uri}> dcterms:references ?pubmed }} ".format(uri=indic)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        publiList = []
        for row in results["results"]["bindings"]:
            publiList.append(row["pubmed"]["value"])
        if len(publiList)<1 :
            publiList = None

        annot.addSupportingEvi(PDB().name,"ECO_0000313",publiList)

        if complEvi != None :
            source = complEvi[0]
            publiList = complEvi[1]
            annot.addSupportingEvi(source,"ECO_0000313",publiList)



def inferFromPDB():
    #for each isoform, check if it has a associated pdb structure annotaed with amyloid
    #if if does, add a amyloid annot source amypdb, reference inferred, description add pubref from pdb 
    print("for amyloid Fibril")
    inferFromPDBWithQualifier(STR_AMTHES+STR_AMNAME+"_Amyloid_Fibril",URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"),"This isoform is associated with a PDB structure described as an amyloid fibril","PDB")
    print("for amyloid Structure")
    inferFromPDBWithQualifier(STR_AMTHES+STR_AMNAME+"_Amyloid_Structure",URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"), "This isoform is associated with a PDB structure described as an amyloid structure","PDB")
    print("for amyloid like Structure")
    inferFromPDBWithQualifier(STR_AMTHES+STR_AMNAME+"_Amyloid_Like_Structure",URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid_Like"), "This isoform is associated with a PDB structure described as an amyloid-like structure","PDB")
  
    print("for label match")
    pdbScheme = STR_AMTHES+PDB().name+"/scheme"
    queryLabel = "PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    SELECT ?entity ?entityId ?indicator ?indicatorId \
    WHERE {{ ?entity rdf:type <{uri}> ; model:accession ?entityId ; model:thesRef ?thesRef . ?thesRef skos:prefLabel ?label . \
    FILTER( strlen( ?label ) > 6 ) .\
    ?pdbRef skos:broadMatch <{desc}> . \
    ?pdbRef skos:prefLabel ?pdbLabel . FILTER regex(?pdbLabel,?label,'i') . \
    ?pdbRef skos:inScheme <{scheme}> . ?pdbRef model:accession ?indicatorId . \
    ?pdbRef owl:sameAs ?indicator . }} ".format(uri=str(AMMODEL.Polypeptide),scheme=pdbScheme,desc=STR_AMTHES+STR_AMNAME+"_Amyloid_Fibril")
    description = "The name of this polypeptide matches the name of a PDB structure described as an amyloid fibril"
    dict = queryAndCreateAnnot(queryLabel,AMQUERY,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"),"ECO_0000363",description,PDB().name)


def inferFromGo():

    goScheme = STR_AMTHES+Go().name+"/scheme"

    query = "PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#> \
    SELECT ?entity ?entityId ?indicator ?indicatorId \
    WHERE {{ ?entity  model:bioProcess  ?indicRef . \
    ?entity model:accession ?entityId . \
    ?indicRef skos:broadMatch <{desc}> . \
    ?indicRef skos:inScheme <{scheme}> .  \
    ?indicRef owl:sameAs ?indicator . \
    ?indicRef model:accession ?indicatorId . }} ".format(scheme=goScheme,desc=STR_AMTHES+STR_AMNAME+"_Amyloid_Process")

    evidence = "ECO_0000362"
    description = "This protein is involved in a biological process related to amyloidy"

    dict= queryAndCreateAnnot(query,AMQUERY,URIRef(STR_AMTHES+STR_AMNAME+"_Amylome_Interactor"),"ECO_0007815",description,Uniprot().name)

    for annot,indic in dict.items() : 
        sparql = SPARQLWrapper(UNIQUERY)
        queryPub = "PREFIX unicore:<http://purl.uniprot.org/core/> \
                PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#> \
                PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> \
                PREFIX skos:<http://www.w3.org/2004/02/skos/core#> \
                SELECT distinct ?pubmed \
                WHERE {{ \
                ?reif a rdf:Statement ; \
                rdf:subject     <{pid}>; \
                rdf:predicate   unicore:annotation ; \
                rdf:object      <{go}>  ; \
                unicore:attribution  ?attribution . \
                ?attribution unicore:source ?citation . \
                ?citation skos:exactMatch ?pubmed . \
                }}".format(pid = annot.targetUri,go=indic)

        sparql.setQuery(queryPub)
        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()

        publiList = []
        for row in results["results"]["bindings"]:
            publiList.append(row["pubmed"]["value"])
        if len(publiList)<1 :
            publiList = None

        annot.addSupportingEvi(Uniprot().name,"ECO_0000311",publiList)

def addHereditary(dict) : 

    for annot,indic in dict.items() : 

        query = "PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    ASK {{ ?disRef rdf:type skos:Concept .  \
    ?disRef owl:sameAs <{indic}> . \
    ?disRef skos:prefLabel|skos:altLabel ?label FILTER regex(?label,'hereditary|familial','i') \
    }} ".format(indic=indic)
        headers = {'content-type' : 'application/x-www-form-urlencoded'}
        myparam = { 'query': query }
        r=requests.get(AMQUERY,myparam,headers=headers)    
        labelExists = r.json()['boolean']
        if labelExists : 
            annot.addQualifier(URIRef(STR_AMTHES+STR_AMNAME+"_Hereditary"))


def inferFromMondo():

    #First, we annotate amyloidosis and amyloid-related disease

    MondoScheme = STR_AMTHES+Mondo().name+"/scheme"

    query = "PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    SELECT ?entity ?entityId ?indicator ?indicatorId \
    WHERE {{ ?entity  rdf:type  <{entityType}> . \
    ?entity model:thesRef ?disRef . \
    ?entity model:accession ?entityId . \
    ?disRef skos:broadMatch <{desc}> . \
    ?disRef skos:inScheme <{scheme}> .  \
    ?disRef model:accession ?indicatorId . \
    ?disRef owl:sameAs ?indicator . \
    }} ".format(scheme=MondoScheme,desc=STR_AMTHES+STR_AMNAME+"_Amyloidosis",entityType = AMMODEL.Disease)

    description = "This disease is classified as an amyloidosis in the MONDO ontology"

    dict1= queryAndCreateAnnot(query,AMQUERY,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloidosis"),"ECO_0000363",description,Mondo().name)

    print("query Amyloidosis ended, now trying to get hereditary type")

    addHereditary(dict1)

    print("now trying to get Amyloid related diseases")

    #Find amyloid related disease
    query2 = "PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    SELECT ?entity ?entityId ?indicator ?indicatorId \
    WHERE {{ ?entity  rdf:type  <{entityType}> . \
    ?entity model:accession ?entityId . \
    ?entity model:thesRef ?disRef . \
    ?disRef skos:broadMatch <{desc}> . \
    ?disRef skos:inScheme <{scheme}> .  \
    ?disRef model:accession ?indicatorId . \
    ?disRef owl:sameAs ?indicator . \
    }} ".format(scheme=MondoScheme,desc=STR_AMTHES+STR_AMNAME+"_Amyloid_Related_Disease",entityType = AMMODEL.Disease)

    description2 = "This disease was listed as a disease involving an amyloid protein in a publication"

    dict2= queryAndCreateAnnot(query2,AMQUERY,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid_Related_Disease"),"ECO_0007631",description2,STR_AMNAME)

    print("now trying to get hereditary type")


    addHereditary(dict2)

    print("adding article ref")

    publiList=[]
    publiList.append("https://www.ncbi.nlm.nih.gov/pubmed/28498720")

    for annot,indic in dict2.items() : 
        annot.addSupportingEvi(STR_AMNAME,"ECO_0000322",publiList)



    print("now trying to get Amyloid related diseases from phenotype")
    query3 = "PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    SELECT ?entity ?entityId \
    WHERE {{ ?entity  rdf:type  model:Disease . \
    ?entity model:accession ?entityId . \
    ?entity model:phenoRef ?phref . ?phref skos:prefLabel 'Amyloidosis' . \
    ?entity model:thesRef ?disRef . \
    FILTER NOT EXISTS {{  ?disRef skos:broadMatch <{desc}> . \
    ?disRef skos:inScheme <{scheme}> .  }} \
    }} ".format(scheme=MondoScheme,desc=STR_AMTHES+STR_AMNAME+"_Amyloidosis", pheno="Amyloidosis")

    description3 = "This disease has Amyloidosis listed in its phenotype in DisGenet"
    evidence3 = "ECO_0007631"
    source3 = Disgenet().name

    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results=sparql.query().convert()
    #dict3={}
    for row in results["results"]["bindings"]:            
        annotId  = row["entityId"]["value"]+"_PhenoAmyloidosis_" +source3+"_"+evidence3           
        annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid_Related_Disease"))
        if not annot.exists() :
            annot.create(evidence3,URIRef(row["entity"]["value"]),source3,description3)
            #dict3[annot] = row["indicator"]["value"]


    

def inferFromDiseaseAssociations():

    #First we annotate genes

    query = "PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    SELECT distinct ?entity ?entityId \
    WHERE {  ?gda rdf:type model:Gda . \
    ?gda model:referringTo ?entity . \
    ?entity rdf:type model:Gene . \
    ?entity model:accession ?entityId . \
    ?gda model:score ?score . FILTER (?score >= 0.3) } "

    description = "This gene is associated to at least one disease with a disgenet score superior or equal to 0.3"
    source = Disgenet().name
    evidence = "ECO_0000363"

    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results=sparql.query().convert()

    for row in results["results"]["bindings"]:            
        annotId  = row["entityId"]["value"]+"_" +source+"_"+evidence 
        annotType = URIRef(STR_AMTHES+STR_AMNAME+"_Pathogenic")       
        annot = Annotation(annotId,annotType)
        if not annot.exists() :
            annot.create(evidence,URIRef(row["entity"]["value"]),source,description)

    #Second we annotate proteins
    query2 = "PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    SELECT distinct ?entity ?entityId (COUNT(?pub) AS ?nb) (GROUP_CONCAT(DISTINCT ?pub; SEPARATOR='|') AS ?publiList) \
    WHERE {  ?proda rdf:type model:Proda . \
    ?proda model:referringTo ?entity . \
    ?entity rdf:type model:Protein . \
    ?entity model:accession ?entityId . \
    ?proda model:supportingEvidence ?evi . \
    ?evi model:publication ?pub . \
    } GROUP BY ?entity ?entityId "

    source2 = Uniprot().name
    evidence2 = "ECO_0000363"

    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(query2)
    sparql.setReturnFormat(JSON)
    results2=sparql.query().convert()

    for row in results2["results"]["bindings"]: 
        if "entityId" in row : 
            annotId  = row["entityId"]["value"]+"_" +source2+"_"+evidence2
            description2 = "This protein is associated to at least one disease in Uniprot and there are " + row["nb"]["value"] + " publications supporting the different disease-gene associations"
            annotType = URIRef(STR_AMTHES+STR_AMNAME+"_Pathogenic")       
            annot = Annotation(annotId,annotType)
            if not annot.exists() :
                annot.create(evidence2,URIRef(row["entity"]["value"]),source2,description2)
                publi = row["publiList"]["value"]
                publiList=publi.split("|")
                annot.addSupportingEvi(Uniprot().name,"ECO_0000323",publiList)




    #Second we annotate proteins from variant-disease annotations
    query3 = "PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    SELECT distinct ?entity ?entityId \
    WHERE {  ?vda rdf:type model:Vda . \
    ?vda model:referringTo ?var . \
    ?var rdf:type model:Variant . \
    ?entity model:isoform ?iso . ?iso model:variant ?var . \
    ?entity model:accession ?entityId . \
    ?vda model:score ?score . FILTER (?score >= 0.3) } "

    source3 = Disgenet().name
    evidence3= "ECO_0000363"
    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(query3)
    sparql.setReturnFormat(JSON)
    results3=sparql.query().convert()

    for row in results3["results"]["bindings"]:            
        annotId  = row["entityId"]["value"]+"_" +source3+"_"+evidence3
        description3 = "This protein has a variant which is associated to at least one disease with a disgenet score superior or equal to 0.3"
        annotType = URIRef(STR_AMTHES+STR_AMNAME+"_Pathogenic")       
        annot = Annotation(annotId,annotType)
        if not annot.exists() :
            annot.create(evidence3,URIRef(row["entity"]["value"]),source3,description3)




def inferFromALBase():


    publiList = []
    publiList.append("https://www.ncbi.nlm.nih.gov/pubmed/19291508")
    complEvi=[ALBase().name,publiList]
    inferFromPDBWithQualifier(STR_AMTHES+STR_AMNAME+"_Amyloid_Light_Chain",URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"),"This isoform is associated with a PDB structure described as a amyloid light chain in ALBase",ALBase().name)

    uniprotList = ["P01834","Q6PJG0","P0DOY3","P01594","P01602","P01599","P01611","P01597","P01614","P01615","P01619","P01624","P06312","P01699","P01700","P01703","P01705","P01709","P01706","P01714","P01715","P01717","P01718","P80748","P01721","P01704","P01701","P01593"]

    evidence = "ECO_0000313"
    description = "This protein (or a protein variant) has an amyloidogenic sequence which is reported to form fibrillar deposits in AL patients"
    for protId in uniprotList :
        print("add ALBASE annot")
        p = Protein(protId)
        if p.exists() : 
            annotId  = protId+"_ALBase_"+evidence
            annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"))
            annot.create(evidence,p.uri,"ALBase",description)
            annot.addQualifier(URIRef(STR_AMTHES+STR_AMNAME+"_Pathogenic"))


def inferFromSignatures():

    #TO ANNOTATE : domains and conserved site => Amyloid
    #TO ANNOTATE : amyloid-like family
    amyloid = STR_AMTHES+STR_AMNAME+"_Amyloid"
    amyloid_like = STR_AMTHES+STR_AMNAME+"_Amyloid_Like"
    Amylome_Interactor =STR_AMTHES+STR_AMNAME+"_Amylome_Interactor"

    descList = [amyloid,amyloid_like,Amylome_Interactor]

    for desc in descList : 

        query = "PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX owl: <http://www.w3.org/2002/07/owl#> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        SELECT ?entity ?entityId ?indicator ?indicatorId \
        WHERE {{ {{ ?entity model:signature ?disRef . \
        ?entity model:accession ?entityId . \
        ?disRef rdf:type skos:Concept . \
        ?disRef skos:notation ?indicatorId .  \
        ?disRef skos:broadMatch <{desc}> . \
        ?disRef model:accession ?id . \
        ?disRef rdfs:seeAlso ?indicator . \
        }} UNION {{ \
        ?entity model:signature ?disRef . \
        ?entity model:accession ?entityId . \
        ?disRef rdf:type skos:Concept . \
        ?disRef skos:notation ?indicatorId .  \
        ?indicator skos:notation ?indicatorId . \
        ?indicator skos:broadMatch <{desc}> . \
        }} \
        }} ".format(desc = desc)

        if "Amyloid_Like" in desc : 
            description = "The sequence of this isoform matches an Interpro signature related to an amyloid-like feature."
            evidence = "ECO_0000259"  #matach to interpro signature member

            dict= queryAndCreateAnnot(query,AMQUERY,URIRef(amyloid_like),evidence,description,STR_AMNAME)
            publiList=[]
            publiList.append("https://www.ncbi.nlm.nih.gov/pubmed/30614283")
            for annot,indic in dict.items() : 
                annot.addSupportingEvi("Interpro","ECO_0000322",publiList)

        elif "Amylome_Interactor" in desc :
            description = "The sequence of this isoform matches an Interpro signature related to an amyloid interactor"
            evidence = "ECO_0000259"  #matach to interpro signature member

            dict= queryAndCreateAnnot(query,AMQUERY,URIRef(Amylome_Interactor),evidence,description,STR_AMNAME)
            publiList=[]
            publiList.append("https://www.ncbi.nlm.nih.gov/pubmed/30614283")
            for annot,indic in dict.items() : 
                annot.addSupportingEvi("Interpro","ECO_0000322",publiList)

        else :
            description = "The sequence of this isoform matches an Interpro signature related to an amyloid entity"
            evidence = "ECO_0000259"  #matach to interpro signature member

            dict= queryAndCreateAnnot(query,AMQUERY,URIRef(amyloid),evidence,description,STR_AMNAME)
            publiList=[]
            publiList.append("https://www.ncbi.nlm.nih.gov/pubmed/30614283")
            for annot,indic in dict.items() : 
                annot.addSupportingEvi("Interpro","ECO_0000322",publiList)







def inferFromISA():
    with open(isa_file_human) as f:
        for line in f:
            info=line.split(";")
            uniId = info[0].strip()
            entityUri = STR_AMKB + uniId
            depositType = info[2]
            transmissionType = info[3]
            localisation = info[4]
            comment = info[5]
            fibril = info[6]
            description = "This protein belongs to a family listed as a protein precursor by the International Society of Amyloidosis (ISA). "
            if "variant" in comment : 
                description += "The amyloidy has only been detected on protein variants. "
            description += "The associated amyloid fibril is designated as "+fibril + ". "
            if "," in depositType : 
                description += "Its deposition type can be both systemic or localized. "
            else : 
                if "S" in depositType : description += "Its deposition type is systemic. "
                elif "L" in depositType : description += "Its deposition type is localized. "
            description += "Amyloid deposition can be found in : " + localisation + ". "
            if "," in transmissionType : 
                description += "Inheritance pattern : both acquired and hereditary transmissions are known ."
            else : 
                if "A" in transmissionType : " Inheritance pattern : the amyloid-related diseases for this protein are not hereditary (acquired form) ."
                elif "H" in transmissionType : " Inheritance pattern : the amyloid-related diseases caused by this protein are hereditary ."

            evidence = "ECO_0000322"
            annotId  = uniId+"_" +STR_AMNAME+"_"+evidence
            annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"))
            annot.create(evidence,URIRef(entityUri),STR_AMNAME,description)
            publiList=[]
            publiList.append("https://www.ncbi.nlm.nih.gov/pubmed/30614283")
            annot.addSupportingEvi("ISA","ECO_0000322",publiList)
            annot.addQualifier(URIRef(STR_AMTHES+STR_AMNAME+"_Pathogenic"))
            if "variant" in comment : 
                annot.addQualifier(URIRef(STR_AMTHES+STR_AMNAME+"_Variant_Only"))

                
    with open(isa_file_animal) as fa :    
            for line in fa :
                uniId = line.strip()
                description = "This protein belongs to a family listed as a protein precursor by the International Society of Amyloidosis (ISA). "
                evidence = "ECO_0000322"
                annotId  = uniId+"_" +STR_AMNAME+"_"+evidence
                annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"))
                annot.create(evidence,URIRef(entityUri),STR_AMNAME,description)
                publiList=[]
                publiList.append("https://www.ncbi.nlm.nih.gov/pubmed/30614283")
                annot.addSupportingEvi("ISA","ECO_0000322",publiList)


def inferFromAmyPro() : 

    with open(amypro_file) as f:
        data = json.load(f)
        for row in data : 
            uniprotId = row["uniprot_id"]
            if "-" in uniprotId : 
                uniprotId=uniprotId.split("-")[0]
            patho_funct = row["class_name"]
            variantOnly = row["wild_type_amyloidogenic"]
            techniquesList = row["experimental_techniques"]

            p=Protein(uniprotId)
            if p.exists() : 
                evidence = "ECO_0000313"
                annotId  = uniprotId+"_AmyPro_"+evidence
                annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"))
                if "patho" in patho_funct : 
                    patho_funct = "pathogenic amyloid"
                    annot.addQualifier(URIRef(STR_AMTHES+STR_AMNAME+"_Pathogenic"))
                if "functional" in patho_funct : 
                    annot.addQualifier(URIRef(STR_AMTHES+STR_AMNAME+"_Functional"))
                description = "This protein is described as a " + patho_funct + " protein in AmyPro. Information about experimental techniques used to support the claim  : "
                for elt in techniquesList : 
                    description += elt + ","
                description = description[:-1]
                annot.create(evidence,p.uri,"AmyPro",description)
                if variantOnly == 0 : 
                    annot.addQualifier(URIRef(STR_AMTHES+STR_AMNAME+"_Variant_Only"))
                    
def inferTest() : 
   with open(amypro_file) as f:
        data = json.load(f)
        for row in data : 
            uniprotId = row["uniprot_id"]
            print(uniprotId)

def inferFromReactome() : 


    query = "PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    PREFIX unicore: <http://purl.uniprot.org/core/> \
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
    SELECT ?entity \
    WHERE { ?entity rdf:type unicore:Protein . \
    ?entity rdfs:seeAlso <http://identifiers.org/reactome/R-HSA-977225> . \
    }"

    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = { 'query': query }
    r=requests.get(UNIQUERY,myparam,headers=headers)    
    data = r.json()

    evidence = "ECO_0000313"
    description = "This protein is annotated with the Reactome descriptor : 'Amyloid fiber formation' in Uniprot"

    for row in data["results"]["bindings"] : 
        uniUri = row["entity"]["value"]
        entityId = uniUri.split("/")[-1]
        p = Protein(entityId)
        annotId  = entityId+"_R-HSA-977225_Uniprot_"+evidence
        annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amylome_Interactor"))
        annot.create(evidence,p.uri,"Uniprot",description,URIRef("http://identifiers.org/reactome/R-HSA-977225"))


def inferFromArticles() : 

    #Functional Amyloid 
    #https://www.ncbi.nlm.nih.gov/pubmed/25131597

    with open(funct_amy_file) as f:

        for line in f :
            uniId = line.strip()
            p=Protein(uniId)
            description = "This protein is listed as a functional amyloid in a publication. "
            evidence = "ECO_0000322"
            annotId  = uniId+"_25131597_" +STR_AMNAME+"_"+evidence
            annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"))
            annot.create(evidence,p.uri,STR_AMNAME,description,URIRef("https://www.ncbi.nlm.nih.gov/pubmed/25131597"))
            annot.addQualifier(URIRef(STR_AMTHES+STR_AMNAME+"_Functional"))

    #RIP1/RIP3
    #https://www.ncbi.nlm.nih.gov/pubmed/22817896
    #idlist = ["P25445","Q13546"]
    #for elt in idlist :

        # uniId = elt
        # description = "This protein is listed as a precursor protein for a functional amyloid complex in a publication. "
        # evidence = "ECO_0000322"
        # annotId  = uniId+"_22817896_" +STR_AMNAME+"_"+evidence
        # annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"))
        # annot.create(evidence,URIRef(STR_AMKB+uniId),STR_AMNAME,description,URIRef("https://www.ncbi.nlm.nih.gov/pubmed/22817896"))
        # annot.addQualifier(URIRef(STR_AMTHES+STR_AMNAME+"_Functional"))


def inferFromPubliInUniprot() : 

    query = """
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#> 
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
    PREFIX unicore: <http://purl.uniprot.org/core/> 
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    SELECT distinct ?entity ?indicator 
    WHERE { ?entity rdf:type unicore:Protein . 
    ?entity unicore:citation ?citation . 
    ?citation skos:exactMatch ?indicator . 
    ?citation unicore:title ?title . FILTER regex(?title,'functional amyloid','i') 
    }"""

    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = { 'query': query }
    r=requests.get(UNIQUERY,myparam,headers=headers)    
    data = r.json()

    evidence = "ECO_0000363"
    description = "This protein is associated with a publication mentioning the terms 'functional amyloid' in its title"

    for row in data["results"]["bindings"] : 
        uniUri = row["entity"]["value"]
        publiUri = row["indicator"]["value"]
        entityId = uniUri.split("/")[-1]
        pubId = publiUri.split("/")[-1]
        p = Protein(entityId)
        annotId  = entityId+"_"+pubId+"_"+STR_AMNAME+"_"+evidence
        annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Functional"))
        annot.create(evidence,p.uri,STR_AMNAME,description,URIRef(publiUri))
        annot.addQualifier(URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid"))


def inferFromUniprotKeywords() : 

    endpoint = "http://localhost:8080/rdf4j-server/repositories/amyp"

    query = """
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#> 
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
    PREFIX unicore: <http://purl.uniprot.org/core/> 
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    SELECT distinct ?entity ?indicator ?indicatorLabel ?tag
    WHERE {{ ?entity rdf:type unicore:Protein . 
    ?entity unicore:classifiedWith ?keyword . 
    SERVICE <{endpoint}> {{
    ?indicator owl:sameAs ?keyword .
    ?indicator skos:inScheme <{scheme}> .
    ?indicator skos:prefLabel ?indicatorLabel .
    ?indicator skos:broadMatch ?tag .
    }}
    }}""".format(endpoint=endpoint, scheme=STR_AMTHES+"Uniprot/scheme")

    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = { 'query': query }
    r=requests.get(UNIQUERY,myparam,headers=headers)    
    data = r.json()
    evidence = "ECO_0000363"
    

    for row in data["results"]["bindings"] : 
        uniUri = row["entity"]["value"]
        keywordUri = row["indicator"]["value"]
        entityId = uniUri.split("/")[-1]
        keywordId = keywordUri.split("/")[-1]
        p = Protein(entityId)
        description = "This protein is tagged with the keyword " + row["indicatorLabel"]["value"] + " in Uniprot"
        annotId  = entityId+"_"+keywordId+"_Uniprot_"+evidence
        if row["tag"]["value"] == STR_AMTHES+STR_AMNAME+"_Amyloid"  : 
            annot = Annotation(annotId,URIRef(row["tag"]["value"]))
        else : 
            annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Involved_In_Amyloidosis"))
        annot.create(evidence,p.uri,"Uniprot",description,Literal("keyword : " + row["indicatorLabel"]["value"]))


def inferFromText() : 

    query="""
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
    PREFIX unicore: <http://purl.uniprot.org/core/>  
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>  
    SELECT distinct ?entity  ?comment 
    WHERE { ?entity rdf:type unicore:Protein .  
    ?entity unicore:annotation ?annot .  
    ?annot rdfs:comment ?comment . 
    FILTER regex(?comment,'(cleav|augment|degrad|induc|regulat|increas|decreas)(e|es|ing|ed)[A-Za-z ,]*(amyloid|prion)','i') } 
    """

    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = { 'query': query }
    r=requests.get(UNIQUERY,myparam,headers=headers)    
    data = r.json()

    evidence = "ECO_0000363"
    description = "This protein is associated with a comment mentioning an interaction with an amyloid entity in Uniprot"

    for row in data["results"]["bindings"] : 
        uniUri = row["entity"]["value"]
        comment = row["comment"]["value"]
        entityId = uniUri.split("/")[-1]
        p = Protein(entityId)
        annotId  = entityId+"_comment_"+Uniprot().name+"_"+evidence
        annot = Annotation(annotId,URIRef(STR_AMTHES+STR_AMNAME+"_Amylome_Interactor"))
        annot.create(evidence,p.uri,Uniprot().name,description,Literal(comment))
  


def addTagToProtein() : 

    
    #remove annotation if protId belongs to black list
    #checkBlackList(prot)
    #checkBlackList(iso)
    #checkBlackList(poly)

    query="""
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
    CONSTRUCT { ?prot model:tag ?qualifier }
        WHERE { ?annot rdf:type model:Annotation .
        VALUES ?qualifier { thes:Amyp_Amyloid thes:Amyp_Amyloid_Like thes:Amyp_Pathogenic thes:Amyp_Functional thes:Amyp_Amylome_Interactor thes:Amyp_Variant_Only }
    {?annot model:referringTo ?prot .  ?annot model:qualifier ?qualifier  . ?prot rdf:type model:Protein .  }
    UNION
    {?annot model:referringTo ?iso .  ?annot model:qualifier ?qualifier  . ?iso rdf:type model:Isoform .  ?prot model:isoform ?iso  . ?prot rdf:type model:Protein .}
    UNION
    {?annot model:referringTo ?poly . ?annot model:qualifier ?qualifier  . ?poly rdf:type model:Polypeptide . ?iso model:cleavedInto ?poly . ?iso rdf:type model:Isoform .  ?prot model:isoform ?iso . ?prot rdf:type model:Protein . }
    } 
    """

    myparam = { 'query': query}
    header = {'Content-Type' : 'application/rdf+n3' }
    results=requests.get(AMQUERY,params=myparam,headers=header)
    graph = Graph()
    graph.parse(data=results.content, format="n3")

    update = " INSERT DATA {{ GRAPH <{g}> {{ {gr} }} }} ".format(gr=graph.serialize(format='nt'),g=STR_AMKB)
    headers = {'content-type' : 'application/sparql-update'}
    try : 
        r=requests.post(AMUPDATE,data=update,headers=headers) 
    except :
        print("There was a problem trying to add amyloid tag info to the triple store")
        
        
def addTagEviToProtein() : 
    #add evidence type experimental when source is AmyPro or  PDB 
    queryEvi="""
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
    CONSTRUCT { ?prot model:amyTagEvidence thes:ECO_0000006 }
        WHERE { ?annot rdf:type model:Annotation .
        VALUES ?tag { thes:Amyp_Amyloid thes:Amyp_Amyloid_Like }
        VALUES ?qualifier { thes:Amyp_Amyloid thes:Amyp_Amyloid_Like thes:Amyp_Variant_Only thes:Amyp_Amyloid_Structure thes:Amyp_Amyloid_Structure thes:Amyloid_Like_Structure thes:Amyp_Amyloid_Fibril thes:Amyp_Amyloid_Complex thes:Amyp_Amyloidogenic}
        VALUES ?source { "PDB" "AmyPro" "AmypDB/PDB" "AmypDB/AmyPro"}
    {?annot model:referringTo ?prot .  ?annot model:qualifier ?qualifier . ?annot model:source ?source . ?prot rdf:type model:Protein .  ?prot model:tag ?tag .   }
    UNION
    {?annot model:referringTo ?iso .  ?annot model:qualifier ?qualifier . ?annot model:source ?source . ?iso rdf:type model:Isoform .  ?prot model:isoform ?iso  . ?prot rdf:type model:Protein . ?prot model:tag ?tag .  }
    UNION
    {?annot model:referringTo ?poly . ?annot model:qualifier ?qualifier .  ?annot model:source ?source . ?poly rdf:type model:Polypeptide . ?iso model:cleavedInto ?poly . ?iso rdf:type model:Isoform .  ?prot model:isoform ?iso . ?prot rdf:type model:Protein .  ?prot model:tag ?tag .  }
    } 
    """

    myparam = { 'query': queryEvi}
    header = {'Content-Type' : 'application/rdf+n3' }
    results=requests.get(AMQUERY,params=myparam,headers=header)
    graph = Graph()
    graph.parse(data=results.content, format="n3")

    update = " INSERT DATA {{ GRAPH <{g}> {{ {gr} }} }} ".format(gr=graph.serialize(format='nt'),g=STR_AMKB)
    headers = {'content-type' : 'application/sparql-update'}
    try : 
        r=requests.post(AMUPDATE,data=update,headers=headers) 
    except :
        print("There was a problem trying to add amyloid tag info to the triple store")
        
        
    queryEvi2="""
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
    CONSTRUCT { ?prot model:amyTagEvidence thes:ECO_0000006 }
        WHERE {  ?prot model:accession ?accession . ?prot a model:Protein . ?prot model:tag ?tag .
    VALUES ?accession {"P01834" "Q6PJG0" "P0DOY3" "P01594" "P01602" "P01599" "P01611" "P01597" "P01614" "P01615" "P01619" "P01624" "P06312" "P01699" "P01700" "P01703" "P01705" "P01709" "P01706" "P01714" "P01715" "P01717" "P01718" "P80748" "P01721" "P01704" "P01701" "P01593"}  
    """

    # myparam = { 'query': queryEvi2}
    # header = {'Content-Type' : 'application/rdf+n3' }
    # results=requests.get(AMQUERY,params=myparam,headers=header)
    # graph = Graph()
    # graph.parse(data=results.content, format="n3")

    # update = " INSERT DATA {{ GRAPH <{g}> {{ {gr} }} }} ".format(gr=graph.serialize(format='nt'),g=STR_AMKB)
    # headers = {'content-type' : 'application/sparql-update'}
    # try : 
        # r=requests.post(AMUPDATE,data=update,headers=headers) 
    # except :
        # print("There was a problem trying to add amyloid tag info to the triple store")    
        
def addTagInteractorToProtein() :    

    query2="""
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    CONSTRUCT {{ ?prot model:tag <{amylome}> }}
    WHERE {{ ?prot rdf:type model:Protein . ?prot model:tag <{amyloid}> . }}
    """.format(amyloid=STR_AMTHES+STR_AMNAME+"_Amyloid", amylome=STR_AMTHES+STR_AMNAME+"_Amylome_Interactor")

    myparam = { 'query': query2}
    header = {'Content-Type' : 'application/rdf+n3' }
    results=requests.get(AMQUERY,params=myparam,headers=header)
    graph = Graph()
    graph.parse(data=results.content, format="n3")

    update = " INSERT DATA {{ GRAPH <{g}> {{ {gr} }} }} ".format(gr=graph.serialize(format='nt'),g=STR_AMKB)
    headers = {'content-type' : 'application/sparql-update'}
    try : 
        r=requests.post(AMUPDATE,data=update,headers=headers) 
    except :
        print("There was a problem trying to add amyloid tag info to the triple store")
    

def addTagToDisease() : 
    query="""
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
    CONSTRUCT { ?disease model:tag ?qualifier }
    WHERE { 
    VALUES ?qualifier {thes:Amyp_Amyloidosis thes:Amyp_Amyloid_Related_Disease}
   { ?annot model:referringTo ?disease . ?annot model:qualifier ?qualifier . ?disease rdf:type model:Disease . } 
   UNION 
   { ?disease rdf:type model:Disease . ?disease model:thesRef ?thes . ?thes skos:broadMatch ?qualifier }
       }
    """

    myparam = { 'query': query}
    header = {'Content-Type' : 'application/rdf+n3' }
    results=requests.get(AMQUERY,params=myparam,headers=header)
    graph= Graph()
    graph.parse(data=results.content, format="n3")
    
    
    update = " INSERT DATA {{ GRAPH <{g}> {{ {gr} }} }} ".format(gr=graph.serialize(format='nt'),g=STR_AMKB)
    headers = {'content-type' : 'application/sparql-update'}
    try : 
        r=requests.post(AMUPDATE,data=update,headers=headers) 
    except :
        print("There was a problem trying to add tag info to disease in the triple store")
    


def addTagToDiseaseFromProt() : 
    
    queryAnnot = """
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    SELECT ?entity ?entityId ?disease ?diseaseId 
    WHERE {{
    ?proda rdf:type model:Proda .
    ?proda model:accession ?acc .
    ?proda model:referringTo ?entity .
    ?entity rdf:type model:Protein .
    ?entity model:accession ?entityId .
    ?entity model:tag <{amyloid}> . 
    ?entity model:amyTagEvidence <{evi}> .
    ?proda model:referringTo ?disease . 
    ?disease rdf:type model:Disease .
    ?disease rdfs:label ?diseaseLabel .
    ?disease model:accession ?diseaseId .
    FILTER NOT EXISTS {{?disease model:tag ?tag}}
    FILTER(regex(?acc,?diseaseId,"i"))
     }}""".format(amyloid=STR_AMTHES+STR_AMNAME+"_Amyloid", evi=STR_AMTHES+"ECO_0000006")

    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(queryAnnot)
    sparql.setReturnFormat(JSON)
    results=sparql.query().convert()

    source = STR_AMSOURCE
    evidence = "ECO_0000363"

    for row in results["results"]["bindings"]:            
        annotId  = row["diseaseId"]["value"]+"_Involves_"+row["entityId"]["value"]+source+"_"+evidence 
        annotType = URIRef(STR_AMTHES+STR_AMNAME+"_Amyloid_Related_Disease")       
        annot = Annotation(annotId,annotType)
        if not annot.exists() :
            description = "This disease is associated to protein " + row["entityId"]["value"] + " which is tagged as Amyloid with experimental evidence"
            annot.create(evidence,URIRef(row["disease"]["value"]),source,description, Literal("Association to " + row["entityId"]["value"]))
            dis = Disease(row["diseaseId"]["value"])
            tempG = Graph()
            tempG.add([dis.uri,AMMODEL.tag,annotType])
            dis.addEntityGraphToStore(tempG)
   
           
def addTagToProtFromDisease() : 
    queryAnnot1 = """
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    SELECT ?entity ?entityId ?diseaseId ?diseaseLabel ?tagLabel
    WHERE {{
    ?proda rdf:type model:Proda .
    ?proda model:referringTo ?disease . 
    ?disease rdf:type model:Disease .
    ?disease model:tag <{amyloidosis}> .
    ?disease rdfs:label ?diseaseLabel .
     <{amyloidosis}> skos:prefLabel ?tagLabel .
    ?disease model:accession ?diseaseId .
    ?proda model:referringTo ?entity .
    ?entity rdf:type model:Protein .
    ?entity model:accession ?entityId . 
     }}""".format(amyloidosis=STR_AMTHES+STR_AMNAME+"_Amyloidosis")

    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(queryAnnot1)
    sparql.setReturnFormat(JSON)
    results=sparql.query().convert()

    source = STR_AMSOURCE
    evidence = "ECO_0000363"

    for row in results["results"]["bindings"]:            
        annotId  = row["entityId"]["value"]+"_Involved_In_"+row["diseaseId"]["value"]+source+"_"+evidence 
        annotType = URIRef(STR_AMTHES+STR_AMNAME+"_Involved_In_Amyloidosis")       
        annot = Annotation(annotId,annotType)
        if not annot.exists() :
            description = "This protein is associated to " + row["diseaseLabel"]["value"] + " which is tagged as a " + row["tagLabel"]["value"]
            annot.create(evidence,URIRef(row["entity"]["value"]),source,description, Literal("Association to " + row["diseaseLabel"]["value"]))
            prot = Protein(row["entityId"]["value"])
            tempG = Graph()
            tempG.add([prot.uri,AMMODEL.tag,annotType])
            prot.addEntityGraphToStore(tempG)



    queryAnnot1b = """
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    SELECT ?entity ?entityId ?diseaseId ?diseaseLabel ?tagLabel
    WHERE {{
    ?proda rdf:type model:Proda .
    ?proda model:accession ?acc .
    ?proda model:referringTo ?disease . 
    ?disease rdf:type model:Disease .
    ?disease model:tag <{amyloidrel}> .
    ?disease rdfs:label ?diseaseLabel .
    <{amyloidrel}> skos:prefLabel ?tagLabel .
    ?disease model:accession ?diseaseId .
    FILTER(regex(?acc,?diseaseId,"i"))
    ?proda model:referringTo ?entity .
    ?entity rdf:type model:Protein .
    ?entity model:accession ?entityId . 
     }}""".format(amyloidrel=STR_AMTHES+STR_AMNAME+"_Amyloid_Related_Disease")

    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(queryAnnot1b)
    sparql.setReturnFormat(JSON)
    results=sparql.query().convert()

    source = STR_AMSOURCE
    evidence = "ECO_0000363"

    for row in results["results"]["bindings"]:            
        annotId  = row["entityId"]["value"]+"_Involved_In_"+row["diseaseId"]["value"]+source+"_"+evidence 
        annotType = URIRef(STR_AMTHES+STR_AMNAME+"_Involved_In_Amyloidosis")       
        annot = Annotation(annotId,annotType)
        if not annot.exists() :
            description = "This protein is associated to " + row["diseaseLabel"]["value"] + " which is tagged as a " + row["tagLabel"]["value"]
            annot.create(evidence,URIRef(row["entity"]["value"]),source,description, Literal("Association to " + row["diseaseLabel"]["value"]))
            prot = Protein(row["entityId"]["value"])
            tempG = Graph()
            tempG.add([prot.uri,AMMODEL.tag,annotType])
            prot.addEntityGraphToStore(tempG)

    print("queryAnnot2")

    queryAnnot2 = """
PREFIX model: <http://purl.amypdb.org/model/> 
PREFIX owl: <http://www.w3.org/2002/07/owl#>  
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
SELECT ?entity ?entityId ?diseaseId ?diseaseLabel ?tagLabel
WHERE {{
?vda rdf:type model:Vda .
?vda model:referringTo ?disease . 
?disease rdf:type model:Disease .
?disease rdfs:label ?diseaseLabel .
?disease model:accession ?diseaseId .
?disease model:tag <{amyloidosis}> .
<{amyloidosis}> skos:prefLabel ?tagLabel .
?vda model:referringTo ?variant .
?variant rdf:type model:VariantAa . 
?iso model:variant ?variant . 
?iso rdf:type model:Isoform .
?entity model:isoform ?iso . 
?entity rdf:type model:Protein .
?entity model:accession ?entityId . 
      }}""".format(amyloidosis=STR_AMTHES+STR_AMNAME+"_Amyloidosis")

    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(queryAnnot2)
    sparql.setReturnFormat(JSON)
    results=sparql.query().convert()

    source = STR_AMSOURCE
    evidence = "ECO_0000363"

    for row in results["results"]["bindings"]:            
        annotId  = row["entityId"]["value"]+"_Involved_In_"+row["diseaseId"]["value"]+source+"_"+evidence 
        annotType = URIRef(STR_AMTHES+STR_AMNAME+"_Involved_In_Amyloidosis")       
        annot = Annotation(annotId,annotType)
        
        description = "This protein has a variant which is associated to " + row["diseaseLabel"]["value"] + " which is tagged as a " + row["tagLabel"]["value"]
        annot.create(evidence,URIRef(row["entity"]["value"]),source,description, Literal("Association to " + row["diseaseLabel"]["value"]))
        prot = Protein(row["entityId"]["value"])
        tempG = Graph()
        tempG.add([prot.uri,AMMODEL.tag,annotType])
        prot.addEntityGraphToStore(tempG)


    print("queryAnnot3")
    queryAnnot3 = """
PREFIX model: <http://purl.amypdb.org/model/> 
PREFIX owl: <http://www.w3.org/2002/07/owl#>  
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
SELECT ?entity ?entityId ?diseaseId ?diseaseLabel ?tagLabel
WHERE {{
?vda rdf:type model:Vda .
?vda model:referringTo ?disease . 
?vda model:accession ?acc .
?disease rdf:type model:Disease .
?disease rdfs:label ?diseaseLabel .
?disease model:accession ?diseaseId .
    FILTER(regex(?acc,?diseaseId,"i"))
?disease model:tag <{amyloidrel}> .
<{amyloidrel}> skos:prefLabel ?tagLabel .
?vda model:referringTo ?variant .
?variant rdf:type model:VariantAa . 
?iso model:variant ?variant . 
?iso rdf:type model:Isoform .
?entity model:isoform ?iso . 
?entity rdf:type model:Protein .
?entity model:accession ?entityId . 
     }}""".format(amyloidrel=STR_AMTHES+STR_AMNAME+"_Amyloid_Related_Disease")

    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(queryAnnot3)
    sparql.setReturnFormat(JSON)
    results=sparql.query().convert()

    source = STR_AMSOURCE
    evidence = "ECO_0000363"

    for row in results["results"]["bindings"]:            
        annotId  = row["entityId"]["value"]+"_Involved_In_"+row["diseaseId"]["value"]+source+"_"+evidence 
        annotType = URIRef(STR_AMTHES+STR_AMNAME+"_Involved_In_Amyloidosis")       
        annot = Annotation(annotId,annotType)
        description = "This protein has a variant which is associated to " + row["diseaseLabel"]["value"] + " which is tagged as a " + row["tagLabel"]["value"]
        annot.create(evidence,URIRef(row["entity"]["value"]),source,description, Literal("Association to " + row["diseaseLabel"]["value"]))
        prot = Protein(row["entityId"]["value"])
        tempG = Graph()
        tempG.add([prot.uri,AMMODEL.tag,annotType])
        prot.addEntityGraphToStore(tempG)
        
    print("queryAnnotFromGeneAssoc")
    queryAnnot4 = """
PREFIX model: <http://purl.amypdb.org/model/> 
PREFIX owl: <http://www.w3.org/2002/07/owl#>  
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
SELECT ?entity ?entityId ?diseaseId ?diseaseLabel ?tagLabel
WHERE {{
?gda rdf:type model:Gda .
?gda model:accession ?acc .
?gda model:referringTo ?disease . 
?disease rdf:type model:Disease .
?disease rdfs:label ?diseaseLabel .
?disease model:accession ?diseaseId .
    FILTER(regex(?acc,?diseaseId,"i"))
?disease model:tag <{amyloidrel}> .
<{amyloidrel}> skos:prefLabel ?tagLabel .
?gda model:referringTo ?gene .
?gene rdf:type model:Gene . 
?gene model:encodes ?entity .
?entity rdf:type model:Protein .
?entity model:accession ?entityId . 
     }}""".format(amyloidrel=STR_AMTHES+STR_AMNAME+"_Amyloid_Related_Disease")

    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(queryAnnot4)
    sparql.setReturnFormat(JSON)
    results=sparql.query().convert()

    source = STR_AMSOURCE
    evidence = "ECO_0000363"

    for row in results["results"]["bindings"]:            
        annotId  = row["entityId"]["value"]+"_Involved_In_"+row["diseaseId"]["value"]+source+"_"+evidence 
        annotType = URIRef(STR_AMTHES+STR_AMNAME+"_Involved_In_Amyloidosis")       
        annot = Annotation(annotId,annotType)
        description = "This protein has a gene which is associated to " + row["diseaseLabel"]["value"] + " which is tagged as a " + row["tagLabel"]["value"]
        annot.create(evidence,URIRef(row["entity"]["value"]),source,description, Literal("Association to " + row["diseaseLabel"]["value"]))
        prot = Protein(row["entityId"]["value"])
        tempG = Graph()
        tempG.add([prot.uri,AMMODEL.tag,annotType])
        prot.addEntityGraphToStore(tempG)        
       
    print("queryAnnotFromGeneAssoc Amyloidosis")
    queryAnnot5 = """
PREFIX model: <http://purl.amypdb.org/model/> 
PREFIX owl: <http://www.w3.org/2002/07/owl#>  
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
SELECT ?entity ?entityId ?diseaseId ?diseaseLabel ?tagLabel
WHERE {{
?gda rdf:type model:Gda .
?gda model:referringTo ?disease . 
?disease rdf:type model:Disease .
?disease rdfs:label ?diseaseLabel .
?disease model:accession ?diseaseId .
?disease model:tag <{amyloidrel}> .
<{amyloidrel}> skos:prefLabel ?tagLabel .
?gda model:referringTo ?gene .
?gene rdf:type model:Gene . 
?gene model:encodes ?entity .
?entity rdf:type model:Protein .
?entity model:accession ?entityId . 
     }}""".format(amyloidrel=STR_AMTHES+STR_AMNAME+"_Amyloidosis")

    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(queryAnnot5)
    sparql.setReturnFormat(JSON)
    results=sparql.query().convert()

    source = STR_AMSOURCE
    evidence = "ECO_0000363"

    for row in results["results"]["bindings"]:            
        annotId  = row["entityId"]["value"]+"_Involved_In_"+row["diseaseId"]["value"]+source+"_"+evidence 
        annotType = URIRef(STR_AMTHES+STR_AMNAME+"_Involved_In_Amyloidosis")       
        annot = Annotation(annotId,annotType)
        description = "This protein has a gene which is associated to " + row["diseaseLabel"]["value"] + " which is tagged as a " + row["tagLabel"]["value"]
        annot.create(evidence,URIRef(row["entity"]["value"]),source,description, Literal("Association to " + row["diseaseLabel"]["value"]))
        prot = Protein(row["entityId"]["value"])
        tempG = Graph()
        tempG.add([prot.uri,AMMODEL.tag,annotType])
        prot.addEntityGraphToStore(tempG)            
        
 

def addBroaderDisease() :
    print("Query all diseases")
    
    queryMondoUriBasis = """
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#> 
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    PREFIX obo: <http://purl.obolibrary.org/obo/>
    SELECT distinct ?disease ?disRef ?mondo WHERE {
    ?disease a model:Disease .
    ?disease model:thesRef ?disRef .
    ?disRef owl:sameAs ?mondo .
    }
       """
    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(queryMondoUriBasis)
    sparql.setReturnFormat(JSON)
    results_1=sparql.query().convert()
    
    tempG = Graph()
    for row in results_1["results"]["bindings"]:            
        mondoUri  = row["mondo"]["value"]
        disRef = row["disRef"]["value"]
        queryBroaderInfo = """
        PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX owl: <http://www.w3.org/2002/07/owl#> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
         PREFIX obo: <http://purl.obolibrary.org/obo/>
        SELECT distinct ?parentUri WHERE {{
        <{mondo}> rdfs:subClassOf ?parentUri .
        ?parentUri rdfs:label ?parentLabel .
        }}
        """.format(mondo=mondoUri)
 
        sparql2 = SPARQLWrapper(MONDOQUERY)
        sparql2.setQuery(queryBroaderInfo)
        sparql2.setReturnFormat(JSON)
        results_1_ok=sparql2.query().convert()
        
       
        exception = ["MONDO_0020573","MONDO_0003900","MONDO_0005583","MONDO_0015879","MONDO_0015880","MONDO_0018652","MONDO_0019183","MONDO_0019275","MONDO_0020012","MONDO_0020683","MONDO_0021147","MONDO_0021166","MONDO_0021178", "MONDO_0021194","MONDO_0021199","MONDO_0021668","MONDO_0021669", "MONDO_0024297", "MONDO_0000001", "MONDO_0003847", "MONDO_0024236", "MONDO_0005559", "MONDO_0021166", "MONDO_0002025", "MONDO_0002254", "MONDO_0000839", "MONDO_0042489", "MONDO_0021125", "MONDO_0045024", "MONDO_0043543", "MONDO_0024505"]

        for row2 in results_1_ok["results"]["bindings"]:            
            parentUri  = row2["parentUri"]["value"]
            disParentId = parentUri.split("/")[-1]  
            if disParentId not in exception :                          
                dis = Disease(disParentId)
                if not dis.exists() :
                    dis.create()
            #print(disRef + " broader " + disParentId) 
                tempG.add([URIRef(disRef),SKOS.broader,URIRef(STR_AMTHES + disParentId)])
    
    upload = " INSERT DATA {{ GRAPH <{g}> {{ {gr} }} }} ".format(gr=tempG.serialize(format='nt'),g=STR_AMKB)
    headers = {'content-type' : 'application/sparql-update'}
    try : 
        r=requests.post(AMUPDATE,data=upload,headers=headers)
    except :
        print("There was a problem trying to add broader relations to the triple store")
 
def addPhenoToAll() :
    print("get diseases")
    query = """
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#> 
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    SELECT distinct ?acc ?mondo WHERE {
    ?disease a model:Disease .
    ?disease model:thesRef ?disRef .
    ?disRef owl:sameAs ?mondo .
    ?disease model:accession ?acc .
    }
    """
    
    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results=sparql.query().convert()
    #dict3={}
    
    print("for each disease, add phenotypes")
    for row in results["results"]["bindings"]:            
        mondoUri  = row["mondo"]["value"]
        disId = row["acc"]["value"]
        disease = Disease(disId)
        disease.addPhenoToDisease(mondoUri)


        
def execBroadDiseasePheno(query) :
    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results=sparql.query().convert()    
    nbline = 25000
    lenresults = len(results["results"]["bindings"])
    #print(lenresults)
    n=int(lenresults/nbline)+1
    for i in range(0,n) :
        tempG = Graph() 
        upto = (i+1)*nbline if (i+1)*nbline < lenresults else lenresults
        #print(str(n) + " " + str(i) + " "+ str(i*nbline))
        for row in results["results"]["bindings"][i*nbline:upto]:  
            diseaseParent  = row["diseaseParent"]["value"]
            phenoRef = row["phenoRef"]["value"]
            tempG.add([URIRef(diseaseParent),AMMODEL.phenoRef,URIRef(phenoRef)])
        
        upload = " INSERT DATA {{ GRAPH <{g}> {{ {gr} }} }} ".format(gr=tempG.serialize(format='nt'),g=STR_AMKB)
        headers = {'content-type' : 'application/sparql-update'}
        try : 
            r=requests.post(AMUPDATE,data=upload,headers=headers) 
        except :
            print("There was a problem trying to add pheno to broader diseases to the triple store")
            
          
    
def addPhenoToBroaderDisease() :    
    print("first add pheno to broader disease from bottom child")
    
    queryPheno = """
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#> 
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
    SELECT distinct ?diseaseParent ?phenoRef WHERE {
    ?disease a model:Disease .
    ?disease model:thesRef ?disRef .
    ?disease model:phenoRef ?phenoRef .
    ?disRef skos:broader ?disParentRef .
    ?diseaseParent model:thesRef ?disParentRef .   
    FILTER NOT EXISTS {?disChildRef skos:broader ?disRef . }
    VALUES ?except {thes:HP_0000006 thes:HP_0000007 thes:HP_0010985 thes:HP_0001417 thes:HP_0001450 thes:HP_0001419 thes:HP_0001423 thes:HP_0001427 thes:HP_0010983 thes:HP_0010984 thes:HP_0001470 }
    FILTER (?phenoRef != ?except )
    }
    """
    
    execBroadDiseasePheno(queryPheno)
            
        
    print("second add pheno to broader disease (2nd rank)")
    queryPheno2 = """
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#> 
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
    SELECT distinct ?diseaseParent ?phenoRef WHERE {
    ?disease a model:Disease .
    ?disease model:thesRef ?disRef .
    ?disease model:phenoRef ?phenoRef .
    ?disRef skos:broader ?disIntRef .
    ?disIntRef skos:broader ?disParentRef .
    ?diseaseParent model:thesRef ?disParentRef .  
    FILTER NOT EXISTS {?disChildRef skos:broader ?disRef . } 
    VALUES ?except {thes:HP_0000006 thes:HP_0000007 thes:HP_0010985 thes:HP_0001417 thes:HP_0001450 thes:HP_0001419 thes:HP_0001423 thes:HP_0001427 thes:HP_0010983 thes:HP_0010984 thes:HP_0001470 }
    FILTER (?phenoRef != ?except )    
    }
    """
    
    execBroadDiseasePheno(queryPheno2)
    
    #print("second add pheno to broader disease (3nd rank)")
    #queryPheno3 = """
    #PREFIX model: <http://purl.amypdb.org/model/> 
    #PREFIX owl: <http://www.w3.org/2002/07/owl#> 
    #PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
    #PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    #SELECT distinct ?diseaseParent ?phenoRef WHERE {
    #?disease a model:Disease .
    #?disease model:thesRef ?disRef .
    #?disease model:phenoRef ?phenoRef .
    #?disRef skos:broader ?disIntRef .
    #?disIntRef skos:broader ?disIntRef2 .
    #?disIntRef2 skos:broader ?disParentRef .
    #?diseaseParent model:thesRef ?disParentRef .  
    #FILTER NOT EXISTS {?disChildRef skos:broader ?disRef . }   
    #VALUES ?except {thes:HP_0000006 thes:HP_HP_0000007 thes:HP_0010985 thes:HP_0001417 thes:HP_0001450 thes:HP_0001419 thes:HP_0001423 thes:HP_0001427 thes:HP_0010983 thes:HP_0010984 thes:HP_0001470 }
    #FILTER (?phenoRef != ?except )    
    #}
    #"""
    
    #execBroadDiseasePheno(queryPheno3)
   


def addBroaderDiseaseToAssoc() :
    print("seach associations with child disease, then add the parents")
    
    
    for atype in ["model:Proda","model:Gda","model:Vda"] :         
        
        queryDisPAssoc = """
        PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX owl: <http://www.w3.org/2002/07/owl#> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        SELECT ?disAssoc ?diseaseParent WHERE {{
        ?disease a model:Disease .
        ?disease model:mentionedIn ?disAssoc .
        ?disAssoc a {assocType} .    
        ?disease model:thesRef ?disRef .
        ?disRef skos:broader+ ?disParentRef .
        ?diseaseParent model:thesRef ?disParentRef .   
        }}
        """.format(assocType = atype)
       
        sparql = SPARQLWrapper(AMQUERY)
        sparql.setQuery(queryDisPAssoc)
        sparql.setReturnFormat(JSON)
        results=sparql.query().convert()
        
        nbline = 20000
        lenresults = len(results["results"]["bindings"])
        n=int(lenresults/nbline)+1
        for i in range(0,n) :
            tempG = Graph() 
            upto = (i+1)*nbline if (i+1)*nbline < lenresults else lenresults
            #print(str(n) + " " + str(upto))
            for row in results["results"]["bindings"][i*nbline:upto] : 
                
                #print(row["diseaseParent"]["value"] + " " + row["disAssoc"]["value"])
                diseaseParent  = row["diseaseParent"]["value"]
                disAssoc = row["disAssoc"]["value"]
                tempG.add([URIRef(diseaseParent),AMMODEL.mentionedIn,URIRef(disAssoc)])
                tempG.add([URIRef(disAssoc),AMMODEL.referringTo,URIRef(diseaseParent)])
                
            upload = " INSERT DATA {{ GRAPH <{g}> {{ {gr} }} }} ".format(gr=tempG.serialize(format='nt'),g=STR_AMKB)
            headers = {'content-type' : 'application/sparql-update'}
            try : 
                r=requests.post(AMUPDATE,data=upload,headers=headers)
                print("Adding " + atype + " relations to the triple store")
            except :
                print("there was a problem trying to add " + atype + " relations to the triple store")


def enrichGeneTranscript() : 
    #gene_trans_file = "data/Gene_transcripts.txt" #Gene stable ID,Transcript stable ID,Protein stable ID,Gene description,Gene name # ensembl biomart
    #gene_tr_prot_file = "data/Gene_Map_From_Uni_Retrieve.csv"  #geneId;protId;geneName;geneSyn;transInfo;taxonId;GeneId

    #INSTEAD OF FILE, THIS COULD BE USED :
    #requestURL = "https://www.uniprot.org/uniprot/"
    #query = "ENSG00000102189 ENSG00000103707"
    #myparam = { 'query': query, 'format':'tab', 'sort':'score', 'columns':'id,genes(PREFERRED),genes(ALTERNATIVE),lineage-id,database(Ensembl),database(GeneID)'} 
    #headers = {'User-Agent' : 'Python laurence.r.noel@gmail.com'}
    #r = requests.get(requestURL,params=myparam, headers=headers)
    #responseBody = r.text
    #print(responseBody)
    #print(r.status_code)
    
    dicoDesc = {}
    dicProt = {}
      
    with open(gene_trans_file) as csv_f :
        csv_r = csv.reader(csv_f, delimiter=',')
        for row in csv_r:
            geneId=row[0]
            transId = row[1]
            protId = row[2]
            geneDesc=row[3]
            
            if geneDesc not in (None, "") :
                dicoDesc[geneId] = geneDesc
                
            if transId not in (None,"") and protId not in (None,"") :
                dicProt[transId] = protId
    
    with open(gene_tr_prot_file) as csv_file:
        csv_reader = list(csv.reader(csv_file, delimiter=';'))
        cp=0
        for row in csv_reader[:9000]:
            cp+=1
            print("row ",str(cp))
            #print(row)
            tempG = Graph()
            
            geneIdL = row[0]
            protId = row[1]
            geneName = row[2]
            if geneName in (None,"") : 
                geneName = "No name for " + geneId
            geneSyn = row[3]
            transInfo = row[4]
            taxonId = row[5]
            geneNCBIdL = row[6]
              
            genes=geneIdL.split(",")
            for geneId in genes : 
                print(geneId)
                gene=Gene(geneId)    
                if not gene.existsAccess() : 
                    tempG.add([gene.uri,RDF.type, gene.type])
                    tempG.add([gene.uri,AMMODEL.accession, Literal(geneId)])        
                    stEnsGuri = STR_ENS+geneId
                    ensGuri=URIRef(STR_ENS+geneId)
                    tempG.add([gene.uri,RDFS.seeAlso,ensGuri])
                    
                if geneNCBIdL not in ("", None) : 
                    genesNCBI = geneNCBIdL.split(";")
                    for ncbiID in genesNCBI : 
                        if ncbiID not in ("", None) : 
                            print(ncbiID)
                            tempG.add([gene.uri,RDFS.seeAlso,URIRef("https://www.ncbi.nlm.nih.gov/gene/"+ncbiID)])
                            disGenUri = URIRef("http://identifiers.org/ncbigene/"+ncbiID)
                            gene.addGda(disGenUri)                
                                    
                tempG.add([gene.uri,RDFS.label,Literal(geneName)])                        
                thes=ThesEntry(STR_ENSNAME+"_"+geneId,STR_ENSNAME)
                if not thes.exists() :
                    thes.createThesGeneEns(geneName,geneId,geneSyn)
                tempG.add([gene.uri,AMMODEL.thesRef,thes.uri])
                tempG.add([gene.uri,RDFS.seeAlso,URIRef(STR_ENS + geneId)]) 
                if geneId in dicoDesc.keys() :
                    tempG.add([gene.uri,AMMODEL.description,Literal(dicoDesc[geneId])])
                    
                taxRef=ThesEntry(Uniprot().name+"_taxonomy_"+taxonId,Uniprot().name+"_taxonomy")
                if not taxRef.exists() :
                    taxRef.createThesOrg()
                tempG.add([gene.uri,AMMODEL.organism,taxRef.uri])    
                   
                if protId not in (None, "") : 
                    prot = Protein(protId)
                    tempG.add([gene.uri,AMMODEL.encodes,prot.uri])
                    tempG.add([prot.uri,AMMODEL.encodedBy,gene.uri])
                    
                if transInfo not in (None, "") : 
                    transList = transInfo.split(";")
                    for trans in transList :
                        trId = trans
                        isoUniId = ""
                        isoEnsId = ""
                        if "[" in trans :
                            trInfo = trans.split(" ")
                            trId = trInfo[0]
                            isowithBracket = trInfo[1]
                            isoUniId =  isowithBracket[1:len(isowithBracket)-1]
                            #print(isoUniId)
                            if trId in dicProt.keys() :
                                isoEnsId = dicProt[trId]    
                                
                        if trId not in (None,"") : 
                            tr = Transcript(trId)
                            if not tr.exists() : 
                                tr.create(isoUniId,isoEnsId)
                            tempG.add([gene.uri,AMMODEL.transcript,tr.uri])
                        
                upload = " INSERT DATA {{ GRAPH <{g}> {{ {gr} }} }} ".format(gr=tempG.serialize(format='nt'),g=STR_AMKB)
                headers = {'content-type' : 'application/sparql-update'}
                try : 
                    r=requests.post(AMUPDATE,data=upload,headers=headers) 
                except :
                    print("There was a problem trying to add gene-trans info to the triple store")
                    

 
def enrichGeneGda() :

    geneList = []
    print("first query")
    query  = """
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#> 
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    SELECT distinct ?dis ?diseaseId ?mondo
    WHERE {
    ?dis a model:Disease .
    ?dis model:accession ?diseaseId .      
    ?dis model:thesRef ?disRef .  
    ?disRef owl:sameAs ?mondo .    
    } 
    """
    
    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results=sparql.query().convert()
    #dict3={}
    
        
    for row in results["results"]["bindings"]:      
    
        disease = row["dis"]["value"]
        disId = row["diseaseId"]["value"]
        mondo = row["mondo"]["value"]
        
        print("query2 " + mondo)
        query2  = """
        PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX owl: <http://www.w3.org/2002/07/owl#> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        SELECT distinct  ?match 
        WHERE {{
        <{mondo}> skos:exactMatch ?match .
        FILTER(regex(str(?match),"umls"))
        }}
        """.format(mondo=mondo)
    
        sparql2 = SPARQLWrapper(MONDOQUERY)
        sparql2.setQuery(query2)
        sparql2.setReturnFormat(JSON)
        results2=sparql2.query().convert()
        #dict3={}
        
        
        for row2 in results2["results"]["bindings"]:            
            umlsUri  = row2["match"]["value"]
            queryGda= """
            PREFIX sio:<http://semanticscience.org/resource/>
            PREFIX ncit:<http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl#>
            SELECT  DISTINCT ?gene ?gdaScore  FROM <{g}> 
            WHERE {{ ?gda sio:SIO_000628 ?gene , <{disgUri}> .    
            ?gda sio:SIO_000216 ?valueR .  
            ?gene rdf:type ncit:C16612 .
            ?valueR sio:SIO_000300 ?gdaScore .  
            FILTER (?gdaScore >= 0.2)  
             }}""".format(g="http://rdf.disgenet.org",disgUri=umlsUri)

            #?gene sio:SIO_010078 ?protein . 
    
            requestURL = DisgenetEndpoint().endpoint
            myparam = { 'query': queryGda }
            try : 
                r = requests.get(requestURL, params=myparam,headers={ "Content-type":"application/x-www-form-urlencoded","Accept" : "application/json"})
            except : 
                print("Unable to retrieve info from Disgenet for GDA")
            else :
                if r.ok : 
                    resultD = r.json()
                    for res in resultD["results"]["bindings"] : 
                        ncbi = res['gene']['value']
                        disGenId = str(ncbi).split("/")[-1]
                        print(disGenId)
                        gdaScore = res['gdaScore']['value']
                        gdaId = disGenId+"_"+disId
  
                            
                        url = 'https://www.uniprot.org/uploadlists/'
                        params = {"from" : "P_ENTREZGENEID","to":"ENSEMBL_ID","format": "list","query": disGenId}
                        try :
                            rmap = requests.get(url,params)
                        except : 
                            print("prb")
                        else : 
                            data = rmap.text
                            print(data)
                            data = data.strip().split()
                            for geneId in data :
                                if geneId not in ("", None) : 
                                    gene=Gene(geneId)   
                                    geneList.append(geneId)
                                    gda = Gda(gdaId,gdaScore,gene.uri,Disease(disId).uri)
                                    if not gda.exists() : 
                                        gda.create(gene.uri,Disease(disId).uri)                      
                                    if not gene.existsAccess() :
                                        tempG = Graph()
                                        tempG.add([gene.uri,RDF.type, gene.type])
                                        tempG.add([gene.uri,AMMODEL.accession, Literal(geneId)])        
                                        stEnsGuri = STR_ENS+geneId
                                        ensGuri=URIRef(STR_ENS+geneId)
                                        tempG.add([gene.uri,RDFS.seeAlso,ensGuri])                              
                                        params2 = {"from" : "ENSEMBL_ID","to":"GENENAME","format": "list","query": geneId}
                                        rmap2 = requests.get(url,params2)
                                        data2 = rmap2.text
                                        geneNames = data2.strip().split()
                                        for geneName in geneNames : 
                                            if geneName not in ("", None) : 
                                                tempG.add([gene.uri,RDFS.label,Literal(geneName)])
                                        gene.addEntityGraphToStore(tempG) 
                                        
    geneId_file = "data/Gene_Id_File.csv"
    with open(geneId_file, "w") as f: 
        for elt in geneList : 
            f.write(elt)
        
     

def addBroaderPheno() :
    print("Query Pheno")
    
    tempG = Graph()
    
    queryPhenoUri = """
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX owl: <http://www.w3.org/2002/07/owl#> 
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    SELECT distinct ?phenoRef ?pheno WHERE {
    ?disease model:phenoRef ?phenoRef .
    ?phenoRef owl:sameAs ?pheno .
    }
    """
    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(queryPhenoUri)
    sparql.setReturnFormat(JSON)
    results=sparql.query().convert()
    #dict3={}
    
    print("Iterate to add broaders")
    for row in results["results"]["bindings"]:            
        phenoUri  = row["pheno"]["value"]
        phenoRef = row["phenoRef"]["value"]
        queryBroaderInfo = """
        PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX owl: <http://www.w3.org/2002/07/owl#> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        SELECT distinct ?parentUri ?parentLabel WHERE {{
        <{pheno}> rdfs:subClassOf ?parentUri .
        ?parentUri rdfs:label ?parentLabel .
        }}
        """.format(pheno=phenoUri)
 
        sparql2 = SPARQLWrapper(HPOQUERY)
        sparql2.setQuery(queryBroaderInfo)
        sparql2.setReturnFormat(JSON)
        results2=sparql2.query().convert()
        
        #print("add broader pheno" + str(len(results2["results"]["bindings"])))

        for row2 in results2["results"]["bindings"]:            
            parentUri  = row2["parentUri"]["value"]
            parentLabel = row2["parentLabel"]["value"]
            phenoParentId = parentUri.split("/")[-1]
   
            pt = ThesEntry(phenoParentId,"HP")
            if not pt.exists() :
                pt.createThesPheno(parentUri,parentLabel)
            tempG.add([URIRef(phenoRef),SKOS.broader,URIRef(STR_AMTHES + phenoParentId)])
    
    upload = " INSERT DATA {{ GRAPH <{g}> {{ {gr} }} }} ".format(gr=tempG.serialize(format='nt'),g=STR_AMTHES)
    headers = {'content-type' : 'application/sparql-update'}
    try : 
        r=requests.post(AMUPDATE,data=upload,headers=headers) 
    except :
        print("There was a problem trying to add broader relations to the triple store")
        
        


def createDict(query,dict) :
    sparql = SPARQLWrapper(AMQUERY)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    data=sparql.query().convert()
    
    for row in data["results"]["bindings"] : 
        cl = row["cl"]["value"]
        clcount = row["nbcl"]["value"]
        dict[cl] = clcount

    return dict


def createSiteInfo():

    print("FOR DISEASES")
    print("Dict pheno 1")


    dictAll = {}
    print("Dict pheno")
    queryPheno2="""
PREFIX model: <http://purl.amypdb.org/model/>
PREFIX owl: <http://www.w3.org/2002/07/owl#> 
PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX thes: <http://purl.amypdb.org/thesaurus/>
SELECT ?cl (count (distinct ?disease) as ?nbcl) 
WHERE {
?disease rdf:type model:Disease .
?disease model:tag ?tag . 
VALUES ?tag { thes:Amyp_Amyloid_Related_Disease thes:Amyp_Amyloidosis } .
?disease model:phenoRef ?phRef . ?phRef skos:prefLabel ?cl . 
} GROUP BY ?cl ORDER BY DESC(?nbcl) """


    dictPhenoAmyloidRelated = {}
    dictPhenoAmyloidRelated = createDict(queryPheno2,dictPhenoAmyloidRelated)
    dictAll["dictDiseasePhenoAmyRelated"]=dictPhenoAmyloidRelated
    
    print("Dict fam 3")

    amypscheme = STR_AMTHES+STR_AMNAME+"/scheme"

    queryDisFam ="PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  \
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>  \
    PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
    SELECT ?cl (count (distinct ?disease) as ?nbcl) \
    WHERE {{  \
    ?disease rdf:type model:Disease . \
    ?disease model:tag ?tag . \
    VALUES  ?tag {{ thes:{am}_Amyloidosis thes:{am}_Amyloid_Related_Disease  }} . \
    {{  ?annot rdf:type model:Proda . ?annot model:referringTo ?disease . \
    ?annot model:referringTo ?prot . }} \
    UNION {{ \
    ?annot rdf:type model:Vda . ?annot model:referringTo ?disease . \
    ?annot model:referringTo ?var . ?var rdf:type model:VariantAa . ?iso model:variant ?var . ?iso rdf:type model:Isoform . ?prot model:isoform ?iso . \
    }} \
    ?prot rdf:type model:Protein . ?prot model:family ?fam . ?fam skos:prefLabel ?cl . ?fam skos:inScheme  <{scheme}> \
       }} GROUP BY ?cl ORDER BY DESC(?nbcl) ".format(am=STR_AMNAME,scheme = amypscheme)
  
    dictDisFam = {}
    dictDisFam = createDict(queryDisFam,dictDisFam)
    dictAll["dictDiseaseFamily"]=dictDisFam

    print("Dict CATH")
    cathScheme = STR_AMTHES+"CATH/scheme"
    queryDisCath ="PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  \
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>  \
    PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
    SELECT ?cl (count (distinct ?disease) as ?nbcl) \
    WHERE {{  \
    ?disease rdf:type model:Disease . \
    ?disease model:tag ?tag . \
    VALUES  ?tag {{ thes:{am}_Amyloidosis thes:{am}_Amyloid_Related_Disease  }} . \
    {{  ?annot rdf:type model:Proda . ?annot model:referringTo ?disease . \
    ?annot model:referringTo ?prot . }} \
    UNION {{ \
    ?annot rdf:type model:Vda . ?annot model:referringTo ?disease . \
    ?annot model:referringTo ?var . ?var rdf:type model:VariantAa . ?iso model:variant ?var . ?iso rdf:type model:Isoform . ?prot model:isoform ?iso . \
    }} \
    ?prot rdf:type model:Protein . ?prot model:family ?fam . ?fam skos:broader ?supf . ?supf skos:prefLabel ?cl . ?fam skos:inScheme  <{scheme}> \
       }} GROUP BY ?cl ORDER BY DESC(?nbcl) ".format(am=STR_AMNAME,scheme = cathScheme)

    dictDisCath = {}
    #dictDisCath = createDict(queryDisCath,dictDisCath)
    #dictAll["dictDiseaseCath"]=dictDisFam

    print("FOR PROTEINS")
    print("Dict ")

    queryProtType="PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  \
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>  \
    PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
    SELECT ?cl (count (distinct ?protein) as ?nbcl) \
    WHERE {{  \
    ?protein rdf:type model:Protein . \
    ?protein model:tag ?tag . \
    VALUES ?tag {{ thes:{am}_Amyloid thes:{am}_Amyloid_Like thes:{am}_Amylome_Interactor }} . \
    ?tag skos:prefLabel ?cl . \
       }} GROUP BY ?cl ORDER BY DESC(?nbcl) ".format(am=STR_AMNAME)

    dictProtType = {}
    dictProtType = createDict(queryProtType,dictProtType)
    dictAll["dictProteinType"]=dictProtType

    queryProtFunct="PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  \
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>  \
    PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
    SELECT ?cl (count (distinct ?protein) as ?nbcl) \
    WHERE {{  \
    ?protein rdf:type model:Protein . \
    ?protein model:tag ?tag . \
    VALUES ?tag {{ thes:{am}_Functional thes:{am}_Pathogenic thes:{am}_Variant_Only }} . \
    ?tag skos:prefLabel ?cl . \
       }} GROUP BY ?cl ORDER BY DESC(?nbcl) ".format(am=STR_AMNAME)

    dictProtFunct = {}
    dictProtFunct = createDict(queryProtFunct,dictProtFunct)
    dictAll["dictProteinFunct"]=dictProtFunct

    queryProtTaxo="PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  \
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>  \
    PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
    SELECT ?cl (count (distinct ?protein) as ?nbcl) \
    WHERE {{  \
    ?protein rdf:type model:Protein . \
    ?protein model:tag ?tag . \
    VALUES ?tag {{ thes:{am}_Amyloid thes:{am}_Amyloid_Like thes:{am}_Amylome_Interactor  }} . \
    ?protein model:organism ?org . \
    ?org skos:prefLabel ?cl . \
       }} GROUP BY ?cl ORDER BY DESC(?nbcl) ".format(am=STR_AMNAME)

    dictProtTaxo = {}
    dictProtTaxo = createDict(queryProtTaxo,dictProtTaxo)
    dictAll["dictProteinTaxo"]=dictProtTaxo



    queryAmylome="PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX owl: <http://www.w3.org/2002/07/owl#>  \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>  \
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>  \
    PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
    SELECT ?cl (count (distinct ?entity) as ?nbcl) \
    WHERE {{  \
    {{ \
    ?entity rdf:type ?type . \
    VALUES ?cl {{  model:Protein }} . \
    ?entity model:tag ?tag . \
    VALUES ?tag {{ thes:{am}_Amyloid thes:{am}_Amyloid_Like thes:{am}_Amylome_Interactor  }} . \
    }} UNION {{ \
    ?entity rdf:type ?type . \
    VALUES ?cl {{  model:Disease }} . \
    ?entity model:tag ?tag . \
    VALUES ?tag {{ thes:{am}_Amyloidosis thes:{am}_Amyloid_Related_Disease  }} . \
    }} UNION {{ \
    ?entity rdf:type ?type . \
    VALUES ?cl {{  model:Gene }} . \
    ?entity model:encodes ?prot . \
    ?prot model:tag ?tag . \
    VALUES ?tag {{ thes:{am}_Amyloid thes:{am}_Amyloid_Like thes:{am}_Amylome_Interactor  }} . \
    }} \
    BIND( \
    IF(regex(str(?type),'Protein'), 'Protein', \
    IF(regex(str(?type),'Disease'), 'Disease', \
    IF(regex(str(?type),'Gene'), 'Gene', \
    'Unknown')))  AS ?cl ) . \
    }} GROUP BY ?cl ".format(am=STR_AMNAME)

    dictAmylome = {}
    dictAmylome = createDict(queryAmylome,dictAmylome)
    dictAll["dictAmylome"]=dictAmylome

    
    with open(site_json_file, 'w') as outfile:  
        json.dump(dictAll, outfile)


def addRaptorxInfo():
    raptorDir = "/mnt/datavol/pred/Predict_Property/results/"
    fastaFiles = os.listdir(raptorDir)
    #fastaFiles = [raptorDir+"P02545-1.fasta"]
    #on va soumettre le graphe tous les 1000 fichiers
    rangeFile = [500,1000,1500,2000,2500,3000,3500,4000,4500,5000,5500,6000,6500,7000,7500,8000,8500,9000,9500,10000,10500,11000,11500,12000,12500,len(fastaFiles)-1]
    #rangeFile = [2000,2500,3000,3500,4000,4500,5000,5500,6000,6500,7000,7500,8000,8500,9000,9500,10000,10500,11000,11500,12000,12500,len(fastaFiles)-1]  
    #cp = 0
    cp = 0
    rang = 0
    tempG = Graph()
    maxG = rangeFile[rang]
    
    for file in fastaFiles : 
        
        filename=file.split("/")[-1]
        isoId = filename.split(".")[0]
        print(isoId)
        isoform = Isoform(isoId)
        
        dictAcc={"E":[],"B":[],"M":[],"\n":[]}
        with open(raptorDir+isoId+"/"+isoId+".acc_simp","r") as accFile : 
            accSeq = accFile.readlines()[2:]
            accSeq = '\n'.join(accSeq)
            accSeq.strip()
            
            letterInit = accSeq[0]
            indexInit = 0
            pos=[indexInit+1,indexInit+1]
            for index,letter in enumerate(accSeq):
                if letter == letterInit and letter != "\n" :                     
                    if  index==len(accSeq)-1 or accSeq[index+1] != letter  : 
                        startpos=pos[0]
                        endpos=index+1
                        pos=[startpos,endpos]
                        listPos = dictAcc[letter].copy()
                        listPos.append(pos)
                        dictAcc[letter] = listPos
                        letterInit = accSeq[index+1]
                        pos=[index+1,index+1]
            #del dictAcc["\n"]
            #del dictAcc["M"]
            #del dictAcc["B"]                    
            #print(dictAcc)  
            
        dictDiso={"*":[],".":[],"\n":[]}
        with open(raptorDir+isoId+"/"+isoId+".diso_simp","r") as disoFile : 
            disoSeq = disoFile.readlines()[2:]
            disoSeq = '\n'.join(disoSeq)
            disoSeq.strip()
            
            letterInit = disoSeq[0]
            indexInit = 0
            pos=[indexInit+1,indexInit+1]
            for index,letter in enumerate(disoSeq):
                if letter == letterInit and letter != "\n" :                     
                    if  index==len(disoSeq)-1 or disoSeq[index+1] != letter  : 
                        startpos=pos[0]
                        endpos=index+1
                        pos=[startpos,endpos]
                        listPos = dictDiso[letter].copy()
                        listPos.append(pos)
                        dictDiso[letter] = listPos
                        letterInit = disoSeq[index+1]
                        pos=[index+1,index+1]
                        
            #print(dictDiso)
            #del dictDiso["\n"]
            #del dictDiso["."]
    
        shortDiso = json.dumps(dictDiso["*"])
        tempG.add([isoform.uri,AMMODEL.disoInfo,Literal(shortDiso)])
        shortAcc = json.dumps(dictAcc["E"])
        tempG.add([isoform.uri,AMMODEL.accInfo,Literal(shortAcc)])
        
        if cp == maxG :
            print("submitting graph with maxindex " , str(maxG))
            upload = " INSERT DATA {{ GRAPH <{g}> {{ {gr} }} }} ".format(gr=tempG.serialize(format='nt'),g=STR_AMKB)
            headers = {'content-type' : 'application/sparql-update'}
            try : 
                requests.post(AMUPDATE,data=upload,headers=headers) 
            except :
                print("There was a problem trying to add broader relations to the triple store")
            finally : 
                rang=rang+1
                maxG = rangeFile[rang]
                tempG = Graph()
        
        cp += 1
        

  
def createBasicThesaurus():
    print("Creating Thesaurus...")
    Thes = Thesaurus()
    Thes.createAmyloidTerms()
    Thes.createCath()
    Thes.createFromExtDescriptors(PDB())
    Thes.createFromExtDescriptors(Go())
    Thes.createFromExtDescriptors(Reactome())
    Thes.createFromExtDescriptors(Mondo())
    Thes.createFromExtDescriptors(Uniprot())
    Thes.createInterpro()


def createEntries():
    print("###################### CREATING ENTRIES #######################")

    print("create Entrie from Amyload")
    createEntriesFromAmyload()
    print("create Entries from ALBase")
    createEntriesFromALBase()
    print("create Entries from Waltz-DB")
    createEntriesFromWaltz()
    print("create Proteins")
    createEntriesFromUniprot()
    
def enrichDiseases():
    print("###################### ENRICH DISEASE #######################")
    print("add broader Diseases and relations")
    addBroaderDisease()
    print("add pheno to disease")
    addPhenoToAll() 
    print("add pheno to broader disease")
    addPhenoToBroaderDisease()    
    print("add broader Disease to the PDA, Vda, GDA associations")
    addBroaderDiseaseToAssoc()     
    
    
def enrichPhenos():
    print("###################### ENRICH PHENO #######################")
    print("add broader parent and broader Relations between Phenotypes")
    addBroaderPheno()


def addAnnotations():
    print("################## ADD ANNOTATIONS ##########################")
    print("add annotation for Amyload")
    #addAnnotForAmyload()
    print("add annotations for ALBAse")
    #addAnnotForALBase()
    print("add annotations for Waltz-DB")
    #addAnnotForWaltz()
    print("add annotations for Amyp")
    addAnnotForMetamyl()
        
    print("infer annotations from ALBase")
    inferFromALBase()
    print("infer annotations from fragments")
    inferFromFragment()

    #TO DO add fragments and hot spots

    print("infer annotations from PDB")
    inferFromPDB() 
    print("infer annotations from Go")
    inferFromGo()  

    print("infer annotations from ISA")
    inferFromISA() 
    print("infer annotation from Signatures")
    inferFromSignatures()  
    print("infer annotations from Amypro")
    inferFromAmyPro()
    print("infer annotations from Articles")
    inferFromArticles() # https://www.ncbi.nlm.nih.gov/pubmed/25131597
    print("infer annotations from Reactome")    
    inferFromReactome()
    print("infer annotations from Uniprot Publications")
    inferFromPubliInUniprot()

    print("infer annotations from Uniprot Keywords")
    inferFromUniprotKeywords()

    print("infer from text")
    inferFromText()


    print("infer annotation from Mondo")
    inferFromMondo()

    print("infer annotation from Disease associations")
    inferFromDiseaseAssociations()
    
    #print("infer from RFAmyloid")
    #inferFromRFAmyloid() #first time : query RFAmyloid with fasta files to get the list of Amyloid / Non Amyloid + score. Use it also to add the ID to blackList when predicted non-Amyloid


def defineStatus():

    #print("add Amyloid / Amyloid-like / Amyloid-interactor to prot")
    #addTagToFragment() NOT USED FOR THE MOMENT, TO CREATE LATER

    print("add Amyloid / Amyloid-like / Amyloid-interactor to prot")
    addTagToProtein()
    print("add Evidence")
    addTagEviToProtein()
    #addTagInteractorToProtein() NOT USED : amylome interactors are just proteins who actually interact with identified prot
    
    print("add tag to diseases")
    addTagToDisease()
    print("add tag to disease From prot")
    addTagToDiseaseFromProt() 
    print("add tag involved in from disease tag")
    addTagToProtFromDisease()
    #print("add tag to other species") addTagToOtherSpecies() TO DO add tag to animal form is human is amyloid . Use : https://www.ebi.ac.uk/proteins/api/doc/#taxonomyApi


if __name__ == '__main__':
    ## When adding new proteins, don't forget to add gene/transcript info
    ## by using http://www.ensembl.org/biomart/martview/10104f87556083c44c41af16a95d3bba 
    ## and retrievingID mappings from uniprot (choosing ensembl in the columns to get the Transcript-Protein relations)
    


    createBasicThesaurus()
    createEntries() #for Genes/transcript : call to EbiEndpoint has been commented (not accessible at the moment)
    #enrichGeneTranscript()   # Pensez à faire en 4 process parallel (0:5000, 5000:1000, 10000:14000, 14000:)
    #enrichDiseases()
    #enrichPhenos()
    #addRaptorxInfo()
    #addAnnotations()
    #defineStatus()
    #enrichGeneGda() # NOT WORKING
    #createSiteInfo()   
