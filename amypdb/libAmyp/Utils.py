from amypdb.libAmyp.Entry import *
from operator import itemgetter
from amypdb.libAmyp.AmyConf import *
from pyramid.response import Response
from pyramid.httpexceptions import HTTPFound,HTTPServiceUnavailable
import requests
import json


def id_from_seq(seq) : 
    idseq = seq
    if len(seq) > 12 :
        mid = int(round(len(seq)/2,0))
        idseq = seq[:6] + "-" + seq[mid-2:mid+2] + "-" + seq[-4:] 
    return idseq


def anti_verbose_json(result_search):
    data=[]
    for row in result_search["results"]["bindings"]:  # permet d'enlever le formatage
        dict = {}
        for elt in result_search["head"]["vars"]:
            if elt in row:
                dict[elt] = row[elt]["value"]
            else:
                dict[elt] = ""
        data.append(dict)
    return data
    
    
def metamyl_with_seq(seqSt) :
    list_hexa = []  # list of each hexapeptide in this sequence
    for aa in range(len(seqSt)):  # fill in list_hexa
        if aa < len(seqSt) - 5:
            list_hexa.append(seqSt[aa:aa + 6])

    to_search = ""
    dico={}
    if len(list_hexa) != 0:
        for i in list_hexa:
            if i not in to_search:
                to_search += "\"" + i + "\" "  # sparql format for search
        querystr1 = """PREFIX ammodel: <http://purl.amypdb.org/model/>
            SELECT DISTINCT ?hexa ?score
            WHERE {{ ?id ammodel:accession ?hexa .
            ?id ammodel:score ?score .}}
            VALUES ?hexa{{ {searched} }}""".format(
            searched=to_search)  # value prend la valeur de la liste d'hexa
            
        #print(querystr1)
        headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
        response = requests.post(METAQUERY, data={"query": querystr1}, headers=headers)
        result_search = response.json()
        print("meta_query done")

        data = anti_verbose_json(result_search)

        results = data
        # creation d'un dictionnaire python  de la forme dict{'AAAAAA'}= 0,5... permet de chercher plus facilement
        # a partir de la liste des hexapeptides
        dict_hexa = {}
        for i in results:
            dict_hexa[i['hexa']] = float(i['score'].strip(";\n, "))

        HST = 0.606  # valeur seuil des hotspot

        HS = [0] * len(seqSt)  # list des hotspot de la seq
        HSA = [0] * len(seqSt)  # avec la moyenne des hostspot
        TA = 0

        list_score = []  # nested list for data display

        for i in range(len(list_hexa)):
            #there can be unknown amino acids (U,Z,X notably) : in this case, there are not in the dictionary. By default, the average value of an hexapeptide is attributed
            if list_hexa[i] not in dict_hexa : 
                dict_hexa[list_hexa[i]] = 0.55225
            list_score.append([list_hexa[i], dict_hexa[list_hexa[i]]])  # liste [AA,score]
            TA += dict_hexa[list_hexa[i]]  # somme des scores de tous les hexa de la seq
            if dict_hexa[list_hexa[i]] > HST:  #pour chaque hexa de la liste si le score de l'hexa est > a la valeur seuil
                j = i
                while j < i + 6:  # si les 6 AA du peptide prennent la valeur 1
                    HS[j] = 1
                    j += 1
            
                

        HS_hex = list()

        for i in range(len(HS) - 5):  # on parcours HS
            j = i
            sumHS = 0
            while j < (i + 6):  # on fais la somme des 6 residue suivant
                sumHS += HS[j]
                j += 1
            if sumHS == 6:  # si ont tous la valeur 1
                HS_hex.append(1)  # alors l'hexapeptide prend la valeur 1
            else:
                HS_hex.append(0)  # sinon 0

        TSA = 0  # total surface amyloide
        start_end = list()  # position de debut et fin +max value de chaque hotspot [[start1,end1,max1],[start1,end2,max2]]
        surface = 0  # somme de l'amylogenicité suppérieure au seuil pour l'ensemble des residue du hotspot
        max = 0  # val la plus elevée d'amylogenicité pour chaque hotspot
        cpt = 0  # longueur du hot spot
        for i in range(len(HS_hex)):
            if HS_hex[i] > 0:
                cpt += 1
                if dict_hexa[list_hexa[i]] > HST:
                    surface += dict_hexa[list_hexa[i]] - HST
                if dict_hexa[list_hexa[i]] > max:
                    max = dict_hexa[list_hexa[i]]
            else:
                if cpt != 0:
                    TSA += surface
                    j = i - cpt
                    while j < i + 5:
                        HSA[j] = surface / cpt
                        j += 1
                    start_end.append([i - cpt, i + 5, round(HSA[i], 5), round(max, 5), seqSt[i - cpt:i + 5]])
                    cpt = 0
                    surface = 0
                    max = 0
        if cpt > 0:
            TSA += surface
            j = len(HS_hex) - cpt
            #print("cpt =", cpt, " j=", j, " stop=", len(HS_hex) + 5)
            while j < len(HS_hex) + 5:
                HSA[j] = surface / cpt
                j += 1
            #for the position, +1 since the index starts at 0 in python but 1 for the sequence itself
            start_end.append([len(HS_hex) - cpt + 1, len(HS_hex) + 5 + 1, round(HSA[len(HS_hex)], 5), round(max, 5),
                              seqSt[len(HS_hex) - cpt:len(HS_hex) + 5]])

        # change format of HSA for a good graph and CSV display (start at 1 : [1 , value HSA])
        shortHSA = {}
        start = 1
        for index in range(len(HSA)):
            val = HSA[index]
            HSA[index] = [index + 1, round(val,5)]
            if val > 0 and index < len(HSA)-1 : 
                if  round(val,5) in shortHSA.keys() :
                    if HSA[index+1] == 0 or HSA[index+1] == len(HSA) :
                        start = shortHSA[round(val,5)][0]
                        shortHSA[round(val,5)]=[start,index+1]
                else : 
                    shortHSA[round(val,5)] = [index+1]
    
                
        # list for the tab containing : protein length, number of hotspot, total area, total amyloid, amyRatio
        general = [len(seqSt), len(start_end), round(TA, 5), round(TSA, 5), round((TSA*100)/TA,1) ]
        dico['HS']=HS
        dico['hexa_scores']=list_score
        dico['hsa']=HSA
        dico['shortHSA']=shortHSA
        dico['hst']=HST
        dico['tab_HS']=start_end
        dico['main_tab']=general
        
    return dico



def metamyl_with_id(id_prot):
    isoDict = Protein(id_prot).getIsoformInfo()
    isoDictSort = sorted(isoDict, key=itemgetter('isoAcc'))
    seqSt = ""
    for elt in isoDictSort:
        if "isoSeq" in elt:
            if "isoCano" in elt:
                if elt["isoCano"] == "True" or elt["isoCano"] == "true":
                    seqSt = elt["isoSeq"]
                 
    dico={}
    
    if seqSt != "":
        dico = metamyl_with_seq(seqSt)
        dico["ItemID"]=id_prot    
    return dico


def getGeneTranscript() : 
    requestURL = "https://www.uniprot.org/uniprot/"
    ENSgenes = ["ENSG00000211949","ENSG00000281962","ENSG00000274576","ENSG00000270550","ENSG00000282777","ENSG00000211962","ENSG00000282131","ENSG00000211938","ENSG00000282211","ENSG00000211973","ENSG00000282350","ENSG00000211934","ENSG00000282550","ENSG00000211937","ENSG00000277318","ENSG00000211955","ENSG00000211959","ENSG00000282579","ENSG00000211956","ENSG00000276491","ENSG00000211967","ENSG00000282593","ENSG00000224373","ENSG00000282691","ENSG00000211941","ENSG00000282322","ENSG00000211890","ENSG00000211891","ENSG00000211892","ENSG00000211893","ENSG00000211895","ENSG00000211896","ENSG00000211898","ENSG00000211899","ENSG00000211905","ENSG00000211933","ENSG00000211935","ENSG00000211942","ENSG00000211943","ENSG00000211944","ENSG00000211945","ENSG00000211946","ENSG00000211947","ENSG00000211950","ENSG00000211951","ENSG00000211952","ENSG00000211957","ENSG00000211958","ENSG00000211961","ENSG00000211964","ENSG00000211965","ENSG00000211966","ENSG00000211968","ENSG00000211970","ENSG00000211972","ENSG00000211974","ENSG00000211976","ENSG00000211979","ENSG00000223648","ENSG00000224650","ENSG00000225698","ENSG00000232216","ENSG00000236170","ENSG00000273539","ENSG00000274236","ENSG00000274497","ENSG00000275148","ENSG00000275316","ENSG00000276192","ENSG00000276775","ENSG00000277016","ENSG00000277574","ENSG00000277633","ENSG00000278042","ENSG00000280411","ENSG00000281990","ENSG00000282045","ENSG00000282104","ENSG00000282122","ENSG00000282151","ENSG00000282213","ENSG00000282223","ENSG00000282281","ENSG00000282284","ENSG00000282286","ENSG00000282290","ENSG00000282305","ENSG00000282337","ENSG00000282344","ENSG00000282399","ENSG00000282409","ENSG00000282425","ENSG00000282451","ENSG00000282476","ENSG00000282482","ENSG00000282627","ENSG00000282633","ENSG00000282639","ENSG00000282651","ENSG00000282653","ENSG00000282657","ENSG00000282714","ENSG00000282716","ENSG00000282726","ENSG00000282745","ENSG00000282776","ENSG00000211597","ENSG00000211598","ENSG00000211599","ENSG00000211611","ENSG00000211623","ENSG00000211625","ENSG00000211626","ENSG00000211632","ENSG00000211633","ENSG00000211637","ENSG00000211638","ENSG00000211639","ENSG00000211641","ENSG00000211642","ENSG00000211643","ENSG00000211645","ENSG00000211647","ENSG00000211649","ENSG00000211650","ENSG00000211654","ENSG00000211656","ENSG00000211672","ENSG00000211674","ENSG00000224041","ENSG00000225523","ENSG00000228325","ENSG00000239571","ENSG00000239819","ENSG00000239855","ENSG00000239862","ENSG00000239951","ENSG00000239975","ENSG00000240382","ENSG00000240671","ENSG00000240864","ENSG00000241244","ENSG00000241294","ENSG00000241351","ENSG00000241566","ENSG00000241755","ENSG00000242076","ENSG00000242371","ENSG00000242534","ENSG00000242580","ENSG00000242766","ENSG00000243063","ENSG00000243238","ENSG00000243264","ENSG00000243290","ENSG00000243466","ENSG00000244116","ENSG00000244437","ENSG00000244575","ENSG00000250036","ENSG00000251039","ENSG00000251546","ENSG00000273962","ENSG00000275777","ENSG00000276566","ENSG00000278857","ENSG00000281345","ENSG00000281471","ENSG00000281933","ENSG00000281978","ENSG00000282025","ENSG00000282120","ENSG00000282163","ENSG00000282172","ENSG00000282210","ENSG00000282282","ENSG00000282310","ENSG00000282394","ENSG00000282402","ENSG00000282447","ENSG00000282666","ENSG00000282671","ENSG00000282694","ENSG00000282758","ENSG00000282801","ENSG00000282811","ENSG00000282823","ENSG00000283113"]
    for elt in  ENSgenes :
        query = elt
        myparam = {'query': query, 'format':'tab', 'sort':'score', 'columns':'id,genes(PREFERRED),genes(ALTERNATIVE),lineage-id,database(Ensembl),database(GeneID)'} 
        headers = {'User-Agent' : 'Python laurence.r.noel@gmail.com'}
        r = requests.get(requestURL,params=myparam, headers=headers)
        responseBody = r.text
        #print(responseBody)
        with open("data/ISA/uniprot_immuno.csv","a") as f :
            resp = responseBody.splitlines()
            for line in resp[1:] :                 
                sent = elt+"\t"+line+"\n"
                f.write(sent)
