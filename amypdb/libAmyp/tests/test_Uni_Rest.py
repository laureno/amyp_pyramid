import requests, sys


requestURL = "https://www.uniprot.org/uniprot/"
query = "ENSG00000102189"
myparam = { 'query': query, 'format':'tab', 'sort':'score', 'columns':'id,genes(PREFERRED),genes(ALTERNATIVE),lineage-id,database(Ensembl),database(GeneID)'} 
headers = {'User-Agent' : 'Python laurence.r.noel@gmail.com'}
r = requests.get(requestURL,params=myparam, headers=headers)
responseBody = r.text
print(responseBody)
print(r.status_code)
with open("toto.txt", "w") as f: 
    f.write(responseBody)

