from BioManager import *
from rdflib import *
from rdflib.namespace import SKOS,RDF,RDFS,OWL
from SPARQLWrapper import JSON,N3,XML,TURTLE
import requests
from amypdb.libAmyp.AmyConf import *

class DescriptorManager(object):
    """
        Manage descriptors specific to Amypdb from external resources
    """

    def __init__(self):
        print("descriptor Manager creation")

    def createFromExtDescriptors(self,bioEntity) :
        print("Descriptor extraction from ", bioEntity.name)
        resultGraph=self._extractDescriptors(bioEntity) 
        print("Creation of our own descriptors from ", bioEntity.name)
        self._transformToAmyDesc(resultGraph,bioEntity)

    def _createId(self,uriSource,bioEntity) :
        uriId = uriSource.split(bioEntity.idSeparator)[-1]
        return uriId


    def _createDescUri(self,uriSource,bioEntity) :
        uriId = uriSource.split(bioEntity.idSeparator)[-1]
        link = STR_AMTHES + bioEntity.name + "_"+uriId
        if bioEntity.baseUri == Orphanet().baseUri or bioEntity.baseUri == Go().baseUri :
            link = STR_AMTHES + uriId
        elif bioEntity.baseUri == Uniprot().baseUri :
            link = STR_AMTHES + bioEntity.name + "_" + uriSource.split(bioEntity.idSeparator)[-2] + "_"+ uriSource.split(bioEntity.idSeparator)[-1]
        return URIRef(link)

    def _transformToAmyDesc(self,resultGraph,bioEntity) : 
        
        g=Graph()
        g.bind('',URIRef(STR_AMTHES))
        g.bind('skos',URIRef(u'http://www.w3.org/2004/02/skos/core#'))
        g.bind('rdf',URIRef(u'http://www.w3.org/1999/02/22-rdf-syntax-ns#'))
        g.bind('rdfs',URIRef(u'http://www.w3.org/2000/01/rdf-schema#'))
        g.bind('owl',URIRef(u'http://www.w3.org/2002/07/owl#'))
        #create conceptScheme
        conceptScheme = URIRef(STR_AMTHES+bioEntity.name+"/scheme")
        g.add([conceptScheme, RDF.type, SKOS.ConceptScheme])
        g.add([conceptScheme, RDFS.label, Literal("Descripteurs issus de "+ bioEntity.name)])
        g.add([conceptScheme, DCTERMS.title, Literal("Descripteurs issu de "+bioEntity.name)])

        for sub in resultGraph.subjects(None,None) :
            if not isinstance(sub,BNode) : 
            
                #Subject creation + in scheme
                subA = self._createDescUri(sub,bioEntity)
                #print("descSubj :", subA)
                
                g.add([subA, RDF.type, SKOS.Concept])
                g.add([subA, SKOS.inScheme, conceptScheme])      
                g.add([subA, OWL.sameAs,sub])
                #Incoherence de la pdb : l'api renvoie des uri avec http et l'endpoint sparql avec https. Dans uniprot, ce sont les uri http qui sont utilisées.
                if "rdf.wwpdb.org/pdb" in str(sub) :
                  httpSub = str(sub).replace("https","http")
                  g.add([subA, OWL.sameAs,URIRef(httpSub)])
                g.add([subA, AMMODEL.accession, Literal(self._createId(sub,bioEntity))])
                #ON AJOUTE LA PROPRIETE ISAMYLOID
                g.add([subA,URIRef(STR_AMMODEL+"amyloidMention"), Literal(True, datatype=XSD.boolean)])   

                for pred,obj in resultGraph.predicate_objects(sub) :
                    #print("predicate :", pred)
                    
            
                    if pred == SKOS.prefLabel or pred == URIRef(u'http://www.biopax.org/release/biopax-level3.owl#displayName'):
                        g.add([subA, SKOS.prefLabel, obj])
                    elif pred == RDFS.label : 
                        if len(g.preferredLabel(subA)) < 1 :
                            g.add([subA, SKOS.prefLabel, obj])
                    elif pred == URIRef(u'http://www.ebi.ac.uk/efo/alternative_term') or pred == SKOS.altLabel or pred == URIRef(u'http://www.geneontology.org/formats/oboInOwl#hasNarrowSynonym') or pred == URIRef(u'http://www.geneontology.org/formats/oboInOwl#hasExactSynonym') or pred == URIRef(u'http://www.geneontology.org/formats/oboInOwl#hasRelatedSynonym') :
                        g.add([subA, SKOS.altLabel, obj])
                
                #Description generation
                    elif pred == URIRef(u'http://www.ebi.ac.uk/efo/definition') or pred == URIRef(u'http://purl.obolibrary.org/obo/IAO_0000115') :
                        g.add([subA, SKOS.definition, obj])           

                    elif pred == SKOS.scopeNote :
                        if isinstance (obj,Literal) : 
                            g.add([subA,SKOS.scopeNote, obj])

                    elif pred == SKOS.note :
                        if isinstance (obj,Literal) :
                            g.add([subA,SKOS.note, obj])

                    elif pred == RDFS.comment : 
                        g.add([subA,SKOS.note, obj])

                #Intra-source relationships
                    elif pred == RDFS.subClassOf or pred == URIRef(u'http://id.nlm.nih.gov/mesh/vocab#broaderDescriptor') or pred == SKOS.broader :
                        if ( not isinstance(obj, BNode) ) and ( obj != URIRef(u'http://www.orpha.net/ORDO/ObsoleteClass') ) and  ( obj != URIRef(u'http://www.semanticweb.org/ontology/HOOM#Orpha_Num')) :
                            g.add([subA, SKOS.broader,self._createDescUri(obj,bioEntity)]) 

                    elif pred == URIRef(u'http://data.bioontology.org/metadata/obo/part_of') :
                        if not isinstance(obj, BNode):
                            g.add([subA, URIRef(u'http://www.w3.org/2004/02/skos/extensions#broaderPartitive'), self._createDescUri(obj,bioEntity)])
                    elif pred == SKOS.narrower :
                        g.add([subA,SKOS.narrower,self._createDescUri(obj,bioEntity)])

                
                #Inter-sources relationships
                    elif pred == URIRef(u'http://www.geneontology.org/formats/oboInOwl#hasDbXref'): 
                        strval=obj.value
                        if (strval.startswith("ICD-10")) or (strval.startswith("MeSH")) or (strval.startswith("OMIM")) or (strval.startswith("UMLS")) :
                            link=STR_AMTHES+strval.split(":")[0]+"_"+strval.split(":")[-1]
                            g.add([subA,SKOS.relatedMatch,URIRef(link)])

                    elif pred == SKOS.exactMatch:
                        strval=str(obj)
                        if "umls" in strval :
                            link=STR_AMTHES+UMLS().name+"_"+strval.split("/")[-1]
                            g.add([subA,SKOS.exactMatch,URIRef(link)])
                        elif "mesh" in strval : 
                            link=STR_AMTHES+Mesh().name+"_"+str(obj).split("/")[-1]
                            g.add([subA,SKOS.exactMatch,URIRef(link)])
                        elif "ORDO" in strval : 
                            link=STR_AMTHES+str(obj).split("/")[-1]
                            g.add([subA,SKOS.exactMatch,URIRef(link)])



                    elif pred == RDFS.seeAlso :
                        if "mesh" in str(obj) or "/mim/" in str(obj) : 
                            link=STR_AMTHES+Mesh().name+"_"+str(obj).split("/")[-1]
                            if "/mim/" in str(obj) :
                                link=STR_AMTHES+Omim().name+"_"+str(obj).split("/")[-1]
                            g.add([subA,SKOS.relatedMatch,URIRef(link)])


                    elif pred == SKOS.changeNote or pred == URIRef(u'http://www.geneontology.org/formats/oboInOwl#hasOBONamespace') :
                        g.add([subA,AMMODEL.refType,URIRef(STR_AMMODEL+obj)])
                        

                    elif pred == RDF.type and "http://www.biopax.org/" in str(obj) :
                        #print(str(obj))
                        g.add([subA,RDF.type,URIRef(STR_AMMODEL+str(obj).split("#")[-1])])

                    elif pred == RDF.type and Uniprot().baseUri in str(obj) : 
                        g.add([subA,RDF.type,URIRef(STR_AMMODEL+str(obj).split("/")[-1])])

        


        
        for s,o in g.subject_objects(SKOS.broader) :
            #print("removing triples")
            if (o, SKOS.prefLabel, None) not in g : 
                g.remove([s,SKOS.broader,o])
        

        for s in g.subjects(None,None) :           
            if (s, SKOS.broader, None) not in g and not isinstance(s,BNode) and (s,RDF.type,SKOS.conceptScheme) not in g :
                g.add([s, SKOS.topConceptOf,conceptScheme])
        
       
        
        #with open("results.xml","w") as file :
        g.serialize(destination="data/results"+bioEntity.name+".ttl", format="nt")
            #file.write(g.serialize(format="pretty-xml"))


    def _extractDescriptors(self, bioEntity):

        amylo = "amylo(id|gen|do|se)|prion"
        prot = "Transthyretin|Beta(-| )*2(-| )*microglobulin|Apolipoprotein A(-| )*I|Apolipoprotein A(-| )*II|Apolipoprotein A(-| )*(IV|4)|Apolipoprotein C(-| )*(II|2)|Gelsolin|Lysozyme C|Leukocyte cell-derived chemotaxin-2|Fibrinogen alpha chain|Cystatin(-| )*C|Integral membrane protein 2B|Amyloid-beta precursor protein|Alpha-synuclein|Major prion protein|^Islet amyloid polypeptide|Natriuretic peptides A|^Prolactin$|^Insulin$|Lactadherin|Transforming growth factor-beta-induced protein ig-h3|Lactotransferrin$"



        if bioEntity.baseUri == Mondo().baseUri : 


          queryNerv = """          
          prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
          prefix owl: <http://www.w3.org/2002/07/owl#> 
          prefix oboInOwl: <http://www.geneontology.org/formats/oboInOwl#>
          prefix rdfs:<http://www.w3.org/2000/01/rdf-schema#>
          CONSTRUCT { ?s skos:changeNote "Disease"@en . 
          ?s ?p ?o . 
          ?s1 ?subClassOf ?s .
          ?s1 ?p1 ?o1 .
          ?s1 skos:changeNote "Disease"@en . 
          }
          WHERE { {
          VALUES ?s {<http://purl.obolibrary.org/obo/MONDO_0005071>}
          VALUES ?p {rdfs:subClassOf rdfs:label oboInOwl:hasExactSynonym  oboInOwl:hasNarrowSynonym skos:exactMatch skos:closeMatch}
          VALUES ?p1 {rdfs:label oboInOwl:hasExactSynonym  oboInOwl:hasNarrowSynonym skos:exactMatch skos:closeMatch}
          ?s ?p ?o . 
          ?s1 ?subClassOf ?s .
          ?s1 ?p1 ?o1 .
          } UNION {
          VALUES ?sinit {<http://purl.obolibrary.org/obo/MONDO_0005071>}
          ?s1 ?subClassOf ?sinit .
          ?s2 ?subClassOf* ?s1 .
          ?s2 ?p2 ?o2 .
          } }
          """


          queryInfo1 = """
          prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
          prefix owl: <http://www.w3.org/2002/07/owl#> 
          prefix oboInOwl: <http://www.geneontology.org/formats/oboInOwl#>
          prefix rdfs:<http://www.w3.org/2000/01/rdf-schema#>
          CONSTRUCT { ?s skos:changeNote "Disease"@en . 
          ?s ?p ?o . 
          ?s1 ?subClassOf ?s .
          ?s1 ?p1 ?o1 .
          ?s1 skos:changeNote "Disease"@en . 
          }
          WHERE { 
          VALUES ?s {<http://purl.obolibrary.org/obo/MONDO_0021179>}
          VALUES ?p {rdfs:subClassOf rdfs:label oboInOwl:hasExactSynonym  oboInOwl:hasNarrowSynonym skos:exactMatch }
          VALUES ?p1 {rdfs:label oboInOwl:hasExactSynonym  oboInOwl:hasNarrowSynonym skos:exactMatch }
          ?s ?p ?o . 
          ?s1 ?subClassOf ?s .
          ?s1 ?p1 ?o1 .
          }
          """

          queryInfo2 = """
          prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
          prefix owl: <http://www.w3.org/2002/07/owl#> 
          prefix oboInOwl: <http://www.geneontology.org/formats/oboInOwl#>
          prefix rdfs:<http://www.w3.org/2000/01/rdf-schema#>
          CONSTRUCT { 
          ?s2 ?subClassOf ?s1 .
          ?s2 ?p2 ?o2 . ?s2 skos:changeNote "Disease"@en . 
          }
          WHERE { 
          VALUES ?s {<http://purl.obolibrary.org/obo/MONDO_0021179>}
          VALUES ?p2 {rdfs:label oboInOwl:hasExactSynonym  oboInOwl:hasNarrowSynonym skos:exactMatch }
          ?s ?p ?o . 
          ?s1 ?subClassOf ?s .
          ?s2 ?subClassOf ?s1 .
          ?s2 ?p2 ?o2 .
          }
          """

          queryInfo3 = """
          prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
          prefix owl: <http://www.w3.org/2002/07/owl#> 
          prefix oboInOwl: <http://www.geneontology.org/formats/oboInOwl#>
          prefix rdfs:<http://www.w3.org/2000/01/rdf-schema#>
          CONSTRUCT { 
          ?s3 ?subClassOf ?s2 .
          ?s3 ?p3 ?o3 . ?s3 skos:changeNote "Disease"@en . 
          }
          WHERE { 
          VALUES ?s {<http://purl.obolibrary.org/obo/MONDO_0021179>}
          VALUES ?p3 {rdfs:label oboInOwl:hasExactSynonym  oboInOwl:hasNarrowSynonym skos:exactMatch }
          ?s ?p ?o . 
          ?s1 ?subClassOf ?s .
          ?s2 ?subClassOf ?s1 .
          ?s3 ?subClassOf ?s2 .
          ?s3 ?p3 ?o3 .
          }
          """

          queryInfo4 = """
          prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
          prefix owl: <http://www.w3.org/2002/07/owl#> 
          prefix oboInOwl: <http://www.geneontology.org/formats/oboInOwl#>
          prefix rdfs:<http://www.w3.org/2000/01/rdf-schema#>
          CONSTRUCT { 
          ?s4 ?subClassOf ?s3 .
          ?s4 ?p4 ?o4 . ?s4 skos:changeNote "Disease"@en . 
          }
          WHERE { 
          VALUES ?s {<http://purl.obolibrary.org/obo/MONDO_0021179>}
          VALUES ?p4 {rdfs:label oboInOwl:hasExactSynonym  oboInOwl:hasNarrowSynonym skos:exactMatch }
          ?s ?p ?o . 
          ?s1 ?subClassOf ?s .
          ?s2 ?subClassOf ?s1 .
          ?s3 ?subClassOf ?s2 .
          ?s4 ?subClassOf ?s3 .
          ?s4 ?p4 ?o4 .
          }
          """

          queryInfo5 = """
          prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
          prefix owl: <http://www.w3.org/2002/07/owl#> 
          prefix oboInOwl: <http://www.geneontology.org/formats/oboInOwl#>
          prefix rdfs:<http://www.w3.org/2000/01/rdf-schema#>
          CONSTRUCT { 
          ?s5 ?subClassOf ?s4 .
          ?s5 ?p5 ?o5 . ?s5 skos:changeNote "Disease"@en . 
          }
          WHERE { 
          VALUES ?s {<http://purl.obolibrary.org/obo/MONDO_0021179>}
          VALUES ?p5 {rdfs:label oboInOwl:hasExactSynonym  oboInOwl:hasNarrowSynonym skos:exactMatch }
          ?s ?p ?o . 
          ?s1 ?subClassOf ?s .
          ?s2 ?subClassOf ?s1 .
          ?s3 ?subClassOf ?s2 .
          ?s4 ?subClassOf ?s3 .
           ?s5 ?subClassOf ?s4 .
          ?s5 ?p5 ?o5 .
          }
          """

          g = Graph()
          results1=bioEntity.getBioSparqlService().execQuery(queryInfo1,N3)
          g.parse(data=results1, format="n3")
          results2=bioEntity.getBioSparqlService().execQuery(queryInfo2,N3)
          g.parse(data=results2, format="n3")
          results3=bioEntity.getBioSparqlService().execQuery(queryInfo3,N3)
          g.parse(data=results3, format="n3")
          results4=bioEntity.getBioSparqlService().execQuery(queryInfo4,N3)
          g.parse(data=results4, format="n3")
          results5=bioEntity.getBioSparqlService().execQuery(queryInfo5,N3)
          g.parse(data=results5, format="n3")

          return g


    
        if bioEntity.baseUri == Uniprot().baseUri :
            #deux query : bcp plus rapide qu'avec une seule query
            queryInfo1 = """
            PREFIX up:<http://purl.uniprot.org/core/> 
            PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
            PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> 
            PREFIX skos:<http://www.w3.org/2004/02/skos/core#> 
            CONSTRUCT { ?s rdf:type ?type . ?s ?p1 ?label . ?s ?p ?o  . ?s skos:changeNote "Keyword"@en} 
            FROM <http://sparql.uniprot.org/keywords>
            WHERE {                 
                   VALUES ?p1 {skos:prefLabel skos:altLabel rdfs:comment}
                   VALUES ?type {up:Concept} 
                  ?s rdf:type ?type . 
                  ?s ?p1 ?label .
                  ?s ?p ?o .            
                   FILTER (regex(?label,"amylo(id|ge|si)|prion","i"))
              }             
"""


            queryInfo2 = """
           PREFIX up:<http://purl.uniprot.org/core/> 
            PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
            PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> 
            PREFIX skos:<http://www.w3.org/2004/02/skos/core#> 
            CONSTRUCT { ?s rdf:type ?type . ?s ?p1 ?label . ?s ?p ?o . ?s skos:changeNote "Disease"@en }
			FROM <http://sparql.uniprot.org/diseases>
            WHERE {                 
                   VALUES ?p1 {skos:prefLabel skos:altLabel rdfs:comment}
                   VALUES ?type {up:Disease} 
                  ?s rdf:type ?type . 
                  ?s ?p1 ?label .
                  ?s ?p ?o .            
                   FILTER (regex(?label,"amylo(id|ge|si)|prion","i"))
                   FILTER NOT EXISTS {?s ?p1 ?label . FILTER (regex(?label,"non-amyloid","i")) }
              }
              """
            #prbl avec l'interrogation du endpoint par SPARQLWrapper, format non reconnu
            #results1=bioEntity.getBioSparqlService().execQuery(queryInfo1,N3)
            myparam = { 'query': queryInfo1, 'format':'rdf'}
            results1=requests.get(UniprotEndpoint().endpoint,params=myparam)
            myparam2 = { 'query':queryInfo2, 'format':'rdf'}
            results2 = requests.get(UniprotEndpoint().endpoint,params=myparam2)       

            print("executed")
            g = Graph()
            g.parse(data=results1.content, format="xml")
            g.parse(data=results2.content, format="xml")
            return g

        
        if bioEntity.baseUri == Go().baseUri :
            queryInfo1 = """
	    prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
	    prefix owl: <http://www.w3.org/2002/07/owl#> 
	    CONSTRUCT {?s ?p ?o . ?s rdfs:label ?label. }
            FROM <http://purl.obolibrary.org/obo/merged/GO> 
	    WHERE { 
	    ?s ?p ?o .
	    ?s rdfs:label ?label .
	    FILTER (regex(?label, "amylo(i|g|si)|prion","i"))
      FILTER NOT EXISTS { ?s owl:deprecated "true"^^xsd:boolean}
	    }
            """
            results1=bioEntity.getBioSparqlService().execQuery(queryInfo1,N3)
            g = Graph()
            g.parse(data=results1, format="n3")
            return g


        if bioEntity.baseUri == Reactome().baseUri :
            queryInfo1 = """
            prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
            prefix owl: <http://www.w3.org/2002/07/owl#>
            prefix biopax3: <http://www.biopax.org/release/biopax-level3.owl#>
            CONSTRUCT {?s ?p ?o . 
                       ?s biopax3:displayName ?label .
                       ?s rdf:type ?type . } 
           
            WHERE { 
            ?s ?p ?o .
            ?s biopax3:displayName ?label .
            ?s rdf:type ?type .
              VALUES ?type { biopax3:Pathway biopax3:PhysicalEntity biopax3:BiochemicalReaction biopax3:Complex }
            FILTER (regex(?label, "amylo(i|g|si)|prion","i"))
            }
            """
           
            results1=bioEntity.getBioSparqlService().execQuery(queryInfo1,N3)
            g = Graph()
            g.parse(data=results1, format="n3")
            return g


        if bioEntity.baseUri == UMLS().baseUri :
            queryInfo1 = """
        CONSTRUCT { ?concept skos:prefLabel ?label . ?concept skos:note ?note . ?concept skos:changeNote "Disease"@en }
	WHERE { 
       ?concept a umls-semnetwork:T047 .
       ?concept skos-xl:prefLabel / skos-xl:literalForm ?label 
       FILTER (regex(?label, "amylo(id|ge)|prion", "i")) 	 
       ?concept skos-xl:prefLabel /skos:note  ?note . 
	   #FILTER (regex(?note, "Mesh|MetaThesaurus|OMIM|NCI|ICD","i"))	
           FILTER (regex(?note, "MetaThesaurus","i"))
        } """

            queryInfo2 = """
CONSTRUCT { ?concept skos:prefLabel ?pref . ?concept skos:note ?note . ?concept skos:altLabel ?alt .  ?concept skos:changeNote "Disease"@en }
WHERE { 
       ?concept a umls-semnetwork:T047 .
        ?concept skos-xl:prefLabel / skos-xl:literalForm ?pref .
        ?concept skos-xl:prefLabel / skos:note  ?note . 
   FILTER (regex(?note, "Metathesaurus","i"))
       ?concept skos-xl:altLabel / skos-xl:literalForm ?alt .
    ?concept skos-xl:altLabel / skos:note  ?note2 . 
       FILTER (regex(?alt, "amylo(id|ge)|prion", "i")) 
        FILTER (regex(?note2, "Mesh|Metathesaurus|OMIM|NCI|ICD","i"))
        }
            """

            queryInfo3 = """
CONSTRUCT { ?concept skos:definition ?definition }
WHERE { 
         ?concept a umls-semnetwork:T047 .
       ?concept skos-xl:prefLabel / skos-xl:literalForm ?label 
       FILTER (regex(?label, "amylo(id|ge)|prion", "i")) 	 
       ?concept skos-xl:prefLabel /skos:note  ?note . 
	    FILTER (regex(?note, "MetaThesaurus","i"))
		?concept skos:definition ?definition .   
       } 
            """

            queryInfo4 = """
CONSTRUCT { ?concept skos:narrower ?narrower . ?narrower skos:prefLabel ?pref . ?narrower skos:changeNote "Disease"@en }
WHERE { 
       ?concept a umls-semnetwork:T047 .
       ?concept skos-xl:prefLabel / skos-xl:literalForm ?label .
       FILTER (regex(?label, "amylo(id|ge)|prion", "i"))      
       ?concept skos-xl:prefLabel / skos:note  ?note . 
        FILTER (regex(?note, "MetaThesaurus","i"))
       {?concept umls:relation ?rel .
       ?rel umls:relationType "Narrower relationship" .
       ?rel umls:relatedConcept ?narrower . } UNION {?concept skos:narrower ?narrower .} 
       ?narrower skos-xl:prefLabel / skos-xl:literalForm ?pref .
        ?narrower skos-xl:prefLabel / skos:note  ?noteN . 
        FILTER (regex(?noteN, "MetaThesaurus","i"))       
       }
            """

            queryInfo5 = """

CONSTRUCT { ?concept skos:broader ?broader }
WHERE { 
       ?concept a umls-semnetwork:T047 .
       ?concept skos-xl:prefLabel / skos-xl:literalForm ?label .
       FILTER (regex(?label, "amylo(id|ge)|prion", "i")) 	 
       ?concept skos-xl:prefLabel / skos:note  ?note . 
	    FILTER (regex(?note, "MetaThesaurus","i"))
       {?concept umls:relation ?rel .
       ?rel umls:relationType "Broader relationship" .
       ?rel umls:relatedConcept ?broader .  } UNION {?concept skos:broader ?broader .}  
        ?broader skos-xl:prefLabel / skos:note  ?noteN . 
	    FILTER (regex(?noteN, "MetaThesaurus","i"))	   	   
       } 
            """



            queryInfo6 = """
        CONSTRUCT { ?concept skos:prefLabel ?label . ?concept skos:note ?note . ?concept skos:changeNote "PhysicalEntity"@en }
	WHERE { 
       ?concept a umls-semnetwork:T116 .
       ?concept skos-xl:prefLabel / skos-xl:literalForm ?label 
       FILTER (regex(?label, "amylo(id|ge)|prion", "i")) 	 
       ?concept skos-xl:prefLabel /skos:note  ?note .
           FILTER (regex(?note, "MetaThesaurus","i"))
        } 
            """


            queryInfo7 = """
CONSTRUCT { ?concept skos:prefLabel ?pref . ?concept skos:note ?note . ?concept skos:altLabel ?alt .  ?concept skos:changeNote "PhysicalEntity"@en }
WHERE { 
       ?concept a umls-semnetwork:T116 .
        ?concept skos-xl:prefLabel / skos-xl:literalForm ?pref .
        ?concept skos-xl:prefLabel / skos:note  ?note . 
   FILTER (regex(?note, "Metathesaurus","i"))
       ?concept skos-xl:altLabel / skos-xl:literalForm ?alt .
    ?concept skos-xl:altLabel / skos:note  ?note2 . 
       FILTER (regex(?alt, "amylo(id|ge)|prion", "i")) 
        FILTER (regex(?note2, "Mesh|Metathesaurus|OMIM|NCI|ICD","i"))
        }
            """


            queryInfo8 = """
CONSTRUCT { ?concept skos:narrower ?narrower . ?narrower skos:prefLabel ?pref . ?narrower skos:changeNote "PhysicalEntity"@en }
WHERE { 
       ?concept a umls-semnetwork:T116 .
       ?concept skos-xl:prefLabel / skos-xl:literalForm ?label .
       FILTER (regex(?label, "amylo(id|ge)|prion", "i"))      
       ?concept skos-xl:prefLabel / skos:note  ?note . 
        FILTER (regex(?note, "MetaThesaurus","i"))
       {?concept umls:relation ?rel .
       ?rel umls:relationType "Narrower relationship" .
       ?rel umls:relatedConcept ?narrower . } UNION {?concept skos:narrower ?narrower .} 
       ?narrower skos-xl:prefLabel / skos-xl:literalForm ?pref .
        ?narrower skos-xl:prefLabel / skos:note  ?noteN . 
        FILTER (regex(?noteN, "MetaThesaurus","i"))       
       }
            """

            queryInfo9 = """

CONSTRUCT { ?concept skos:broader ?broader }
WHERE { 
       ?concept a umls-semnetwork:T116 .
       ?concept skos-xl:prefLabel / skos-xl:literalForm ?label .
       FILTER (regex(?label, "amylo(id|ge)|prion", "i")) 	 
       ?concept skos-xl:prefLabel / skos:note  ?note . 
	    FILTER (regex(?note, "MetaThesaurus","i"))
       {?concept umls:relation ?rel .
       ?rel umls:relationType "Broader relationship" .
       ?rel umls:relatedConcept ?broader .  } UNION {?concept skos:broader ?broader .}  
        ?broader skos-xl:prefLabel / skos:note  ?noteN . 
	    FILTER (regex(?noteN, "MetaThesaurus","i"))	   	   
       } 
            """


            myparam = { 'query': queryInfo1, 'format':'rdf'}
            header = {'Content-Type' : 'application/rdf+xml' }
            results1=requests.get(LinkedLifeDataEndpoint().endpoint,params=myparam, headers=header)
            myparam2 = { 'query':queryInfo2, 'format':'rdf'}
            results2 = requests.get(LinkedLifeDataEndpoint().endpoint,params=myparam2, headers=header)
            myparam3 = { 'query': queryInfo3, 'format':'rdf'}
            results3=requests.get(LinkedLifeDataEndpoint().endpoint,params=myparam3, headers=header)
            myparam4 = { 'query':queryInfo4, 'format':'rdf'}
            results4 = requests.get(LinkedLifeDataEndpoint().endpoint,params=myparam4, headers=header)
            myparam5 = { 'query': queryInfo5, 'format':'rdf'}
            results5 = requests.get(LinkedLifeDataEndpoint().endpoint,params=myparam5, headers=header)

            myparam6 = { 'query': queryInfo6, 'format':'rdf'}
            myparam7 = { 'query': queryInfo7, 'format':'rdf'}
            myparam8 = { 'query': queryInfo8, 'format':'rdf'}
            myparam9 = { 'query': queryInfo9, 'format':'rdf'}
            results6 = requests.get(LinkedLifeDataEndpoint().endpoint,params=myparam6, headers=header)
            results7 = requests.get(LinkedLifeDataEndpoint().endpoint,params=myparam7, headers=header)
            results8 = requests.get(LinkedLifeDataEndpoint().endpoint,params=myparam8, headers=header)
            results9 = requests.get(LinkedLifeDataEndpoint().endpoint,params=myparam9, headers=header)


            #results1=bioEntity.getBioSparqlService().execQuery(queryInfo1,N3)
            #results2=bioEntity.getBioSparqlService().execQuery(queryInfo2,N3)
            #results3=bioEntity.getBioSparqlService().execQuery(queryInfo3,N3)
            #results4=bioEntity.getBioSparqlService().execQuery(queryInfo4,N3)
            #results5=bioEntity.getBioSparqlService().execQuery(queryInfo5,N3)
            #print(results1.content.decode())

            g = Graph()
            g.parse(data=results1.content.decode(), format="xml")
            g.parse(data=results2.content.decode(), format="xml")
            g.parse(data=results3.content.decode(), format="xml")
            g.parse(data=results4.content.decode(), format="xml")
            g.parse(data=results5.content.decode(), format="xml")
            g.parse(data=results6.content.decode(), format="xml")
            g.parse(data=results7.content.decode(), format="xml")
            g.parse(data=results8.content.decode(), format="xml")
            g.parse(data=results9.content.decode(), format="xml")
            return g

            

        if bioEntity.baseUri == Orphanet().baseUri :
	    #First query based on label "amyloid|prion"
            
            queryInfo1 = """
PREFIX skos:<http://www.w3.org/2004/02/skos/core#>
            PREFIX efo:<http://www.ebi.ac.uk/efo/>
            CONSTRUCT { ?orpha ?p ?o .
                        ?orpha ?p1 ?label .
                        ?orpha skos:changeNote "Disease"@en .
                        }
            WHERE {
            ?orpha ?p ?o .
            VALUES ?p1 { rdfs:label skos:prefLabel efo:definition efo:alternative_term rdfs:comment }
            ?orpha ?p1 ?label . 
            FILTER (regex(?label, "amylo(id|ge|si)|prion","i"))
            FILTER NOT EXISTS {?orpha rdfs:label | skos:prefLabel | efo:definition | efo:alternative_term  ?label . FILTER (regex(?label,"non-amyloid","i")) } 
            FILTER NOT EXISTS {?orpha rdfs:subClassOf <http://www.orpha.net/ORDO/Orphanet_410298> }   
             }
            """
            queryInfo2 = """
            PREFIX skos:<http://www.w3.org/2004/02/skos/core#>
            PREFIX efo:<http://www.ebi.ac.uk/efo/>
            PREFIX obo:<http://data.bioontology.org/metadata/obo/>
            CONSTRUCT { ?s ?p ?o .  ?s ?p1 ?label . ?s rdfs:subClassOf ?interm . ?s skos:changeNote "PhysicalEntity"@en . }
            WHERE { ?s ?p ?o .
                 ?s rdfs:subClassOf+ ?interm .
                 ?interm rdfs:subClassOf* ?top .
                 VALUES ?top { <http://www.orpha.net/ORDO/Orphanet_410298>  }
                 ?s ?p1 ?label . VALUES ?p1 { rdfs:label skos:prefLabel efo:definition efo:alternative_term } 
            FILTER (regex(?label, "Transthyretin|Beta(-| )*2(-| )*microglobulin|Apolipoprotein A(-| )*I|Apolipoprotein A(-| )*II|Apolipoprotein A(-| )*(IV|4)|Apolipoprotein C(-| )*(II|2)|Gelsolin|Lysozyme C|Leukocyte cell-derived chemotaxin-2|Fibrinogen alpha chain|Cystatin(-| )*C|Integral membrane protein 2B|Amyloid-beta precursor protein|Alpha-synuclein|Major prion protein|Islet amyloid polypeptide|Natriuretic peptides A|Prolactin|Insulin$|Lactadherin|Transforming growth factor-beta-induced protein ig-h3|Lactotransferrin","i"))
            FILTER NOT EXISTS {?s rdfs:label | skos:prefLabel | efo:definition | efo:alternative_term  ?label . FILTER (regex(?label,"non-amyloid","i")) }
            }  
		"""

            myparam = { 'query': queryInfo1}
            header = {'Content-Type' : 'application/x-turtle' }
            endpoint = "http://localhost:80/rdf4j-server/repositories/external"
            results1=requests.get(endpoint,params=myparam,headers=header)

            #results1=bioEntity.getBioSparqlService().execQuery(queryInfo1,N3)
            myparam2 = { 'query' : queryInfo2}
            results2=requests.get(endpoint,params=myparam2, headers=header)
            #results2=bioEntity.getBioSparqlService().execQuery(queryInfo2,N3)
            print("Merging results in a single graph")

            
            g = Graph()
            g.parse(data=results1.content, format="n3")
            g.parse(data=results2.content, format="n3")

            return g
        
        if bioEntity.baseUri == Mesh().baseUri :
            queryInfo1 ="""
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX meshv: <http://id.nlm.nih.gov/mesh/vocab#>
PREFIX mesh: <http://id.nlm.nih.gov/mesh/>
PREFIX skos:<http://www.w3.org/2004/02/skos/core#>
CONSTRUCT {?term rdfs:label ?label ; 
                 meshv:identifier ?id ; 
                 meshv:broaderDescriptor ?broader .          
          ?term skos:changeNote "Disease"@en .
           ?term <http://www.w3.org/2004/02/skos/core#altLabel> ?label2 .
          }
FROM <http://id.nlm.nih.gov/mesh>
WHERE { ?term rdfs:label ?label ; 
              meshv:identifier ?id ; 
              meshv:broaderDescriptor ?broader .
      ?term a meshv:TopicalDescriptor .
      ?term meshv:active "true"^^xsd:boolean .
      ?term meshv:broaderDescriptor* <http://id.nlm.nih.gov/mesh/D009750> .
       OPTIONAL { ?term meshv:preferredTerm ?concept .
             ?concept meshv:altLabel ?label2 . }
       FILTER (regex(?label,"amyloid|prion","i"))
      }"""
            queryInfo2 = """
	    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX meshv: <http://id.nlm.nih.gov/mesh/vocab#>
PREFIX mesh: <http://id.nlm.nih.gov/mesh/>
PREFIX skos:<http://www.w3.org/2004/02/skos/core#>
CONSTRUCT {?term rdfs:label ?label ; 
                 meshv:identifier ?id ; 
                 meshv:broaderDescriptor ?broader . 
          ?term skos:changeNote "PhysicalEntity"@en . 
           ?term <http://www.w3.org/2004/02/skos/core#altLabel> ?label2 .
          }
FROM <http://id.nlm.nih.gov/mesh>
WHERE { ?term rdfs:label ?label ; 
              meshv:identifier ?id ; 
              meshv:broaderDescriptor ?broader .
      ?term a meshv:TopicalDescriptor .
      ?term meshv:active "true"^^xsd:boolean .
       OPTIONAL { ?term meshv:preferredTerm ?concept .
                    ?concept meshv:altLabel ?label2 . }
      ?term meshv:broaderDescriptor* ?top .
      VALUES ?top {<http://id.nlm.nih.gov/mesh/D011506> <http://id.nlm.nih.gov/mesh/D010455> <http://id.nlm.nih.gov/mesh/D000596> }
         FILTER (regex(?label,"amyloid|prion|Transthyretin|Beta(-| )*2(-| )*microglobulin|Apolipoprotein A(-| )*I|Apolipoprotein A(-| )*II|Apolipoprotein A(-| )*(IV|4)|Apolipoprotein C(-| )*(II|2)|Gelsolin|Lysozyme C|Leukocyte cell-derived chemotaxin-2|Fibrinogen alpha chain|Cystatin(-| )*C|Integral membrane protein 2B|Amyloid-beta precursor protein|Alpha-synuclein|Major prion protein|^Islet amyloid polypeptide|Natriuretic peptides A|^Prolactin$|^Insulin$|Lactadherin|Transforming growth factor-beta-induced protein ig-h3|Lactotransferrin$","i"))
      }
	    """

            results1 = bioEntity.getBioSparqlService().execQuery(queryInfo1,N3)
            results2 = bioEntity.getBioSparqlService().execQuery(queryInfo2,N3)
            g = Graph()
            g.parse(data=results1, format="n3")
            g.parse(data=results2, format="n3")

            return g



        if bioEntity.baseUri == PDB().baseUri :
            queryInfo1 ="""
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX PDBo: <https://rdf.wwpdb.org/schema/pdbx-v50.owl#>
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
CONSTRUCT {?pdb rdfs:label ?text . ?pdb dcterms:identifier ?id . ?pdb skos:altLabel ?alt . }
FROM <http://rdf.integbio.jp/dataset/pdbj>
WHERE { {
?pdb dc:title ?text .
?pdb skos:altLabel ?label .
} UNION {  
?struct_keywords_ref PDBo:struct_keywords.text ?text .
?struct_keywords_ref PDBo:of_datablock ?pdb . 
}
FILTER regex(?text, "amylo(id|gen|do|se)|fibril", "i")
}
 """

            results1 = bioEntity.getBioSparqlService().execQuery(queryInfo1,N3)
            g = Graph()
            g.parse(data=results1, format="n3")

            return g


if __name__ == '__main__':
    descM = DescriptorManager()
    #descM.createFromExtDescriptors(UMLS())
    #descM.createFromExtDescriptors(Orphanet()) 
    #descM.createFromExtDescriptors(Mesh())
    #descM.createFromExtDescriptors(Go())
    #descM.createFromExtDescriptors(Reactome())
    #descM.createFromExtDescriptors(Uniprot())
    #descM.createFromExtDescriptors(Mondo())
    descM.createFromExtDescriptors(PDB())
