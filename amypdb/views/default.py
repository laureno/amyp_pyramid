from pyramid.view import view_config,view_defaults
from pyramid.response import Response
from pyramid.httpexceptions import HTTPFound,HTTPServiceUnavailable
from pyramid_mailer.message import Message
import requests
import json
import re
import transaction
from amypdb.libAmyp.Entry import *
from amypdb.libAmyp.Utils import *
from rdflib import URIRef, Literal,Graph
from rdflib.namespace import SKOS,RDF,RDFS,OWL,DC,XSD
from operator import itemgetter
from pyramid.path import AssetResolver
from collections import Counter
from SPARQLWrapper import SPARQLWrapper, JSON


@view_config(route_name='home', renderer='../templates/home.jinja2')
def home(request):
    settings = request.registry.settings
    if settings['maintenance'] == True :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)

    endpoint = settings['repo_amyp_query']
    # resolver = AssetResolver()
    # static_path = resolver.resolve('amypdb:static').abspath()
    # json_url = static_path+"/browse_count.json"
    # with open(json_url,'r') as json_file:  
    #     data = json.load(json_file)
    #print(dictCathJson)

    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    resultCount={}

    queryCountGenes = """
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX model: <http://purl.amypdb.org/model/> 
PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
SELECT (count(distinct ?gene) AS ?nbGene)
WHERE { 
?gene rdf:type model:Gene .
?gene model:encodes ?protein .
?protein model:organism <http://purl.amypdb.org/thesaurus/Uniprot_taxonomy_9606> .
{ ?protein model:tag <http://purl.amypdb.org/thesaurus/Amyp_Amyloid> . } 
UNION {?protein model:tag <http://purl.amypdb.org/thesaurus/Amyp_Amyloid_Like> .}
UNION {?protein model:tag <http://purl.amypdb.org/thesaurus/Amyp_Involved_In_Amyloidosis> .}
UNION {?protein model:tag  <http://purl.amypdb.org/thesaurus/Amyp_Amylome_Interactor>  .}
}"""

    #myparam = { 'query': queryCountGenes}
    #r=requests.get(endpoint,myparam,headers=headers)    
    #results = r.json()
    #for row in results["results"]["bindings"] :
        #resultCount["nbGene"] = row["nbGene"]["value"]
    resultCount["nbGene"] = 894

    queryCountAmylomeProteins = """
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX model: <http://purl.amypdb.org/model/> 
PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
SELECT (count(distinct ?protein) AS ?nbProtein) 
WHERE {
?protein rdf:type model:Protein .
?protein model:tag ?tag .
?protein model:organism <http://purl.amypdb.org/thesaurus/Uniprot_taxonomy_9606> .
VALUES ?tag { <http://purl.amypdb.org/thesaurus/Amyp_Amyloid> <http://purl.amypdb.org/thesaurus/Amyp_Amyloid_Like> 
<http://purl.amypdb.org/thesaurus/Amyp_Involved_In_Amyloidosis> <http://purl.amypdb.org/thesaurus/Amyp_Amylome_Interactor>  }
} """

    myparam = { 'query': queryCountAmylomeProteins}
    # #r=requests.get(endpoint,myparam,headers=headers)    
    # results = r.json()
    # for row in results["results"]["bindings"] :
    #     resultCount["nbAmylomeProtein"] = row["nbProtein"]["value"]
    resultCount["nbAmylomeProtein"] = 598

    queryCountAmyloidProteins = """
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX model: <http://purl.amypdb.org/model/> 
PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
SELECT (count(distinct ?protein) AS ?nbProtein) 
WHERE { 
?protein rdf:type model:Protein .
?protein model:tag <http://purl.amypdb.org/thesaurus/Amyp_Amyloid> .
?protein model:organism <http://purl.amypdb.org/thesaurus/Uniprot_taxonomy_9606> . } """

    myparam = { 'query': queryCountAmyloidProteins}
    # r=requests.get(endpoint,myparam,headers=headers)    
    # results = r.json()
    # for row in results["results"]["bindings"] :
    #     resultCount["nbAmyloidProtein"] = row["nbProtein"]["value"]
    resultCount["nbAmyloidProtein"] = 265

    queryCountDiseases = """
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX model: <http://purl.amypdb.org/model/> 
PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
SELECT (count(distinct ?disease) AS ?nbDisease) 
WHERE {?disease rdf:type model:Disease . ?disease model:tag ?tag .
VALUES ?tag {<http://purl.amypdb.org/thesaurus/Amyp_Amyloidosis> <http://purl.amypdb.org/thesaurus/Amyp_Amyloid_Related_Disease>}
}"""

    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = { 'query': queryCountDiseases}
    r=requests.get(endpoint,myparam,headers=headers)  
    print(endpoint)  
    print(myparam)
    print(r.status_code)
    results = r.json()
    for row in results["results"]["bindings"] :
        resultCount["nbDisease"] = row["nbDisease"]["value"]
        
        
    data = []
    resolver = AssetResolver()
    static_path = resolver.resolve('amypdb:static').abspath()
    json_url = static_path+"/table_nomenclature.json"
    with open(json_url,'r') as json_file:  
        data = json.load(json_file)
    return {'project': 'amyp' , 'humanCount' : resultCount, 'resultISA': data}
    
    
@view_config(route_name='contact', renderer='../templates/contact.jinja2')
def contact(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    return {'contact': 'Page contact'}

@view_config(route_name='contact_response', renderer="../templates/contact_response.jinja2")
def contact_response(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavaiblable')
        return HTTPFound(location=url)
    mailer = request.mailer
    message = Message(subject="hello", sender="ln@toto.com", recipients=["laurence.noel@inserm.fr"],body="hello")
    mailer.send(message)
    transaction.commit()
    return {'message': 'message sent'}

@view_config(route_name='about_data', renderer='../templates/about_data.jinja2')
def about_data(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    return {'data': 'Page data'}


@view_config(route_name='stats', renderer='../templates/stats.jinja2')
def stats(request):
    settings = request.registry.settings
    if settings['maintenance'] :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)

    endpoint = settings['repo_amyp_query']
    queryAvg = """
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
        SELECT (AVG(?aromaticity) AS ?avgAro) 
        (AVG(?isoelectricPoint) AS ?avgIso) 
        (AVG(?molWeight) AS ?avgMol) 
        (AVG(?instability) AS ?avgInst) 
        WHERE {
        ?prot rdf:type model:Protein .
        ?prot model:tag <http://purl.amypdb.org/thesaurus/Amyp_Amyloid> .
        ?prot model:isoform ?iso .
        ?prot model:family ?family .
        FILTER regex(str(?family),"Amyp_")
        ?iso model:isCanonical ?boolean .
        ?iso model:aromaticity ?aromaticity .
        ?iso model:isoelectricPoint ?isoelectricPoint .
        OPTIONAL { ?iso model:molWeight ?molWeight . }
        OPTIONAL { ?iso model:instabilityIndex ?instability . }
              } GROUP BY ?family """
    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = { 'query': queryAvg }
    r=requests.get(endpoint,myparam,headers=headers)    
    results = r.json()
      
    dictAvg =  []
    for row in results["results"]["bindings"] :
        dict =  {}
        for elt in results["head"]["vars"] : 
            if elt in row :
                dict[elt] = row[elt]["value"] 
            else : 
                dict[elt] = ""
        dictAvg.append(dict)

    dictAvgFinal= []
    counter = 0
    avgAro = 0.0
    avgIso = 0.0
    avgInst = 0.0
    avgMol = 0.0
    for item in dictAvg : 
        counter = counter + 1
        avgAro = avgAro + float(item["avgAro"])
        avgIso = avgIso + float(item["avgIso"])
        avgInst = avgInst + float(item["avgInst"])
        avgMol = avgMol + float(item["avgMol"])
    dictAvgFinal = {"avgAro":avgAro/counter,"avgIso":avgIso/counter,"avgInst":avgInst/counter,"avgMol":avgMol/counter}


    queryAmino = """
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        SELECT ?amino ?family
        WHERE {
        ?prot rdf:type model:Protein .
        ?prot model:tag <http://purl.amypdb.org/thesaurus/Amyp_Amyloid> .
        ?prot model:isoform ?iso .
        ?prot model:family ?family .
        FILTER regex(str(?family),"Amyp_")
        ?iso model:isCanonical ?boolean .
        ?iso model:aminoPercent ?amino .
              }  """
    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = { 'query': queryAmino }
    r=requests.get(endpoint,myparam,headers=headers)    
    results = r.json()
    aminoData = {}

    for item in results["results"]["bindings"] : 
        if item["family"]["value"] in aminoData :
            tab = aminoData[item["family"]["value"]]
            tab.append(item["amino"]["value"])
            aminoData[item["family"]["value"]]=tab
        else : 
            aminoData[item["family"]["value"]] = [item["amino"]["value"]]

    #first average by family
    aminoAvgFam = []
    for key, value in aminoData.items(): 
        sums = Counter()
        counters = Counter()
        for itemset in value :
            itemset = json.loads(itemset)
            sums.update(itemset)
            counters.update(itemset.keys())
        ret = {x: float(sums[x])/counters[x] for x in sums.keys()}
        aminoAvgFam.append(ret)

    #then average 

    sums = Counter()
    counters = Counter()
    for itemset in aminoAvgFam :
        sums.update(itemset)
        counters.update(itemset.keys())
    aminoFinal= {x: float(sums[x])/counters[x] for x in sums.keys()}

    diagram = []
    unidata = {"A":0.0916,"Q":0.0377,"L":0.0990,"S":0.0665,"R":0.0575,"E":0.0618,"K":0.0494,"T":0.0554,"N":0.0383,"G":0.0733,"M":0.0238,"W":0.0130,"D":0.0547,"H":0.0219,"F":0.0392,"Y":0.0291,"C":0.0120,"I":0.0567,"P":0.0486,"V":0.0690}
    for key,value in aminoFinal.items():
        for key2,value2 in unidata.items():
            if key==key2 :
                diagram.append([key,value*100,value2*100])


    #print(aminoFinal)

    queryCATH = """
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        SELECT ?cathTopLabel ?cathLabel (COUNT(distinct ?family2) as ?nb)
        WHERE {
        ?prot rdf:type model:Protein .
        ?prot model:tag <http://purl.amypdb.org/thesaurus/Amyp_Amyloid> .
        ?prot model:isoform ?iso .
        ?prot model:family ?family .
        FILTER regex(str(?family),"CATH","i")
        ?prot model:family ?family2 .
        FILTER regex(str(?family2),"Amyp","i")
        ?family skos:broader ?broadCath .        
        ?broadCath skos:prefLabel ?cathLabel .
        ?broadCath skos:broader ?cathTop . 
        ?cathTop skos:prefLabel ?cathTopLabel .
        }
        GROUP BY ?cathTopLabel ?cathLabel
     """
    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = { 'query': queryCATH }
    r=requests.get(endpoint,myparam,headers=headers)    
    results = r.json()
      
    dictCath =  []
    for row in results["results"]["bindings"] :
        dict =  {}
        dict["name"] = row["cathLabel"]["value"]
        dict["value"] = row["nb"]["value"]
        dict["group"] = row["cathTopLabel"]["value"]
        dictCath.append(dict)

    dictCath2 = sorted(dictCath, key=itemgetter('group'))

    dictCathJson = json.loads(json.dumps(dictCath2))
    #print(dictCathJson)

    return {'project': 'amyp' , 'dictAvg' : dictAvgFinal, 'aminoAvg' : diagram, 'dictCath' : dictCathJson}






@view_config(route_name='protein_amy', renderer='../templates/protein.jinja2')
def protein_amy(request):
    settings = request.registry.settings
    if settings['maintenance'] :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']
    mainInfo = Protein(itemId).getMainInfo()
    annotDict = Protein(itemId).getAmyloidAnnotations()
    annotDictSort = sorted(annotDict, key=itemgetter('qualiLabel'))
    resultAnnot = json.loads(json.dumps(annotDictSort))

    return {'itemId': itemId , 'mainInfo' : mainInfo, 'annotDict': resultAnnot}

@view_config(route_name='protein_bio', renderer='../templates/protein_bio.jinja2')

def protein_bio(request):

    settings = request.registry.settings
    if settings['maintenance'] :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']
    print("identifiant : " + itemId)  
    mainInfo = Protein(itemId).getMainInfo()
    sankey = Protein(itemId).getBioThread()
    data = json.loads(json.dumps(sankey["data"]))
    colors = json.loads(json.dumps(sankey["node_colors"]))       
    legend = json.loads(json.dumps(sankey["legend"]))
    return {'itemId': itemId ,  'mainInfo' : mainInfo, 'data' : data, 'colors' : colors, 'legend' : legend}


@view_config(route_name='protein_seq', renderer='../templates/protein_seq.jinja2')
def protein_seq(request):

    settings = request.registry.settings
    if settings['maintenance'] :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']
    print("identifiant : " + itemId) 
    mainInfo = Protein(itemId).getMainInfo() 
    isoCanoLabel = ""
    bestPdbId = ""

    isoDict = Protein(itemId).getIsoformInfo()
    isoDictSort=sorted(isoDict, key=itemgetter('isoAcc')) 
    
    for elt in isoDictSort : 
        #if "isoSeq" in elt : 
            #seqSt = elt["isoSeq"]
            #seqArr = re.findall('.{1,10}', seqSt)
            #seqSt2 = '  '.join(seqArr)
            #elt["isoSeq"] = seqSt2
        if "signInfoList" in elt : 
            seqSign = elt["signInfoList"]          
            tab = seqSign.split("|")
            seqStr2 = ""
            if len(tab) > 0  : 
                sign = [] 
                nbSignature = 0
                for item in tab :
                    id_name = item.split("::")
                    if len(id_name)> 1 : 
                        nbSignature += 1
                        sign.append("<li><a target='_blank' href='https://www.ebi.ac.uk/interpro/entry/"+id_name[0]+"'>"+ id_name[1] +"</a><li>")
                seqStr2 ="<ul>"+ ' '.join(sign) +"</ul>"
            elt['signInfoList'] = seqStr2
            elt['nbSignature'] = nbSignature
        if "isoCano" in elt : 
            if elt["isoCano"] == "True" or elt["isoCano"] == "true" :
                isoCanoLabel = elt["isoAcc"]
        if "bestPdbId" in elt : 
            bestPdbId = elt["bestPdbId"]

    resultIso = json.loads(json.dumps(isoDictSort))
    
    data = Isoform(isoCanoLabel).getIsoProp() 
    dictHSA = json.loads(data[0])
    isoLength = int(data[1])
    listDiso = json.loads(data[2])
    listSolvent = json.loads(data[3])
    HSA = []
    HSAviewer = []
    Diso = []
    Solvent = []
    for i in range(0,isoLength) : 
        HSA.append([i + 1, 0])
        HSAviewer.append({"x":i+1,"y":0})
    
    for key,value in dictHSA.items() : 
        if len(value) > 1 :
            for index in range(value[0],value[1]) :            
                HSA[index-1]=[index,key]
                HSAviewer[index-1]={"x":index,"y":key}
        else :
            HSA[value[0]-1]=[value[0],key]
            HSAviewer[value[0]-1]={"x":value[0],"y":key}
            
    for disoElt in listDiso : 
        print(disoElt)
        Diso.append({"x":disoElt[0],"y":disoElt[1]})
        
    for solElt in listSolvent : 
        Solvent.append({"x":solElt[0],"y":solElt[1]})
        

    return {'itemId': itemId ,  'mainInfo' : mainInfo, 'isoDict': resultIso , 'isoCanoLabel' : isoCanoLabel, 'bestPdbId' : bestPdbId, 'hsa' : HSA, 'hsaviewer': HSAviewer, 'diso' : Diso, 'solvent' : Solvent }



@view_config(route_name='protein_var', renderer='../templates/protein_var.jinja2')
def protein_var(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']
    print("identifiant : " + itemId)
    mainInfo = Protein(itemId).getMainInfo()

    print("starting getDiseaseInfo")
   # disDict = Protein(itemId).getDiseaseInfo()
    # print("starting sort")
    #disDictSort = sorted(disDict, key=itemgetter('diseaseLabel'))
    # print("modifying values")

    # for elt in disDictSort:
    #     if "pubList" in elt:
    #         seqSign = elt["pubList"]
    #         tab = seqSign.split("|")
    #         sign = []
    #         counter = 0
    #         for item in tab:
    #             if counter < 4:
    #                 sign.append("<a target='_blank' href='" + item + "'>Pubmed:" + item.split('/')[-1] + "</a>")
    #                 counter = counter + 1
    #             elif counter == 4:
    #                 sign.append("...")
    #                 counter = counter + 1
    #             else:
    #                 break
    #         seqStr2 = ', '.join(sign)
    #         elt['pubList'] = seqStr2

    #resultDis = json.loads(json.dumps(disDictSort))

    #disComment = Protein(itemId).getDisComment()
    #print(disComment)

    variantDict = Protein(itemId).getAllVariantInfo()
    variantDictSort = sorted(variantDict, key=itemgetter('score'))

    return {'itemId': itemId, 'mainInfo': mainInfo, 'variantDict': variantDictSort}

@view_config(route_name='protein_patho', renderer='../templates/protein_patho.jinja2')
def protein_patho(request):

    settings = request.registry.settings
    if settings['maintenance'] :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']
    print("identifiant : " + itemId)  
    mainInfo = Protein(itemId).getMainInfo()
    disDictG = []
    print("geneInfo")
    if "geneList" in mainInfo :
        seqGenes = mainInfo["geneList"]          
        tabGenes = seqGenes.split("|")
        genes = [] 
        for item in tabGenes:
            elt = item.split("::")
            if len(elt) > 1 : 
                genes.append({"geneId":elt[0],"geneLabel":elt[1]})
                if len(disDictG) == 0 : 
                    disDictG = Gene(elt[0]).getDiseases()
                else :
                    disDictG = disDictG + Gene(elt[0]).getDiseases()
        mainInfo['geneList'] = genes
    disDictSortG = sorted(disDictG, key=itemgetter('score'), reverse=True)
    resultDisG = json.loads(json.dumps(disDictSortG))
    
    print("starting getDiseaseInfo")
    disDict = Protein(itemId).getDiseaseInfo()
    #print("starting sort")
    disDictSort = sorted(disDict, key=itemgetter('diseaseLabel'))
    #print("modifiying values")
 
    for elt in disDictSort : 
        if "pubList" in elt : 
            if elt["pubList"]  != "" :
                seqSign = elt["pubList"]          
                tab = seqSign.split("|")
                sign = [] 
                counter = 0
                for item in tab :
                    if counter < 4 :
                        sign.append("<a target='_blank' href='"+item+"'>Pubmed:"+item.split('/')[-1]+"</a>")
                        counter = counter + 1
                    elif counter == 4 :
                        sign.append("...")
                        counter = counter + 1
                    else : 
                        break
                seqStr2 =', '.join(sign) 
                elt['pubList'] = seqStr2

    resultDis = json.loads(json.dumps(disDictSort))
    

    disComment = Protein(itemId).getDisComment()
    print(disComment)
    
    print("get Variant Ifno")

    variantDict = Protein(itemId).getVariantInfo()
    for elt in variantDict : 
        if "position" in elt :
            pos = elt["position"].split("-")
            start = pos[0]
            end = pos[1]
            if start == end :
                elt["position"] = start
            
    variantDictSort = sorted(variantDict, key=itemgetter('score'), reverse=True)
    
    print("returning results")
    
    return {'itemId': itemId ,  'mainInfo' : mainInfo, 'disDict': resultDis, 'variantDict' : variantDictSort, 'disComment' : disComment, 'disDictG':resultDisG}




@view_config(route_name='protein_pub', renderer='../templates/protein_pub.jinja2')
def protein_pub(request):

    settings = request.registry.settings
    if settings['maintenance'] :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']
    print("get_publi")
    pubDict = Protein(itemId).getPublications()
    resultPub = json.loads(json.dumps(pubDict))
    print("got_publi")
    pubDisg = Protein(itemId).getEntrezGeneId()
    geneId = ""
    print(pubDisg)
    for elt in pubDisg : 
        if "geneUrl" in elt : 
            geneId = elt["geneUrl"].split("/")[-1]
    print("get_mainInfo")
    mainInfo = Protein(itemId).getMainInfo()
    return {'itemId': itemId , 'mainInfo' : mainInfo , 'pubDict' : resultPub, 'geneId': geneId}



@view_config(route_name='gene', renderer='../templates/gene.jinja2')
def gene(request):

    settings = request.registry.settings
    if settings['maintenance'] :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']
    mainInfo = Gene(itemId).getMainInfo()
    protList = Gene(itemId).getProteins()
    sankey = Gene(itemId).getBioThread()
    data = json.loads(json.dumps(sankey["data"]))
    colors = json.loads(json.dumps(sankey["node_colors"]))       
    legend = json.loads(json.dumps(sankey["legend"]))
    return {'itemId': itemId, 'mainInfo' : mainInfo,'data' : data, 'colors' : colors, 'legend' : legend, 'protList' : protList }

@view_config(route_name='gene_diseases', renderer='../templates/gene_diseases.jinja2')
def gene_diseases(request):

    settings = request.registry.settings
    if settings['maintenance'] :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']
    mainInfo = Gene(itemId).getMainInfo()
    disDict = Gene(itemId).getDiseases()
    disDictSort = sorted(disDict, key=itemgetter('score'), reverse=True)

    phenoList = Gene(itemId).getPheno()
    mainPheno = []
    disPheno = []
    for item in phenoList : 
        if item not in disPheno : 
            disPheno.append(item)
            if phenoList.count(item) > 1 : 
                mainPheno.append({"phenoLabel":item,"nb":phenoList.count(item)})
    mainPhenoSorted = sorted(mainPheno, key=itemgetter('nb'), reverse=True)
    #print(mainPhenoSorted)
    return {'itemId': itemId, 'mainInfo' : mainInfo, 'disDict' : disDictSort, 'pheno' : mainPhenoSorted}



@view_config(route_name='disease', renderer='../templates/disease.jinja2')
def disease(request):

    settings = request.registry.settings
    if settings['maintenance'] :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']
    mainInfo = Disease(itemId).getMainInfo()
    
    resultParent = Disease(itemId).getParent()
    resultChildren = Disease(itemId).getChildren()
    resultAssoc = Disease(itemId).getAssociated()
    
    if "phenoList" in mainInfo :
        seqPheno = mainInfo["phenoList"]          
        tabPheno = seqPheno.split("|")
        pheno = [] 
        for item in tabPheno:
            pheno.append(item)
        mainInfo['phenoList'] = pheno
        
    # geneDict = Disease(itemId).getGenes()
    # resultGene = geneDict

    # genes = ""

    # for elt in geneDict : 
        # if "geneId" in elt : 
            # if len(geneDict) > 10 and len(geneDict) < 30 : 
                # if "score" in elt : 
                    # if float(elt["score"]) > 0.3 :
                        # genes = genes + "<" + STR_AMKB+elt["geneId"] +">" 

            # elif len(geneDict) >= 30 : 
                # if "score" in elt : 
                    # if float(elt["score"]) >= 0.4 : 
                        # genes = genes + "<" + STR_AMKB+elt["geneId"] +">" 
            # else : 
                # genes = genes + "<" + STR_AMKB+elt["geneId"] +">"  


    # if len(genes) > 0 : 
        # sankey = Disease(itemId).getDiseaseFromGenes(genes)
        # data = json.loads(json.dumps(sankey["data"]))
        # colors = json.loads(json.dumps(sankey["node_colors"]))       
        # legend = json.loads(json.dumps(sankey["legend"]))
    # else : 
        # data = []
        # colors = []
        # legend = []


    return {'itemId': itemId, 'mainInfo' : mainInfo, 'resultParent' : resultParent, 'resultChildren' : resultChildren, 'resultAssoc' : resultAssoc}
    
    

@view_config(route_name='disease_geneprot', renderer='../templates/disease_geneprot.jinja2')
def disease_genes(request):

    settings = request.registry.settings
    if settings['maintenance'] :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)
    itemId = request.matchdict['itemId']
    endpoint = settings['repo_amyp_query']

    mainInfo = Disease(itemId).getMainInfo()

    geneDict = Disease(itemId).getGenes("LIMIT 100")
    resultGene = sorted(geneDict,key=itemgetter('score'), reverse=True)
    print("gene ready")
    resultProt = Disease(itemId).getProteins("LIMIT 20")
    resultProtSorted = sorted(resultProt, key=itemgetter('proteinLabel'))
    print("prot ready")
    #resultVar = Disease(itemId).getVariants("LIMIT 20")
    #resultVariant = sorted(resultVar,key=itemgetter('proteinLabel'))
    #print("var ready")

    # phenoList = Disease(itemId).getPhenoFromGenes(genes)
    # mainPheno = []
    # disPheno = []
    # for item in phenoList : 
        # if item not in disPheno : 
            # disPheno.append(item)
            # if phenoList.count(item) > 1 : 
                # mainPheno.append({"phenoLabel":item,"nb":phenoList.count(item)})
    # mainPhenoSorted = sorted(mainPheno, key=itemgetter('nb'), reverse=True)
  
    #return {'itemId': itemId, 'mainInfo' : mainInfo, 'resultGene' : resultGene, 'data' : data, 'colors' : colors, 'legend' : legend, 'pheno': mainPhenoSorted}
    return {'itemId': itemId, 'mainInfo' : mainInfo, 'resultGene' : resultGene,  'resultProt' : resultProtSorted}


# @view_config(route_name='disease_proteins', renderer='../templates/disease_proteins.jinja2')
# def disease_proteins(request):

    # settings = request.registry.settings
    # if settings['maintenance'] :
        # url = request.route_url(route_name='unavailable')
        # return HTTPFound(location = url)
    # itemId = request.matchdict['itemId']
    # endpoint = settings['repo_amyp_query']
    
    # print("disease_proteins")

    # mainInfo = Disease(itemId).getMainInfo()
    # resultProt = Disease(itemId).getProteins()
    # resultProtSorted = sorted(resultProt, key=itemgetter('nb'), reverse=True)
    # resultVariant = Disease(itemId).getVariants()


    # return {'itemId': itemId, 'mainInfo' : mainInfo, 'resultProt' : resultProtSorted, 'resultVariant' : resultVariant}
    
    



@view_config(route_name='amylome_protein_list', renderer='../templates/amylome_protein_list.jinja2')
def amylome_protein_list(request):
    settings = request.registry.settings
    if settings['maintenance'] :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)

    endpoint = settings['repo_amyp_query']
    querystr = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    SELECT distinct ?entityId ?entityLabel (GROUP_CONCAT(DISTINCT ?desc; SEPARATOR=". ") AS ?description) (GROUP_CONCAT(DISTINCT ?qualiL; SEPARATOR=", ") AS ?qualifier) 
    WHERE { 
    ?entity rdf:type model:Protein .
    ?entity model:accession ?entityId .
    ?entity rdfs:label ?entityLabel .
    ?entity model:description ?desc .
    ?entity model:tag ?tag .
    ?tag skos:prefLabel ?qualiL .
    ?entity model:organism <http://purl.amypdb.org/thesaurus/Uniprot_taxonomy_9606> .
    VALUES ?tag {
    <http://purl.amypdb.org/thesaurus/Amyp_Amyloid>
	<http://purl.amypdb.org/thesaurus/Amyp_Amyloid_Candidate>
    <http://purl.amypdb.org/thesaurus/Amyp_Amyloid_Like> 
    <http://purl.amypdb.org/thesaurus/Amyp_Involved_In_Amyloidosis>
    <http://purl.amypdb.org/thesaurus/Amyp_Amylome_Interactor>  }
    } GROUP BY  ?entityId ?entityLabel
     """
    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = { 'query': querystr }
    r=requests.get(endpoint,myparam,headers=headers)    
    results = r.json()
      
    data=[]
    for row in results["results"]["bindings"] :
        dict =  {}
        for elt in results["head"]["vars"] : 
            if elt in row :
                dict[elt] = row[elt]["value"] 
            else : 
                dict[elt] = ""
        data.append(dict)


    return {'resultList' : data, 'title' : 'Proteins belonging to the Amylome : amyloid proteins, proteins involved in amyloidosis and theirs interactors'}


@view_config(route_name='amyloid_protein_list', renderer='../templates/amylome_protein_list.jinja2')
def amyloid_protein_list(request):
    print("amyloid_protein_list")
    settings = request.registry.settings
    if settings['maintenance'] :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)
    endpoint = settings['repo_amyp_query']

    querystr = """
    PREFIX search: <http://www.openrdf.org/contrib/lucenesail#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
        SELECT distinct ?entityId ?entityLabel (GROUP_CONCAT(DISTINCT ?desc; SEPARATOR=". ") AS ?description) (GROUP_CONCAT(DISTINCT ?qualiL; SEPARATOR=", ") AS ?qualifier) 
        WHERE { 
        ?entity rdf:type model:Protein .
        ?entity model:accession ?entityId .
         ?entity rdfs:label ?entityLabel .
         ?entity model:description ?desc .
        ?entity model:amyTagEvidence thes:ECO_0000006 .
        ?entity model:tag ?tag .
        ?tag skos:prefLabel ?qualiL .
    ?entity model:organism <http://purl.amypdb.org/thesaurus/Uniprot_taxonomy_9606> .
    VALUES ?tag { <http://purl.amypdb.org/thesaurus/Amyp_Amyloid> <http://purl.amypdb.org/thesaurus/Amyp_Amyloid_Like> 
} } GROUP BY ?entityId ?entityLabel   """
    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = { 'query': querystr }
    r=requests.get(endpoint,myparam,headers=headers)    
    results = r.json()
      
    data=[]
    for row in results["results"]["bindings"] :
        dict =  {}
        for elt in results["head"]["vars"] : 
            if elt in row :
                dict[elt] = row[elt]["value"] 
            else : 
                dict[elt] = ""
        data.append(dict)
    return {'resultList' : data, 'title' : 'Amyloid proteins     (experimental evidence)'}
    
@view_config(route_name='amyloid_fragment_list', renderer='../templates/amyloid_fragment_list.jinja2')
def amyloid_fragment_list(request):
    settings = request.registry.settings
    if settings['maintenance'] :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)
    endpoint = settings['repo_amyp_query']

    querystr = """
    PREFIX search: <http://www.openrdf.org/contrib/lucenesail#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
        SELECT distinct ?entityId ?sequence ?annotInfo (GROUP_CONCAT(DISTINCT ?isoId; SEPARATOR=",") AS ?isoIdList)   
        WHERE { 
        ?entity rdf:type model:Fragment .
        ?entity model:accession ?entityId .        
         ?entity model:primStruct ?primStruct .
         ?primStruct model:sequence ?sequence .
         ?annot model:referringTo ?entity .
         ?annot model:description ?description .
         ?annot model:source ?source .
         BIND(CONCAT(?description," <b>[Source : ", ?source,"]</b>") as ?annotInfo)
         ?iso model:amyFragment ?entity .
         ?iso model:accession ?isoId .
        } GROUP BY ?entityId  ?sequence ?annotInfo """
    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = { 'query': querystr }
    r=requests.get(endpoint,myparam,headers=headers)    
    results = r.json()
      
    data=[]
    for row in results["results"]["bindings"] :
        dict =  {}
        for elt in results["head"]["vars"] : 
            if elt in row :
                dict[elt] = row[elt]["value"] 
            else : 
                dict[elt] = ""
        data.append(dict)
        
        
    for elt in data : 
        if "sequence" in elt : 
            dictSeq={}
            nb = 0
            for index in range(0,len(elt["sequence"]),20) : 
                if index+20 < len(elt["sequence"]) : 
                    dictSeq[index] = elt["sequence"][index:index+20] + "<br />"       
                else : 
                    dictSeq[index] = elt["sequence"][index:len(elt["sequence"])]
            elt["sequence"] = ""
            for key in dictSeq.keys() : 
                elt["sequence"] += dictSeq[key]
                
            
        if "isoIdList" in elt : 
            if elt["isoIdList"]  != "" :
                seqIso = elt["isoIdList"]                 
                tab = seqIso.split(",")
                tab2 = tab.copy()
                protList = ""
                for elt2 in tab2 : 
                    protList = protList + elt2.split("-")[0]+","
                if protList != "" : 
                    protList = protList[:-1]
                #isoList = [] 
                counter= 0 
                isoListStr = ""
                for item in tab :
                    if counter < 4 :
                        link = "<a href='"+ request.route_url(route_name='protein_seq', itemId=item.split("-")[0]) +"'>"+ item.split("-")[0] + "</a>"
                        #isoList.append(link)
                        isoListStr = isoListStr + link + ", "
                        counter = counter + 1                        
                    elif counter == 4 :                     
                        form = """<form action='/results_list' method='post'><a href='javascript:;' onclick='parentNode.submit();'>Check all proteins matching this sequence</a><input type='hidden' name='search_item' value='proteins matching """ 
                        seq = elt["sequence"]
                        if len(seq) > 10 : 
                            form += seq[:10] + "..."
                        else :
                            form += seq
                        form += """ ' /><input type='hidden' name='search_list' value=' """
                        form += "List:["+ protList + "]"
                        form += """' /> </form> """
                        isoListStr = isoListStr + "... " + form
                        counter = counter + 1  
                    else :
                        break
                        
                elt['isoIdList'] = isoListStr
                
    noRepeat={}
    for elt in data : 
        if elt["entityId"] in noRepeat.keys() : 
            dictInfo2 = dictInfo.copy()
            dictInfo2["annotInfo"] = dictInfo2["annotInfo"] + "<br /><br />" + elt["annotInfo"] 
        else : 
            dictInfo = {}
            dictInfo["annotInfo"] = elt["annotInfo"] 
            print(elt["annotInfo"])
            dictInfo["isoIdList"] = elt["isoIdList"]
            dictInfo["sequence"] = elt["sequence"]
            noRepeat[elt["entityId"]] = dictInfo
            
    data2 = [] 
    for value in noRepeat.values() : 
        data2.append(value)       
        
   
    return {'resultList' : data2, 'title' : 'Amino acid sequences which form fibrillar deposits (with experimental evidence)'}
    
    
    
@view_config(route_name='all_amyloid_protein_list', renderer='../templates/amylome_protein_list.jinja2')
def all_amyloid_protein_list(request):
    settings = request.registry.settings
    if settings['maintenance'] :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)
    endpoint = settings['repo_amyp_query']

    querystr = """
    PREFIX search: <http://www.openrdf.org/contrib/lucenesail#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
        SELECT distinct ?entityId ?entityLabel (GROUP_CONCAT(DISTINCT ?desc; SEPARATOR=". ") AS ?description) (GROUP_CONCAT(DISTINCT ?qualiL; SEPARATOR=", ") AS ?qualifier)
        WHERE { 
        ?entity rdf:type model:Protein .
        ?entity model:accession ?entityId .
         ?entity rdfs:label ?entityLabel .
         ?entity model:description ?desc .
        ?entity model:tag ?tag .
        ?tag skos:prefLabel ?qualiL .
        ?entity model:organism <http://purl.amypdb.org/thesaurus/Uniprot_taxonomy_9606> .
        VALUES ?tag { <http://purl.amypdb.org/thesaurus/Amyp_Amyloid> <http://purl.amypdb.org/thesaurus/Amyp_Amyloid_Like> <http://purl.amypdb.org/thesaurus/Amyp_Amyloid_Candidate>
        } }  GROUP BY ?entityId ?entityLabel   """
    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = { 'query': querystr }
    r=requests.get(endpoint,myparam,headers=headers)    
    results = r.json()
      
    data=[]
    for row in results["results"]["bindings"] :
        dict =  {}
        for elt in results["head"]["vars"] : 
            if elt in row :
                dict[elt] = row[elt]["value"] 
            else : 
                dict[elt] = ""
        data.append(dict)


    return {'resultList' : data, 'title' : 'Proteins annotated as Amyloid or Amyloid Candidate'}


@view_config(route_name='disease_list', renderer='../templates/disease_list.jinja2')
def disease_list(request):
    settings = request.registry.settings
    if settings['maintenance'] :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)
    endpoint = settings['repo_amyp_query']

    querystr = """PREFIX search: <http://www.openrdf.org/contrib/lucenesail#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
        SELECT distinct ?entityId ?entityLabel (GROUP_CONCAT(DISTINCT ?desc; SEPARATOR=". ") AS ?description) (GROUP_CONCAT(DISTINCT ?qualiL; SEPARATOR=", ") AS ?qualifier) 
        WHERE {{ 
        ?entity model:accession ?entityId . 
        ?entity rdfs:label ?entityLabel . 
        ?entity model:description ?desc . 
        ?entity rdf:type model:Disease . 
        ?entity model:tag ?tag . 
        ?tag skos:prefLabel ?qualiL . 
        VALUES ?tag {<http://purl.amypdb.org/thesaurus/Amyp_Amyloidosis> <http://purl.amypdb.org/thesaurus/Amyp_Amyloid_Related_Disease>  } 
        }} GROUP BY ?entityId ?entityLabel """
    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = { 'query': querystr }
    r=requests.get(endpoint,myparam,headers=headers)    
    results = r.json()
      
    data=[]
    for row in results["results"]["bindings"] :
        dict =  {}
        for elt in results["head"]["vars"] : 
            if elt in row :
                dict[elt] = row[elt]["value"] 
            else : 
                dict[elt] = ""
        data.append(dict)


    return {'resultList' : data}
    
    
@view_config(route_name='disease_list_pheno', renderer='../templates/disease_list_pheno.jinja2')
def disease_list_pheno(request):
    settings = request.registry.settings
    if settings['maintenance'] :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)
    endpoint = settings['repo_amyp_query']

    querystr = """PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
                        PREFIX model: <http://purl.amypdb.org/model/>
						PREFIX thes: <http://purl.amypdb.org/thesaurus/>
                        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
                        SELECT ?phenoRef ?phenoLabel  (COUNT(?maladie) AS ?nb)
                        WHERE {
						{ ?maladie model:tag thes:Amyp_Amyloidosis .}
						UNION 
						{ ?maladie model:tag thes:Amyp_Amyloid_Related_Disease . } 
						?maladie model:phenoRef ?phenoRef .
						?phenoRef skos:prefLabel ?phenoLabel .						
                        } GROUP BY ?phenoRef ?phenoLabel ORDER BY DESC(?nb) LIMIT 99 """
    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = { 'query': querystr }
    r=requests.get(endpoint,myparam,headers=headers)    
    results = r.json()
      
    data=[]
    for row in results["results"]["bindings"] :
        dict =  {}
        for elt in results["head"]["vars"] : 
            if elt in row :
                dict[elt] = row[elt]["value"] 
            else : 
                dict[elt] = ""
        data.append(dict)
    return {'resultList' : data}


@view_config(route_name='gene_list', renderer='../templates/gene_list.jinja2')
def gene_list(request):
    settings = request.registry.settings
    if settings['maintenance'] :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)
    endpoint = settings['repo_amyp_query']

    querystr = """
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX model: <http://purl.amypdb.org/model/> 
PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
SELECT distinct ?entityId ?entityLabel ?description  (GROUP_CONCAT(DISTINCT ?proteinLabel; SEPARATOR=", ") AS ?proteinLabels) 
WHERE { 
?entity rdf:type model:Gene .
?entity model:accession ?entityId .
?entity rdfs:label ?entityLabel .
?entity model:description ?description . 
?entity model:encodes ?protein .
?protein model:tag ?tag .
?protein rdfs:label ?proteinLabel .
?protein model:organism <http://purl.amypdb.org/thesaurus/Uniprot_taxonomy_9606> .
VALUES ?tag { <http://purl.amypdb.org/thesaurus/Amyp_Amyloid> <http://purl.amypdb.org/thesaurus/Amyp_Amyloid_Like> 
<http://purl.amypdb.org/thesaurus/Amyp_Involved_In_Amyloidosis> <http://purl.amypdb.org/thesaurus/Amyp_Amylome_Interactor>  }
} GROUP BY ?entityId ?entityLabel ?description
    """
    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    myparam = { 'query': querystr }
    r=requests.get(endpoint,myparam,headers=headers)    
    results = r.json()
      
    data=[]
    for row in results["results"]["bindings"] :
        dict =  {}
        for elt in results["head"]["vars"] : 
            if elt in row :
                dict[elt] = row[elt]["value"] 
            else : 
                dict[elt] = ""
        data.append(dict)


    return {'resultList' : data}



@view_config(route_name='results_txt', renderer='../templates/results.jinja2')
def results_txt(request):

    resultGene = []
    resultProtein = []
    resultDisease = []

    settings = request.registry.settings
    endpoint = settings['repo_amyp_query']
    if settings['maintenance'] :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)

    

    if 'search_text' in request.params :
        search_item = request.params['search_text']
		#search_opt = request.params['entity_type']

		
		#search_type = " model:Gene model:Protein model:Disease "
		#search_type = 'model:Protein'
		#if search_opt == "proteins" :
		#	search_type = " model:Protein "
		#elif search_opt == "diseases" :
		#	search_type = " model:Disease "
		#elif search_opt == "genes" :
		#	search_type = " model:Gene "

        print(search_item)
        search_opt = "any_type"
        if "entity_type" in request.params : 
            search_opt = request.params['entity_type']
        
        search_param = "any_specie"
        if "specie_select" in request.params : 
            search_param = request.params['specie_select']
        search_type = "model:Disease model:Protein model:Gene"
        search_tag = "?quali"
        
        if search_opt == "proteins" :
            search_type = "model:Protein"
        elif search_opt == "amyloid" :
            search_type = "model:Protein"
            search_tag = "<http://purl.amypdb.org/thesaurus/Amyp_Amyloid>"
        elif search_opt == "amylome_interactor" :
            search_type = "model:Protein"
            search_tag = "<http://purl.amypdb.org/thesaurus/Amyp_Amylome_Interactor>"
        elif search_opt == "involved_amyloidosis" :
            search_type = "model:Protein"
            search_tag = "<http://purl.amypdb.org/thesaurus/Amyp_Involved_In_Amyloidosis>"
        elif search_opt == "pathogenic" :
            search_type = "model:Protein"
            search_tag = "<http://purl.amypdb.org/thesaurus/Amyp_Pathogenic>"
        elif search_opt == "diseases" :
            search_type = "model:Disease"
        elif search_opt == "amyloidosis" :
            search_type = "model:Disease"
            search_tag = "<http://purl.amypdb.org/thesaurus/Amyp_Amyloidosis>"
        elif search_opt == "amyloid_related_disease" :
            search_type = "model:Disease"
            search_tag = "<http://purl.amypdb.org/thesaurus/Amyp_Amyloid_Related_Disease>"
        elif search_opt == "genes" :
            search_type = "model:Gene"

        print(search_opt)
        print(search_type)
        print(search_tag)
        
        

        if search_tag == "?quali" :
            search_tag_line = "OPTIONAL{ ?entity model:tag ?quali . ?quali skos:prefLabel ?qualifier . }"
        else : 
            search_tag_line = "?entity model:tag "+ search_tag + " . " + search_tag + " skos:prefLabel ?qualifier ."

        search_tag_line2 = ""
        if search_param == 'human' and search_type != "model:Disease" :
            search_tag_line2 = " ?entity model:organism ?org . ?org skos:prefLabel ?orgLabel . FILTER (?org = <http://purl.amypdb.org/thesaurus/Uniprot_taxonomy_9606>)"
        elif search_type != "model:Disease" : 
            search_tag_line2 = " ?entity model:organism ?org . ?org skos:prefLabel ?orgLabel ."

        
        #query if entityType is Protein or Disease
        querystr = """PREFIX search: <http://www.openrdf.org/contrib/lucenesail#>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
        PREFIX model: <http://purl.amypdb.org/model/> 
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
        SELECT ?entityId ?entityLabel ?entityType ?orgLabel ?description  ?qualifier
        WHERE {{  
        BIND({searchtype} as ?entityType)
        ?entity rdf:type ?entityType .
        VALUES ?prop {{ model:description rdfs:label }} 
        ?entity search:matches [ 
        search:query '{searched}'; 
        search:property ?prop ;
        search:snippet ?snip] .    
        ?entity model:accession ?entityId . 
        ?entity rdfs:label ?entityLabel . 
        ?entity model:description ?description . 
        {searchtagline2} 
         {searchtagline}         
        }}  """.format(searched = search_item.strip(), searchtype = search_type ,  searchtagline = search_tag_line, searchtagline2 = search_tag_line2)
        headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
        
        #query if all entity type
        if search_type == "model:Disease model:Protein model:Gene" :
            querystr = """PREFIX search: <http://www.openrdf.org/contrib/lucenesail#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
            PREFIX model: <http://purl.amypdb.org/model/> 
            PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
            SELECT ?entityId ?entityLabel ?entityType ?orgLabel ?description  ?qualifier ?protId ?protLabel
            WHERE {{  
            {{
             BIND(model:Disease as ?entityType)
           ?entity rdf:type ?entityType .
            ?entity model:accession ?entityId . 
            ?entity rdfs:label ?entityLabel . 
            FILTER regex(?entityLabel,"{searched}","i")
            ?entity model:description ?description .             
             {searchtagline}  }}
             UNION {{
           BIND(model:Disease as ?entityType)
           ?entity rdf:type ?entityType .
            ?entity model:accession ?entityId . 
            ?entity rdfs:label ?entityLabel . 
            ?entity model:description ?description .  
            FILTER regex(?description,"{searched}","i")            
             {searchtagline}
             }}
             UNION {{
             BIND(model:Protein as ?entityType)
           ?entity rdf:type ?entityType .          
            ?entity model:accession ?entityId . 
            ?entity rdfs:label ?entityLabel . 
               FILTER regex(?entityLabel,"{searched}","i")
            ?entity model:description ?description .             
            {searchtagline2} 
            {searchtagline}
             }}  UNION {{
           BIND(model:Protein as ?entityType)
           ?entity rdf:type ?entityType .          
            ?entity model:accession ?entityId . 
            ?entity rdfs:label ?entityLabel . 
            ?entity model:description ?description .    
               FILTER regex(?description,"{searched}","i")            
            {searchtagline2} 
            {searchtagline}
             }} UNION {{
             BIND(model:Gene as ?entityType)
            ?entity rdf:type ?entityType .
            ?entity model:accession ?entityId . 
             ?entity model:encodes ?prot .
            ?prot model:accession ?protId .
            {searchtagline2}
            ?entity rdfs:label ?entityLabel .             
            FILTER regex(?entityLabel,"{searched}","i")
            ?entity model:description ?description .                  
           }} UNION {{
             BIND(model:Gene as ?entityType)
            ?entity rdf:type ?entityType .
            ?entity model:accession ?entityId . 
             ?entity model:encodes ?prot .
            ?prot model:accession ?protId .
            {searchtagline2}
            ?entity rdfs:label ?entityLabel .             
            ?entity model:description ?description .    
            FILTER regex(?description,"{searched}","i")            
           }}
            }}  """.format(searched = search_item.strip(), searchtype = search_type ,  searchtagline = search_tag_line, searchtagline2 = search_tag_line2)
            headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}        
        
        elif search_type == "model:Gene" :
            querystr = """PREFIX search: <http://www.openrdf.org/contrib/lucenesail#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
            PREFIX model: <http://purl.amypdb.org/model/> 
            PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
            SELECT ?entityId ?entityLabel ?entityType ?orgLabel (GROUP_CONCAT(DISTINCT ?desc; SEPARATOR=".") AS ?description) (GROUP_CONCAT(DISTINCT ?acc; SEPARATOR=";") AS ?protId)
            WHERE {{  
            BIND(model:Gene as ?entityType)
            ?entity rdf:type ?entityType .
            ?entity model:accession ?entityId . 
             ?entity model:encodes ?prot .
            ?prot model:accession ?acc .
            {searchtagline2}
            {{
            ?entity rdfs:label ?entityLabel .             
            FILTER regex(?entityLabel,"{searched}","i")
            ?entity model:description ?desc .                  
             }}
             UNION {{
            ?entity rdfs:label ?entityLabel . 
            ?entity model:description ?desc .  
            FILTER regex(?description,"{searched}","i")            
             }}
            }} GROUP BY ?entityId ?entityLabel ?entityType ?orgLabel """.format(searched = search_item.strip(), searchtype = search_type ,  searchtagline = search_tag_line, searchtagline2 = search_tag_line2)
            headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}        
        
        print(querystr)
        myparam = { 'query': querystr }
        r=requests.get(endpoint,myparam,headers=headers)    
        results = r.json()
        
        print("results returned")
 
        data=[]
        for row in results["results"]["bindings"] :
            dict =  {}
            for elt in results["head"]["vars"] : 
                if elt in row :
                    dict[elt] = row[elt]["value"] 
                else : 
                    dict[elt] = ""
            data.append(dict)
     
        
        
        if len(data) > 0 : 
            dicoById = {}
            
            for item in data :                
                if item["entityId"] in dicoById.keys() : 
                    if "qualifier" in item : 
                        qualif = dicoById[item["entityId"]]["qualifier"]                    
                        if item["qualifier"] not in qualif : 
                            qualif.append(item["qualifier"])                        
                        item["qualifier"] = qualif
                    dicoById[item["entityId"]] = item
                else : 
                    if "qualifier" in item : 
                        qualif=[]
                        qualif.append(item["qualifier"])
                        item["qualifier"] = qualif
                    dicoById[item["entityId"]] = item
                
            
            dataProtein=[]
            dataGene=[]
            dataDisease=[]
            
            print("sorting results by type")
            for key,item in dicoById.items() :
                #print("ITEM ",item["entityType"])
                if "Protein" in item["entityType"] :                   
                    dataProtein.append(item)
                elif "Gene" in item["entityType"] :
                    dataGene.append(item)
                elif "Disease" in item["entityType"] :
                    dataDisease.append(item)
                    
            if len(dataGene) > 0 : 
                for item in dataGene : 
                    protList = item["protId"]
                    if ";" in protList : 
                        protList = protList[:-1]
                        if ";" in protList : 
                            item["protId"] = protList.split(";")
                        else :
                            item["protId"] = [item["protId"]]
                    else : 
                        item["protId"] = [item["protId"]]

            resultGene = dataGene
            resultProtein = dataProtein
            resultDisease = dataDisease


            return {'search_item' : search_item, 'resultGene' : resultGene, 'resultProtein' : resultProtein, 'resultDisease' : resultDisease  }
        else : 
            print(search_item)
            querySuggest = "PREFIX search: <http://www.openrdf.org/contrib/lucenesail#> \
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
        SELECT distinct ?entityId ?entityLabel ?orgLabel \
        WHERE {{ \
        ?entity model:accession ?entityId . \
        ?entity rdf:type model:Protein . \
        ?entity rdfs:label ?entityLabel . \
        FILTER (!regex(?entityLabel,'unnamed|ABPP','')) \
        ?entity model:organism <http://purl.amypdb.org/thesaurus/Uniprot_taxonomy_9606> . <http://purl.amypdb.org/thesaurus/Uniprot_taxonomy_9606> skos:prefLabel ?orgLabel .\
         ?entity model:tag <{tag}> . \
         }} LIMIT 10 ".format(tag = STR_AMTHES+STR_AMNAME+"_Amyloid")
            headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
            myparam = { 'query': querySuggest }
            r=requests.get(endpoint,myparam,headers=headers)    
            search = r.json()
            #print("search results for suggestions")
            #print(search)
            suggestions=[]
            for row in search["results"]["bindings"] :
                dict =  {}
                for elt in search["head"]["vars"] : 
                    if elt in row :
                        dict[elt] = row[elt]["value"] 
                    else : 
                        dict[elt] = ""
                suggestions.append(dict)
            resultSuggest = suggestions
            print(resultSuggest)

            return {'search_item' : search_item, 'resultGene' : resultGene, 'resultProtein' : resultProtein, 'resultDisease' : resultDisease , 'suggestions' : resultSuggest }

 
    else :
        url = request.route_url(route_name='home')
        return HTTPFound(location = url)


@view_config(route_name='results_list', renderer='../templates/results.jinja2')
def results_list(request):
    settings = request.registry.settings
    if settings['maintenance'] :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)
    endpoint = settings['repo_amyp_query']
    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    
    search_item = request.params['search_item']
    search_list = request.params['search_list']
    print(search_list)
    strAcc = search_list.split(":")[1]
    accList = strAcc[1:len(strAcc)-1].split(",")
    valuesAcc = ""
    for elt in accList : 
        valuesAcc += "'"+elt.strip()+"'" + " "
    
    print(valuesAcc)
    
    querystr = """
    PREFIX search: <http://www.openrdf.org/contrib/lucenesail#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    PREFIX model: <http://purl.amypdb.org/model/> 
    PREFIX thes: <http://purl.amypdb.org/thesaurus/> 
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    SELECT distinct ?entityId ?entityLabel ?entityType  ?orgLabel (GROUP_CONCAT(DISTINCT ?desc; SEPARATOR=".") AS ?description) (GROUP_CONCAT(DISTINCT ?qualiL; SEPARATOR=",") AS ?qualifier)
    WHERE {{ 
    VALUES ?entityId {{ {protlist} }} 
    VALUES ?entityType {{ model:Protein }}
    ?entity model:accession ?entityId . 
    ?entity rdfs:label ?entityLabel . 
    ?entity model:description ?desc . 
    ?entity rdf:type ?entityType . 
    OPTIONAL{{?entity model:organism ?org . ?org skos:prefLabel ?orgLabel .}} 
    OPTIONAL{{?entity model:tag ?quali . ?quali skos:prefLabel ?qualiL .}}
    }}  GROUP BY ?entityId ?entityLabel ?entityType  ?orgLabel """.format(protlist = valuesAcc)
    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    
    print(querystr)
    myparam = { 'query': querystr }
    r=requests.get(endpoint,myparam,headers=headers)    
    results = r.json()
  
    data=[]
    for row in results["results"]["bindings"] :
        dict =  {}
        for elt in results["head"]["vars"] : 
            if elt in row :
                dict[elt] = row[elt]["value"] 
            else : 
                dict[elt] = ""
        data.append(dict)
        
    for item in data : 
        item["qualifier"] = item["qualifier"].split(",")
    
    return {'search_item' : search_item, 'resultGene' : [], 'resultProtein' : data, 'resultDisease' : []  }
        

@view_config(route_name='results_id', renderer='../templates/results.jinja2')
def results_id(request):
    search = None
    settings = request.registry.settings
    if settings['maintenance'] :
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location = url)
    endpoint = settings['repo_amyp_query']
    headers = {'content-type' : 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    if ('search_identifier' in request.params) : 
        search_item = request.params['search_identifier']
        print(search_item)
        if ":" in search_item : 
            search_item = search_item.replace(":","_")
            print(search_item)

        querystr1 = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
        SELECT distinct ?entity ?entityId  ?type  \
        WHERE {{ \
        ?entity model:accession ?entityId . \
        VALUES ?entityId {{ '{searched}' }}  \
        ?entity rdf:type ?type . \
         }}".format(searched = search_item.strip())

        querystr2 = "PREFIX search: <http://www.openrdf.org/contrib/lucenesail#> \
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
        PREFIX model: <http://purl.amypdb.org/model/> \
        PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
        SELECT distinct ?entity ?entityId  ?type  \
        WHERE {{ \
        VALUES ?prop {{ skos:notation }} \
        ?ref search:matches [ \
        search:query '{searched}'; \
        search:property ?prop ] . \
        ?entity model:thesRef ?ref . \
        ?entity model:accession ?entityId . \
        ?entity rdf:type ?type . \
         }}".format(searched = search_item.strip())
         
         
        r1 = requests.get(endpoint,params={"query":querystr1},headers = headers)
        search = r1.json()
        itemType = None
        itemId = None
        print(search)
        if len(search["results"]["bindings"]) > 0 :
            print("entityId")
            for item in search["results"]["bindings"] : 
                itemId = item["entityId"]["value"]
                itemType = item["type"]["value"]
        else :         
            print("second request")
            r2=requests.get(endpoint,params={"query":querystr2},headers=headers)    
            search = r2.json()
            if len(search["results"]["bindings"])>0 :
                for item in search["results"]["bindings"] : 
                    itemId = item["entityId"]["value"]
                    itemType = item["type"]["value"]

        if itemType != None :       
            if "Protein" in itemType :  
                url = request.route_url(route_name='protein_seq', itemId=itemId)
                return HTTPFound(location = url)
            elif "Gene" in itemType : 
                url = request.route_url(route_name='gene', itemId=itemId)
                return HTTPFound(location = url)
            elif "Concept" in itemType or "Disease" in itemType : 
                url = request.route_url(route_name='disease', itemId = itemId)
                return HTTPFound(location = url)
        else :
            print("try search text " + request.params['search_identifier'] )
            url = request.route_url(route_name='results_txt') + "?search_text="+request.params['search_identifier']
            print(url)
            return HTTPFound(location = url)



@view_config(route_name='results_pheno', renderer='../templates/pheno.jinja2')
def results_pheno(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)

    endpoint = settings['repo_amyp_query']
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}

    # get the symptomes ids from the autocompletion menu
    print("request: ", request.params['phenolist'])
    requestP = request.params['phenolist']

    search_items = []
    search_uris = ""
    if "," in requestP : 
        print("multiple")
        search_it = requestP.split(",")
    else : 
        print("single")
        search_it = [requestP]
    for elt in search_it : 
        if elt != "" : 
            search_uris = search_uris +  "<"+elt+"> "
            search_items.append(elt)
            

    phenoInfo = []

    querystr2 = """PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
                            PREFIX model: <http://purl.amypdb.org/model/> 
                            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
                            SELECT ?phenoLabel ?pheno
                            WHERE {{ VALUES ?pheno {{ {phenoUris} }}
                            ?pheno skos:prefLabel ?phenoLabel .
                            }}
                            """.format(phenoUris=search_uris)
    r2 = requests.get(endpoint, params={"query": querystr2}, headers=headers)
    search2 = r2.json()
    phenoInfo=anti_verbose_json(search2)
    print(phenoInfo)
        
    
    if len(search_items) > 0:
        data = []
        to_search = "" # var to stock the formated symptoms for sparql request

        for i,item in enumerate(search_items):
            #to_search += " {{ ?maladie model:phenoRef <" + i + "> . }} UNION {{?maladie model:phenoRef ?phenoChildRef .  ?phenoChildRef skos:broader*  <" + i + "> }} "
            to_search += " { ?maladie model:phenoRef ?phenoChildRef"+str(i)+" .  ?phenoChildRef"+str(i)+" skos:broader  <" + item + "> . } UNION { ?maladie model:phenoRef <" + item + "> . }"
        #print(to_search)

        querystr1 = """PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
                        PREFIX model: <http://purl.amypdb.org/model/>
                        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
                        SELECT DISTINCT ?ID ?maladie ?label ?description  (GROUP_CONCAT(DISTINCT ?qualiL; SEPARATOR=";") AS ?qualifier)
                        WHERE {{ {searched} 
                        ?maladie model:accession ?ID .
                        ?maladie rdfs:label ?label .
                        ?maladie model:description ?description .
                        OPTIONAL{{ ?maladie model:tag ?quali .
                        ?quali skos:prefLabel ?qualiL .}} 
                        }} GROUP BY ?ID ?maladie ?label ?description """.format(searched=to_search)

        print(querystr1)
        r1 = requests.get(endpoint, params={"query": querystr1}, headers=headers) # do the request sparql
        search = r1.json() # to json format
        data = anti_verbose_json(search) # see in libAmyp/some_function.py
        print("DATA", data)
        if len(data) == 1 :
            if data[0]["ID"] == "" : 
                data = []
        
        for item in data :
            if ";" in item["qualifier"] : 
                item["qualifier"] = item["qualifier"].split(";")
            else :
                item["qualifier"] = [item["qualifier"]]
                
        
        
        return {'search_items': search_items, 'symptoms': phenoInfo, 'diseases': data, 'geneprot' : []}


 

        
        
        
@view_config(route_name='result_variant', renderer='../templates/results.jinja2')
def result_variant(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    endpoint = settings['repo_amyp_query']
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}

    search_item = request.params['id_prot_var']
    print("identifiant : " + search_item)

    if ":" in search_item:
        search_item = search_item.replace(":", "_")
        print("search by id replace " + search_item)

    querystr1 = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \
    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \
    PREFIX model: <http://purl.amypdb.org/model/> \
    PREFIX thes: <http://purl.amypdb.org/thesaurus/> \
    SELECT distinct ?entity ?entityId  ?type \
    WHERE {{ \
    ?entity model:accession ?entityId . \
    VALUES ?entityId {{ '{searched}' }}  \
    ?entity rdf:type ?type . \
     }}".format(searched=search_item.strip())
    print(querystr1)

    r1 = requests.get(endpoint, params={"query": querystr1}, headers=headers)
    search = r1.json()
    itemType = None
    itemId = None
    print('search : ', search)
    if len(search["results"]["bindings"]) > 0:
        print("entityId")
        for item in search["results"]["bindings"]:
            itemId = item["entityId"]["value"]
            itemType = item["type"]["value"]
    print('itemtype ; ',itemType)
    if itemType != None and "Protein" in itemType:
            url = request.route_url(route_name='variant', itemId=itemId)
            return HTTPFound(location=url)
    else:
        print("try search text " + request.params['id_prot_var'])
        url = request.route_url(route_name='results_txt') + "?search_text=" + request.params['id_prot_var']
        print(url)
        return HTTPFound(location=url)
        
        
        
@view_config(route_name='variant', renderer='../templates/variant.jinja2')
def variant(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    endpoint = settings['repo_metamyl_query']
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    itemId = request.matchdict['itemId']
    print("identifiant : " + itemId)
    results = metamyl_with_id(itemId)
    if results != {} :
        return results
    else :
        url = request.route_url(route_name='home')  # TODO RENVOYER SUR NOT FOUND
        return HTTPFound(location=url)

@view_config(route_name='variant_search', renderer='../templates/variant_search.jinja2')
def var_search(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    itemId = request.matchdict['itemId']
    print(itemId)
    var_info=Protein(itemId).getVariantInfo()
    if len(var_info)>0:
        print(var_info)
        return {'var_info': var_info,'ItemID':itemId}
    else:
        return {'var_info': None,'ItemID':itemId}

@view_config(route_name='variant_new', renderer='../templates/variant_new.jinja2')
def var_new(request):
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    itemId = request.matchdict['itemId']

    isoDict = Protein(itemId).getIsoformInfo()
    isoDictSort = sorted(isoDict, key=itemgetter('isoAcc'))

    seqSt=""
    for elt in isoDictSort:
        if "isoCano" in elt:
            if elt["isoCano"] == "True" or elt["isoCano"] == "true":
                seqSt = elt["isoSeq"]

    if len(seqSt)>0:
        print(type(seqSt))
    return {'seq': seqSt.lower(),'ItemID':itemId}
    
    
@view_config(route_name='results_seq', renderer='../templates/variant.jinja2')
def results_seq(request):
    print( 'pred with seq')
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    endpoint = settings['repo_metamyl_query']
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    sequence = request.params['saav']
    if len(sequence) > 5 : 
        print(sequence)
        if "\n" or "\r" or "\t" or " " in sequence : 
            sequence = sequence.replace("\n", "")
            sequence = sequence.replace("\r","")
            sequence = sequence.replace("\t","")
            sequence = sequence.replace(" ","")
        results = metamyl_with_seq(sequence)
        
        return {'seq':sequence,'HS': results['HS'], 'hexa_scores': results['hexa_scores'], 'hsa': results['hsa'], 'hst': results['hst'], 'tab_HS': results['tab_HS'],'main_tab':results['main_tab']}
    else : 
        return {'seq':sequence,'HS': [], 'hexa_scores': [], 'hsa': [], 'hst': [], 'tab_HS': [],'main_tab':[]}



@view_config(route_name='variant_metamyl', renderer='../templates/variant_metamyl.jinja2')
def var_new_metamyl(request):
    print( 'new metamyl')
    settings = request.registry.settings
    if settings['maintenance']:
        url = request.route_url(route_name='unavailable')
        return HTTPFound(location=url)
    endpoint = settings['repo_metamyl_query']
    headers = {'content-type': 'application/x-www-form-urlencoded', 'Accept': 'application/json'}
    itemId = request.matchdict['itemId']

    isoDict = Protein(itemId).getIsoformInfo()
    isoDictSort = sorted(isoDict, key=itemgetter('isoAcc'))
    seqSt2 = ""
    for elt in isoDictSort:
        if "isoSeq" in elt:
            if "isoCano" in elt:
                if elt["isoCano"] == "True" or elt["isoCano"] == "true":
                    seqSt2 = elt["isoSeq"]

    search_item = request.params.dict_of_lists()
    print(search_item)
    variant= seqSt2
    if 'del_pos_end' in search_item:
        print('deletion')
        for pos in range(len(search_item['del_pos_start'])):
            if search_item['del_pos_end'][pos] != "" and search_item['del_pos_start'][pos] != "":
                start = int(search_item['del_pos_start'][pos])-1
                end = int(search_item['del_pos_end'][pos])
                variant_list = list(variant)
                for i in range(end-1, start-1,-1):
                    variant_list.pop(i)
                variant = "".join(variant_list)
            else:
                variant = 'error_del'
                break

    elif 'in_pos_start' in search_item:
        for i in range(len(search_item['in_pos_start'])):
            if search_item['in_pos_start'][i] != "" and search_item['in_pos_end'][i] != "" and search_item['in_seq_in'][
                i] != "":
                start = int(search_item['in_pos_start'][i])
                end = int(search_item['in_pos_end'][i]) - 1
                seq = search_item['in_seq_in'][i].upper()
                variant = variant[:start] + seq + variant[end:]
            else:
                variant = 'error_in'
                break

    elif 'indel_pos_start_i' in search_item:
        for i in range(len(search_item['indel_pos_start_i'])):
            if search_item['indel_pos_start_d'][i] != "" and search_item['indel_pos_end_d'][i] != "" and\
                    search_item['indel_pos_start_i'][i] != "" and search_item['indel_pos_end_i'][i] != "" and \
                    search_item['indel_pos_seq'][i] != "": # check none of input were empty
                # deletion
                start_d = int(search_item['indel_pos_start_d'][i]) - 1
                end_d = int(search_item['indel_pos_end_d'][i])
                variant_list = list(variant)
                for i in range(end_d-1, start_d-1, -1):
                    variant_list.pop(i)
                variant = "".join(variant_list)
                # insertion
                start = int(search_item['indel_pos_start_i'][i])
                end = int(search_item['indel_pos_end_i'][i]) - 1
                seq = search_item['indel_pos_seq'][i].upper()
                variant = variant[:start] + seq + variant[end:]
            else:
                variant = 'error_indel'
                break
    else:
        for i in range(len(search_item['sub_AA_select_B'])):
            if search_item['sub_AA_pos'][i] != "":
                print('in select')
                AA_1 = search_item['sub_AA_select_B'][i]
                AA_2 = search_item['sub_AA_select_A'][i]
                start_1 = int(search_item['sub_AA_pos'][i])-1
                print(variant[start_1], AA_1, AA_2)
                if variant[start_1] == AA_1:
                    variant_list = list(variant)
                    variant_list[start_1] = AA_2
                    variant = "".join(variant_list)
                    print ('after : ',variant, ' AA : ', variant[start_1])
                else:
                    variant = 'error_sub'
                    break
            else:
                variant = 'error_sub'
                break
    print(variant)

    if variant.startswith('error') :
        return {'ItemID': itemId, 'variant': variant}
    else:
        results = metamyl_with_seq(variant)
        results['variant']=variant.lower()
        results['seq_origin']=seqSt2.lower()
        return results
        #return {'HS': HS, 'hexa_scores': list_score, 'hsa': HSA, 'hst': HST, 'tab_HS': start_end,'ItemID':itemId,'variant':variant.lower() ,"seq_origin":seqSt2.lower(),'main_tab':general}





           
@view_config(route_name='unavailable', renderer='../templates/unavailable.jinja2')
def unavailable(request):
    return {}
